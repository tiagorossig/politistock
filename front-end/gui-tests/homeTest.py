import unittest
import sys

from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By


# Inspired from Texas Votes 

#to run this do python3 homeTest.py

URL = "https://www.politistock.me/"

class NavBarTests(unittest.TestCase):

    #set up the chrome dummy for the tests
    @classmethod
    def setUp(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--disable-dev-shm-usage')

        self.driver = webdriver.Firefox(options=firefox_options)
        self.driver.get ('https://www.politistock.me/')


    #quit gracefully
    @classmethod
    def tearDown(self) :
        self.driver.quit()

    
    # Testing home link on NavBar
    def testHome(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Home').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/')
    
    def testAbout(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'About').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/about/')
    
    def testPoliticians(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Politicians').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/politicians/')
    
    def testStocks(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Stocks').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/stocks/')
    
    def testCompanies(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Companies').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/companies/')
    
    def testLinksAbout(self) :
        self.driver.get("https://www.politistock.me/about/")
        self.driver.find_elements(By.CLASS_NAME, 'Cards_float__DkC-G')[5].click()
        self.assertEqual(self.driver.current_url, 'https://developers.google.com/web/tools/puppeteer/get-started')
    
    def testGoingBack(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Companies').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/companies/')
        self.driver.back()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/')
    
    def testOtherStockButton(self) :
        self.driver.get(URL)
        self.driver.find_element(By.XPATH, '//button[text()="See All Stocks"]').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/stocks')
    
    def testOtherPoliticianButton(self) :
        self.driver.get(URL)
        self.driver.find_element(By.XPATH, '//button[text()="See All Politicians"]').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/politicians')
    
    def testOtherCompanyButton(self) :
        self.driver.get(URL)
        self.driver.find_element(By.XPATH, '//button[text()="See All Companies"]').click()
        self.assertEqual(self.driver.current_url, 'https://www.politistock.me/companies')



# If running this script directly
if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'])
