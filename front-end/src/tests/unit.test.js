import React from 'react';
import { configure, shallow } from 'enzyme';
import About from '../screens/About/About';
import Splash from '../screens/Home/Home';
import PolPage from '../screens/Politicians/Instance/PoliticianInstance';
import StockIn from '../screens/Stocks/Instance/StockInstance';
import Company from '../screens/Companies/Instance/CompaniesInstance';
import PoliticianCard from '../components/Cards/PoliticianCard';
import StockCard from '../components/Cards/StockCard';
import CompanyCard from '../components/Cards/CompanyCard';
import PersonCard from '../components/Cards/PersonCard';
import ModelCard from '../components/Cards/ModelCard';
import Adapter from 'enzyme-adapter-react-16';
import { Router } from 'react-bootstrap-icons';

//Change the directories when everything merges, also don't forget
//to import the cards

configure({ adapter: new Adapter() });

//Seeing if the about page renders

describe('Testing If The Various Pages can Render', () => {
	test('Checking if About Page Renders', () => {
		const test_about = shallow(
			<Router>
				{' '}
				<About />{' '}
			</Router>
		);
		expect(test_about).toMatchSnapshot();
	});

	test('Checking if Splash/Home Renders', () => {
		const test_splash = shallow(
			<Router>
				{' '}
				<Splash />{' '}
			</Router>
		);
		expect(test_splash).toMatchSnapshot();
	});

	test('Checking if Politician Instance Renders', () => {
		const test_pol = shallow(
			<Router>
				{' '}
				<PolPage />{' '}
			</Router>
		);
		expect(test_pol).toMatchSnapshot();
	});

	test('Checking if Stock Renders', () => {
		const test_stock = shallow(
			<Router>
				{' '}
				<StockIn />{' '}
			</Router>
		);
		expect(test_stock).toMatchSnapshot();
	});

	test('Checking if Company Renders', () => {
		const test_comp = shallow(
			<Router>
				{' '}
				<Company />{' '}
			</Router>
		);
		expect(test_comp).toMatchSnapshot();
	});
});

//Future Testing for the Card Renders

//Uncomment this when everything is connected, then fill info for cards

describe('Testing the different card components', () => {
	test('Testing Politician Card', () => {
		const pol_card = shallow(
			<PoliticianCard
				imgs="https://fivethirtyeight.com/wp-content/uploads/2015/07/natesilver2_light.jpg?w=575"
				name="Ramon Marquer"
				party="Democratic"
				id="Hello"
				traded_volume={100000}
				num_trades={10}
				chamber="house"
				state="texas"
			/>
		);
		expect(pol_card).toMatchSnapshot();
	});

	test('Testing Stock Card', () => {
		const stock_card = shallow(
			<StockCard
				id="bigboi"
				companyid="hellothere"
				marketcap={42000}
				displayName="dog"
				longName="doggggg"
				exchange="your_mom"
				fiftyTwoWeekHigh="22"
				fiftyTwoWeekLow="1"
			/>
		);
		expect(stock_card).toMatchSnapshot();
	});

	test('Testing Company Card', () => {
		const comp_card = shallow(
			<CompanyCard
				id="dude"
				name="BigBoi"
				category={'bigg'}
				foundedYear={3}
				geo={'asdf'}
				logo="https://fivethirtyeight.com/wp-content/uploads/2015/07/natesilver2_light.jpg?w=575"
				politicians={'asdfasd'}
				metrics="2"
			/>
		);
		expect(comp_card).toMatchSnapshot();
	});

	test('Testing Person Card', () => {
		const person_card = shallow(
			<PersonCard
				name="Ruhmon"
				username="something"
				email="something else"
				role="something again"
				bio="i like something"
				commits={1}
				issues={2}
				tests={5}
			/>
		);
		expect(person_card).toMatchSnapshot();
	});

	test('Testing Model Card', () => {
		const model_card = shallow(
			<ModelCard
				img="https://i.guim.co.uk/img/media/fe1e34da640c5c56ed16f76ce6f994fa9343d09d/0_174_3408_2046/master/3408.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=0d3f33fb6aa6e0154b7713a00454c83d"
				title="Dog picture"
				text="Picture of a dog"
				buttonText="Dog"
				href="www.google.com"
			/>
		);
		expect(model_card).toMatchSnapshot();
	});
});
