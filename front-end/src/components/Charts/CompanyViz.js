import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend, Title } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import { Typography } from '@mui/material';

var formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	notation: 'compact',
	maximumFractionDigits: 1,
});

function CompanyViz({ companies }) {
	ChartJS.register(ArcElement, Tooltip, Legend, Title);
	const keys = Object.keys(companies);
	const ranges = Object.values(companies);

	const options = {
		responsive: true,
		maintainAspectRatio: false,
		plugins: {
			title: {
				display: true,
				text: 'Company Investment Distribution',
			},
			tooltip: {
				callbacks: {
					label: (context) => {
						const { label, dataIndex, dataset } = context;
						const { ranges } = dataset;
						const minMax = ranges[dataIndex];
						const result = `${formatter.format(
							minMax[0]
						)} - ${formatter.format(minMax[1])}`;
						return `${label}\n${result}`;
					},
				},
			},
		},
	};
	const dataVals = ranges.map((range) => range[1]);
	const data = {
		labels: Object.keys(companies),
		datasets: [
			{
				label: '# of Votes',
				data: dataVals,
				ranges,
				keys,
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)',
					'rgba(237, 161, 255, 0.2)',
				],
				borderColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)',
					'rgba(237, 161, 255, 1)',
				],
				borderWidth: 1,
			},
		],
	};

	return keys.length > 0 ? (
		<>
			<Doughnut options={options} data={data} />
			<Typography variant="caption">
				**Approximate Estimation of Investment Distribution
			</Typography>
		</>
	) : (
		<div
			style={{
				width: '100%',
				height: '100%',
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			No Company Investment Data Found
		</div>
	);
}

export default CompanyViz;
