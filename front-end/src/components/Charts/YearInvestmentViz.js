import React from 'react';
import {
	Chart as ChartJS,
	CategoryScale,
	LinearScale,
	BarElement,
	Title,
	Tooltip,
	Legend,
} from 'chart.js';
import { Typography } from '@mui/material';
import { Bar } from 'react-chartjs-2';

function YearInvestedViz({ years }) {
	ChartJS.register(
		CategoryScale,
		LinearScale,
		BarElement,
		Title,
		Tooltip,
		Legend
	);
	const keys = Object.keys(years);

	const options = {
		responsive: true,
		plugins: {
			title: {
				display: true,
				text: 'Volume Per Year',
			},
		},
	};

	const data = {
		keys,
		datasets: [
			{
				label: 'Amount Invested',
				data: years,
				backgroundColor: 'rgba(255, 250, 132, 0.5)',
			},
		],
	};
	return keys.length > 0 ? (
		<>
			<Bar data={data} options={options} />
			<Typography variant="caption">
				**Approximate Estimation of Volume Per Year
			</Typography>
		</>
	) : (
		<div
			style={{
				width: '100%',
				height: '100%',
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			No Investment Data Found
		</div>
	);
}
export default YearInvestedViz;
