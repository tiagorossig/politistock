import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend, Title } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import { Typography } from '@mui/material';

function LocationsViz({ locations }) {
	ChartJS.register(ArcElement, Tooltip, Legend, Title);

	const keys = Object.keys(locations);
	const ranges = Object.values(locations);

	const options = {
		responsive: true,
		maintainAspectRatio: false,
		plugins: {
			title: {
				display: true,
				text: 'Location Investment Distribution',
			},
			tooltip: {
				callbacks: {
					label: (context) => {
						const { label, dataIndex, dataset } = context;
						const { data } = dataset;
						const minMax = data[dataIndex];
						const result = `${minMax}`;
						return `${label}\n${result}`;
					},
				},
			},
		},
	};

	const data = {
		labels: Object.keys(locations),
		datasets: [
			{
				label: '# of Companies At Location',
				data: ranges,
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)',
					'rgba(123, 341, 134, 0.2)',
				],
				borderColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)',
					'rgba(123, 341, 134, 1)',
				],
				borderWidth: 1,
			},
		],
	};

	return keys.length > 0 ? (
		<>
			<Doughnut data={data} options={options} />
			<Typography variant="caption">
				**Approximate Estimation of Location Distribution
			</Typography>
		</>
	) : (
		<div
			style={{
				width: '100%',
				height: '100%',
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			No Location Data Found
		</div>
	);
}

export default LocationsViz;
