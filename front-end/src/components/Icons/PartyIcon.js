import React from 'react';
import styles from './Icons.module.css';

function PartyIcon({ party }) {
	const partyClassName =
		' ' +
		(party === 'd'
			? styles.democrat
			: party === 'r'
			? styles.republican
			: styles.independent);
	const partyAbbr = party === 'd' ? 'D' : party === 'r' ? 'R' : 'I';

	return <div className={styles.icon + partyClassName}>{partyAbbr}</div>;
}

export default PartyIcon;
