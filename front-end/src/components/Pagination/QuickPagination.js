import React from 'react';
import { ArrowBackIosNew, ArrowForwardIos } from '@mui/icons-material';
import { IconButton, Select, MenuItem } from '@mui/material';

function QuickPagination({ pagination, posts, setPosts }) {
	return (
		<div>
			<IconButton
				onClick={() => {
					const elem =
						pagination.current.querySelectorAll('ul > li > a')[0];
					if (elem.ariaDisabled === null) {
						elem.click();
					}
				}}
			>
				<ArrowBackIosNew />
			</IconButton>
			<IconButton
				onClick={() => {
					const elements =
						pagination.current.querySelectorAll('ul > li > a');
					const elem = elements[elements.length - 1];
					if (elem.ariaDisabled === null) {
						elem.click();
					}
				}}
			>
				<ArrowForwardIos />
			</IconButton>
			<Select
				value={posts}
				labelId="demo-simple-select-helper-label"
				id="demo-simple-select-helper"
				label="Per Page: "
				onChange={(e) => setPosts(e.target.value)}
			>
				<MenuItem value={12}>
					<em>12</em>
				</MenuItem>
				<MenuItem value={24}>
					<em>24</em>
				</MenuItem>
				<MenuItem value={48}>
					<em>48</em>
				</MenuItem>
			</Select>
		</div>
	);
}

export default QuickPagination;
