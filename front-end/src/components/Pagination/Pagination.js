import React from 'react';
import { Pagination as MUIPagination, PaginationItem } from '@mui/material';
import { Link } from 'react-router-dom';
import styles from './Pagination.module.css';

function Pagination({ postsPerPage, totalPosts, reference, page, setPage }) {
	return (
		<div className={styles.paginationWrapper}>
			<div className={styles.b}>Total {totalPosts} items </div>
			<MUIPagination
				page={page}
				ref={reference}
				count={Math.ceil(totalPosts / postsPerPage)}
				className={styles.b}
				renderItem={(item) => (
					<PaginationItem
						component={Link}
						to={`?page=${item.page}&perPage=${postsPerPage}`}
						{...item}
						onClick={() => {
							setPage(item.page);
						}}
					/>
				)}
			/>
		</div>
	);
}

export default Pagination;
