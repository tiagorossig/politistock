import { Navbar as NavBootstrap } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import logo from '../../images/icon.png';

function Navbar() {
	return (
		<NavBootstrap bg="dark" variant="dark" expand="sm">
			<NavBootstrap.Brand>
				<img
					src={logo}
					className="logo"
					width="75px"
					height="75px"
					alt=""
				/>
				PolitiStock
			</NavBootstrap.Brand>
			<NavBootstrap.Toggle />
			<NavBootstrap.Collapse>
				<Nav>
					<Nav.Link href="/">Home</Nav.Link>
					<Nav.Link href="/about">About</Nav.Link>
					<Nav.Link href="/politicians">Politicians</Nav.Link>
					<Nav.Link href="/stocks">Stocks</Nav.Link>
					<Nav.Link href="/companies">Companies</Nav.Link>
					<Nav.Link href="/search">Search</Nav.Link>
					<Nav.Link href="/politistockViz">
						Politistock Visualizations
					</Nav.Link>
					<Nav.Link href="/providerViz">
						Provider Visualizations
					</Nav.Link>
				</Nav>
			</NavBootstrap.Collapse>
		</NavBootstrap>
	);
}

export default Navbar;
