import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import {
	Bar,
	BarChart,
	CartesianGrid,
	XAxis,
	YAxis,
	Tooltip,
	Label,
	ResponsiveContainer,
} from 'recharts';

const getNMostPopularStocks = (data, N) => {
	const result = {};
	Object.keys(data)
		.sort((a, b) => data[b] - data[a])
		.forEach((key, i) => {
			if (i < N) {
				result[key] = data[key];
			}
		});
	return result;
};

const requestOptions = {
	method: 'GET',
	redirect: 'follow',
};

function StockCount() {
	const navigate = useNavigate();
	const [data, setData] = useState();
	const [polData, setPolData] = useState();
	const [loaded, setLoaded] = useState(false);
	useEffect(() => {
		const fetchPoliticians = async () => {
			fetch('https://api.politistock.me/politicians', requestOptions)
				.then((response) => response.json())
				.then((polResult) => setPolData(polResult))
				.catch((error) => console.log('error', error));
		};
		fetchPoliticians();
	}, []);

	useEffect(() => {
		const fetchStocks = async () => {
			fetch('https://api.politistock.me/stocks', requestOptions)
				.then((response) => response.json())
				.then((stocksResult) => {
					delete stocksResult.num_entries;
					const allStockData = new Map();
					const stockCount = {};
					const polsDataMap = new Map(Object.entries(polData));

					// build data to feed into BarGraph
					for (const [key, value] of Object.entries(stocksResult)) {
						var dems = 0;
						var reps = 0;

						const stockDataMap = new Map(Object.entries(value));
						stockDataMap.get('politicians').forEach((item) => {
							const polDataMap = new Map(
								Object.entries(polsDataMap.get(item))
							);
							if (polDataMap.get('party') == 'democrat') {
								dems++;
							} else {
								reps++;
							}
						});

						stockCount[key] = dems + reps;
						allStockData.set(key, { D: dems, R: reps });
					}

					// only use data from 50 most popular stocks
					const result = new Array();
					const popularStocks = getNMostPopularStocks(stockCount, 50);
					for (const [ticker, count] of Object.entries(
						popularStocks
					)) {
						const partyCounts = new Map(
							Object.entries(allStockData.get(ticker))
						);
						result.push({
							name: ticker,
							D: partyCounts.get('D'),
							R: partyCounts.get('R'),
						});
					}

					setData(result);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		if (typeof polData !== 'undefined') {
			fetchStocks();
		}
	}, [polData]);

	return loaded ? (
		<ResponsiveContainer width="80%" height={600}>
			<BarChart
				data={data}
				margin={{
					top: 20,
					bottom: 20,
				}}
				onClick={({ activePayload }) => {
					const ticker = activePayload?.pop().payload.name;
					if (ticker) {
						navigate(`/stocks/${ticker}`);
					}
				}}
			>
				<CartesianGrid strokeDasharray="3 3" />
				<XAxis dataKey="name" tick={false}>
					<Label
						value="Ticker"
						position="insideBottom"
						style={{ textAnchor: 'middle' }}
					/>
				</XAxis>
				<YAxis>
					<Label
						angle={-90}
						value="Number of Holdings"
						position="insideLeft"
						style={{ textAnchor: 'middle' }}
					/>
				</YAxis>
				<Tooltip />
				<Bar dataKey="D" stackId="a" fill="#0000ff" />
				<Bar dataKey="R" stackId="a" fill="#ff0803" />
			</BarChart>
		</ResponsiveContainer>
	) : (
		<div
			style={{
				marginTop: '25px',
				justifyContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
			}}
		>
			<CircularProgress />
		</div>
	);
}

export default StockCount;
