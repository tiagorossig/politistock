import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import DonutChart from './donut-chart';

function SectorCount() {
	const [data, setData] = useState();
	const [loaded, setLoaded] = useState(false);
	const colors = [
		'#9c82d4',
		'#c981b2',
		'#b14891',
		'#ff6d00',
		'#ff810a',
		'#ff9831',
		'#ffb46a',
		'#ff9a91',
		'#ff736a',
		'#f95d54',
		'#ff4136',
		'#c4c4cd',
	];

	const formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
	});

	useEffect(() => {
		var requestOptions = {
			method: 'GET',
			redirect: 'follow',
		};
		const fetchStocks = async () => {
			const sector_map = new Map();

			fetch('https://api.politistock.me/stocks', requestOptions)
				.then((response) => response.json())
				.then((result) => {
					delete result.num_entries;
					for (const [key, value] of Object.entries(result)) {
						const oldVal = sector_map.has(value.sector)
							? sector_map.get(value.sector)
							: 0;

						sector_map.set(value.sector, oldVal + value.marketCap);
					}
					const mapSorted = new Map(
						[...sector_map.entries()].sort((a, b) => b[1] - a[1])
					);
					const manuKeys = mapSorted.keys();
					const shorterMap = new Map();
					for (let i = 0; i < 12; i++) {
						const curr = manuKeys.next().value;
						shorterMap.set(curr, mapSorted.get(curr));
					}

					setData(
						Array.from(shorterMap).map(([name, value]) => {
							return {
								name: name,
								value: value,
							};
						})
					);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		fetchStocks();
	}, []);

	return loaded ? (
		<div style={{ maxHeight: '900px', maxWidth: '900px' }}>
			<DonutChart
				data={data}
				format={true}
				formatter={(val) => formatter.format(val).toString()}
				change_color={true}
				new_colors={colors}
			/>
		</div>
	) : (
		<div
			style={{
				marginTop: '25px',
				justifyContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
			}}
		>
			<CircularProgress />
		</div>
	);
}

export default SectorCount;
