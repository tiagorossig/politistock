import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import DonutChart from './donut-chart';

function DosageCount() {
	const [data, setData] = useState();
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		var requestOptions = {
			method: 'GET',
			redirect: 'follow',
		};
		const fetchMed = async () => {
			const dos_map = new Map();

			fetch('https://api.pharmabase.me/drugs', requestOptions)
				.then((response) => response.json())
				.then((result) => {
					result.data.forEach((element) => {
						const method = element.dosage_form.split(',')[0];
						const oldVal = dos_map.has(method)
							? dos_map.get(method)
							: 0;
						dos_map.set(method, oldVal + 1);
					});

					const mapSorted = new Map(
						[...dos_map.entries()].sort((a, b) => b[1] - a[1])
					);
					const manuKeys = mapSorted.keys();
					const shorterMap = new Map();
					for (let i = 0; i < 12; i++) {
						const curr = manuKeys.next().value;
						shorterMap.set(curr, mapSorted.get(curr));
					}

					setData(
						Array.from(shorterMap).map(([name, value]) => {
							return {
								name: name,
								value: value,
							};
						})
					);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		fetchMed();
	}, []);

	return loaded ? (
		<div style={{ maxHeight: '900px', maxWidth: '900px' }}>
			<DonutChart data={data} />
		</div>
	) : (
		<div
			style={{
				maxWidth: '1250px',
				marginTop: '25px',
				justifyContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
			}}
		>
			<CircularProgress />
		</div>
	);
}

export default DosageCount;
