import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import BubbleChart from './react-bubble-chart-d3';

function ManufacturerDrugs({ width }) {
	const [data, setData] = useState();
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		var requestOptions = {
			method: 'GET',
			redirect: 'follow',
		};
		const fetchManu = async () => {
			const manu_map = new Map();

			fetch('https://api.pharmabase.me/drugs', requestOptions)
				.then((response) => response.json())
				.then((result) => {
					result.data.forEach((element) => {
						const manu = element.manufacturer_name;
						const oldVal = manu_map.has(manu)
							? manu_map.get(manu)
							: 0;
						manu_map.set(manu, oldVal + 1);
					});
					const mapSorted = new Map(
						[...manu_map.entries()].sort((a, b) => b[1] - a[1])
					);
					const manuKeys = mapSorted.keys();
					const shorterMap = new Map();
					for (let i = 0; i < 25; i++) {
						const curr = manuKeys.next().value;
						shorterMap.set(curr, mapSorted.get(curr));
					}

					setData(
						Array.from(shorterMap).map(([name, value]) => {
							return {
								label: name,
								value: value,
							};
						})
					);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		fetchManu();
	}, []);

	return loaded ? (
		<BubbleChart
			graph={{
				zoom: 0.7,
				offsetX: 0.0,
				offsetY: 0.0,
			}}
			showLegend={false}
			width={width}
			format={false}
			height={width * 0.8}
			valueFont={{
				family: 'Arial',
				size: 12,
				color: '#fff',
				weight: 'bold',
			}}
			labelFont={{
				family: 'Arial',
				size: 10,
				color: '#fff',
				weight: '200',
			}}
			data={data}
		/>
	) : (
		<div
			style={{
				marginTop: '25px',
				justifyContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
			}}
		>
			<CircularProgress />
		</div>
	);
}

export default ManufacturerDrugs;
