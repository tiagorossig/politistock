import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import Scatterplot from './Scatterplot.js';

function ElementScatterplot({ size }) {
	const [data, setData] = useState([]);
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		var requestOptions = {
			method: 'GET',
			redirect: 'follow',
		};
		const fetchElems = async () => {
			const elems = [];

			fetch('https://api.pharmabase.me/elements', requestOptions)
				.then((response) => response.json())
				.then((result) => {
					result.data.forEach((element) => {
						elems.push({
							id: element.name,
							ar: element.atomic_radius,
							ie: element.ionization_en,
						});
					});
					setData(elems);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		fetchElems();
	}, []);

	return loaded ? (
		<Scatterplot data={data} size={size} />
	) : (
		<div
			style={{
				maxWidth: '1250px',
				marginTop: '25px',
				justifyContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
			}}
		>
			<CircularProgress />
		</div>
	);
}

export default ElementScatterplot;
