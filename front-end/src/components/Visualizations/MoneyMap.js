import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import BubbleChart from './react-bubble-chart-d3';

function MoneyMap({ width }) {
	const navigate = useNavigate();
	const [data, setData] = useState();

	const [loaded, setLoaded] = useState(false);

	const formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
	});

	useEffect(() => {
		var requestOptions = {
			method: 'GET',
			redirect: 'follow',
		};
		const fetchPosts = async () => {
			fetch('https://api.politistock.me/politicians', requestOptions)
				.then((response) => response.json())
				.then((result) => {
					delete result.num_entries;
					const polMap = new Map();
					for (const [key, value] of Object.entries(result)) {
						const oldVal = polMap.has(value.state)
							? polMap.get(value.state)
							: 0;

						polMap.set(value.state, oldVal + value.traded_volume);
					}
					setData(
						Array.from(polMap).map(([name, value]) => {
							return {
								label: name
									.match(/(\w)+/g)
									.map(
										(word) =>
											word.charAt(0).toUpperCase() +
											word.substring(1, word.length)
									)
									.join(' '),
								value: value,
							};
						})
					);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		fetchPosts();
	}, []);

	return loaded ? (
		<BubbleChart
			graph={{
				zoom: 0.7,
				offsetX: 0.0,
				offsetY: 0.0,
			}}
			showLegend={false}
			format={true}
			formatter={(val) => formatter.format(val).toString()}
			width={width}
			height={width * 0.8}
			valueFont={{
				family: 'Arial',
				size: 12,
				color: '#fff',
				weight: 'bold',
			}}
			labelFont={{
				family: 'Arial',
				size: 16,
				color: '#fff',
				weight: 'bold',
			}}
			data={data}
			bubbleClickFun={(state) => {
				navigate(`/politicians/?state=${state}`);
			}}
		/>
	) : (
		<div
			style={{
				marginTop: '25px',
				justifyContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
			}}
		>
			<CircularProgress />
		</div>
	);
}

export default MoneyMap;
