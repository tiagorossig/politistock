import React, { Component } from 'react';
import * as d3 from 'd3';

class Scatterplot extends Component {
	constructor(props) {
		super(props);
		this.chRef = React.createRef();
	}

	// Chart load after component Mount
	componentDidMount() {
		this.drawPlot();
	}

	componentDidUpdate() {
		this.drawPlot();
	}

	// adapted from https://d3-graph-gallery.com/graph/scatter_basic.html
	drawPlot() {
		const { data, size } = this.props;

		// Reset the svg element to a empty state.
		this.chRef.current.innerHTML = '';

		var margin = { top: 30, right: 30, bottom: 60, left: 60 };
		var width = size - margin.left - margin.right;
		var height = size - margin.top - margin.bottom;

		// create svg
		const svg = d3
			.select(this.chRef.current)
			.append('svg')
			.attr('width', width + margin.left + margin.right)
			.attr('height', height + margin.top + margin.bottom)
			.append('g')
			.attr(
				'transform',
				'translate(' + margin.left + ',' + margin.top + ')'
			);

		// add x-axis
		var x = d3.scaleLinear().domain([0, 240]).range([0, width]);
		svg.append('g')
			.attr('transform', 'translate(0,' + height + ')')
			.call(d3.axisBottom(x));

		// add y-axis
		var y = d3.scaleLinear().domain([0, 2400]).range([height, 0]);
		svg.append('g').call(d3.axisLeft(y));

		// add points
		svg.append('g')
			.selectAll('dot')
			.data(data)
			.enter()
			.append('circle')
			.attr('cx', (d) => x(d.ar))
			.attr('cy', (d) => y(d.ie))
			.attr('r', 4)
			// generates random color, taken from https://stackoverflow.com/questions/1484506/random-color-generator
			.style(
				'fill',
				(d) => '#' + (((1 << 24) * Math.random()) | 0).toString(16)
			)
			.append('title')
			.text(
				(d) =>
					`Ionization Energy of ${d.id}: ${d.ie} kJ/mol - Atomic Radius of ${d.id}: ${d.ar} pm`
			);

		// add axis labels
		svg.append('text')
			.attr('x', width / 2)
			.attr('y', height + 40)
			.style('text-anchor', 'middle')
			.text('Atomic Radius (pm)');

		svg.append('text')
			.attr('transform', 'rotate(-90)')
			.attr('x', -height / 2)
			.attr('y', -45)
			.style('text-anchor', 'middle')
			.text('Ionization Energy (kJ/mol)');
	}

	render() {
		const { size } = this.props;

		return <svg width={size} height={size} ref={this.chRef} />;
	}
}

export default Scatterplot;
