import React from 'react';
import { Card, List, ListItem } from '@mui/material';
import { CheckCircle, Git, ListCheck, Share } from 'react-bootstrap-icons';
import styles from './Cards.module.css';

function GitlabCard({ commits, issues, tests, branches }) {
	const size = 50;
	return (
		<a
			className={styles.float}
			href="https://gitlab.com/tiagorossig/politistock"
		>
			<Card className={styles.gitCard} style={{ width: '18rem' }}>
				<List variant="flush">
					<ListItem className={styles.gitElement}>
						<div className={styles.gitWrapper}>
							<Git size={size} />
							<h3>Total Commits: {commits}</h3>
						</div>
					</ListItem>
					<ListItem className={styles.gitElement}>
						<div className={styles.gitWrapper}>
							<ListCheck size={size} />
							<h3>Total Issues: {issues}</h3>
						</div>
					</ListItem>
					<ListItem className={styles.gitElement}>
						<div className={styles.gitWrapper}>
							<Share size={size} />
							<h3>Total Branches: {branches}</h3>
						</div>
					</ListItem>
					<ListItem className={styles.gitElement}>
						<div className={styles.gitWrapper}>
							<CheckCircle size={size} />
							<h3>Total Tests: {tests}</h3>
						</div>
					</ListItem>
				</List>
			</Card>
		</a>
	);
}

export default GitlabCard;
