import React, { useEffect, useState } from 'react';
import { Card, CardContent, Typography, Skeleton } from '@mui/material';
import { Link } from 'react-router-dom';
import styles from './Cards.module.css';

function StockCard({
	id,
	companyId,
	marketCap,
	displayName,
	longName,
	exchange,
	fiftyTwoWeekHigh,
	fiftyTwoWeekLow,
}) {
	const [img, setImg] = useState();
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		setLoaded(false);
		var requestOptions = {
			method: 'GET',
			redirect: 'follow',
		};
		if (companyId !== 'None') {
			fetch(
				'http://api.politistock.me/company/' + companyId,
				requestOptions
			)
				.then((response) => response.json())
				.then((result) => {
					const { logo } = result[companyId];
					if (logo !== 'None') {
						fetch(logo)
							.then((response) => response.blob())
							.then((imageBlob) => {
								const imageObjectURL =
									URL.createObjectURL(imageBlob);
								setImg(imageObjectURL);
								setLoaded(true);
							})
							.catch((error) => console.log('error', error));
					}
				})
				.catch((error) => console.log('error', error));
		}
	}, [companyId]);

	var formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
	});

	const name =
		displayName !== 'N/A'
			? displayName
			: longName !== 'N/A'
			? longName
			: '';

	return (
		<Link className={styles.float} to={id}>
			<Card className={styles.companyCard}>
				{loaded ? (
					<div
						style={{ width: '250px', height: '250px' }}
						className={styles.imgCover}
					>
						<img
							className={styles.companyImg}
							alt={name}
							src={img}
						/>
					</div>
				) : (
					<Skeleton
						variant="rectangular"
						width={'100%'}
						height={'250px'}
					/>
				)}
				<CardContent>
					<Typography
						className={styles.stockTitle}
						variant="h5"
						component="div"
					>
						{id}
					</Typography>
					<Typography
						className={styles.stockTitle}
						variant="h6"
						component="div"
						noWrap={true}
					>
						{name}
					</Typography>
					<div className={styles.infoWrapper}>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Market Cap
							</Typography>
							{formatter.format(marketCap)}
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Exchange
							</Typography>
							{exchange}
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								52 Week High
							</Typography>
							{formatter.format(fiftyTwoWeekHigh)}
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								52 Week Low
							</Typography>
							{formatter.format(fiftyTwoWeekLow)}
						</div>
					</div>
				</CardContent>
			</Card>
		</Link>
	);
}

export default StockCard;
