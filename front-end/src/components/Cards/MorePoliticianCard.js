import { Card, CardContent, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import styles from './Cards.module.css';

function MorePolitician({ numResults, search }) {
	return (
		<Link
			className={styles.float}
			to={'/politicians' + (search !== '' ? '?search=' + search : '')}
		>
			<Card className={styles.politicianCard}>
				<CardContent
					sx={{
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						height: '100%',
					}}
				>
					<Typography variant="h6">
						See all {numResults} Politicians in your search
					</Typography>
				</CardContent>
			</Card>
		</Link>
	);
}

export default MorePolitician;
