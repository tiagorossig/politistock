import React from 'react';
import { Card, CardContent, Typography, CardMedia } from '@mui/material';
import styles from './Cards.module.css';
import { CheckCircle, Git, ListCheck } from 'react-bootstrap-icons';

function PersonCard({
	name,
	link,
	username,
	email,
	role,
	phaseLeader,
	bio,
	img,
	commits,
	issues,
	tests,
}) {
	return (
		<a className={styles.float} href={link}>
			<Card className={styles.personCard} sx={{ width: '18rem' }}>
				<CardContent>
					<CardMedia
						component="img"
						className={styles.roundedImg}
						image={img}
						alt="bio picture"
					/>
					<Typography variant="h6" component="div">
						{name}
					</Typography>
					<div>
						{phaseLeader === undefined ? (
							<Typography variant="subtitle1" component="div">
								{role}
							</Typography>
						) : (
							<div className={styles.pairParent}>
								<Typography
									className={styles.sideBySide}
									variant="subtitle1"
									component="div"
								>
									{role}
								</Typography>
								<Typography
									className={styles.sideBySide}
									variant="subtitle1"
									component="div"
								>
									{'Phase ' + phaseLeader + ' Leader'}
								</Typography>
							</div>
						)}
					</div>
					<Typography
						variant="body"
						color="text.secondary"
						component="div"
					>
						{bio}
					</Typography>
					<hr></hr>
					<div className={styles.statsWrapper}>
						<div className={styles.miniStats}>
							Commits
							<Git />
							{commits}
						</div>
						<div className={styles.miniStats}>
							Issues
							<ListCheck />
							{issues}
						</div>
						<div className={styles.miniStats}>
							Tests
							<CheckCircle />
							{tests}
						</div>
					</div>
				</CardContent>
			</Card>
		</a>
	);
}

export default PersonCard;
