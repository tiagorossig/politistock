import React from 'react';
import {
	Card,
	CardContent,
	CardMedia,
	Typography,
	Button,
} from '@mui/material';
import { Link } from 'react-router-dom';
import styles from './Cards.module.css';

function ModelCard({ img, title, text, buttonText, href }) {
	return (
		<Card className={styles.modelCard} style={{ width: '18rem' }}>
			<CardMedia component="img" image={img} alt={title} />
			<CardContent>
				<Typography variant="h6">{title}</Typography>
				<Typography variant="body">{text}</Typography>
			</CardContent>
			<Link className={styles.buttonLink} to={href}>
				<Button variant="contained">{buttonText}</Button>
			</Link>
		</Card>
	);
}

export default ModelCard;
