import { Card, CardContent, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import PartyIcon from '../Icons/PartyIcon';
import styles from './Cards.module.css';
import Highlight from '../Search/Highlight';

function PoliticianCard({
	search,
	age,
	chamber,
	companies,
	dob,
	id,
	imgs,
	last_trade,
	name,
	num_trades,
	party,
	preferred_name,
	role,
	state,
	tickers,
	traded_volume,
	trades,
}) {
	var formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
	});

	return (
		<Link className={styles.float} to={'/politicians/' + id}>
			<PartyIcon party={party.charAt(0)} />
			<Card className={styles.politicianCard}>
				<div
					style={{ width: '320px', height: '350px' }}
					className={styles.imgCover}
				>
					<img
						className={styles.politicianImg}
						alt={name}
						src={imgs}
					/>
				</div>
				<CardContent>
					<Typography
						className={styles.polTitle}
						variant="h5"
						component="div"
					>
						<Highlight text={preferred_name} search={search} />
					</Typography>
					<div className={styles.infoWrapper}>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								State
							</Typography>
							<Highlight
								text={state
									.match(/(\w)+/g)
									.map(
										(word) =>
											word.charAt(0).toUpperCase() +
											word.substring(1, word.length)
									)
									.join(' ')}
								search={search}
							/>
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Chamber
							</Typography>
							<Highlight
								text={
									chamber.charAt(0).toUpperCase() +
									chamber.substring(1, chamber.length)
								}
								search={search}
							/>
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Trades
							</Typography>
							<Highlight
								text={num_trades.toString()}
								search={search}
							/>
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Trade Volume
							</Typography>
							<Highlight
								text={formatter
									.format(traded_volume)
									.toString()}
								search={search}
							/>
						</div>
					</div>
				</CardContent>
			</Card>
		</Link>
	);
}

export default PoliticianCard;
