import React, { useEffect, useState } from 'react';
import { Card, CardContent, Typography, Skeleton } from '@mui/material';
import { Link } from 'react-router-dom';
import styles from './Cards.module.css';
import noImage from '../../images/noimage.png';
import Highlight from '../Search/Highlight';

function CompanyCard({
	id,
	search,
	name,
	legalName,
	domain,
	category,
	tags,
	description,
	foundedYear,
	location,
	timeZone,
	utcOffset,
	geo,
	logo,
	politicians,
	twitter,
	type,
	ticker,
	identifiers,
	phone,
	metrics,
	parent,
	ultimateParent,
}) {
	const [img, setImg] = useState();
	const [loaded, setLoaded] = useState(false);
	useEffect(() => {
		setLoaded(false);
		if (logo !== 'None') {
			fetch(logo)
				.then((response) => response.blob())
				.then((imageBlob) => {
					const imageObjectURL = URL.createObjectURL(imageBlob);
					setImg(imageObjectURL);
					setLoaded(true);
				})
				.catch((error) => {
					console.log('error', error);
					setImg(noImage);
					setLoaded(true);
				});
		} else {
			setImg(noImage);
			setLoaded(true);
		}
	}, [logo]);

	return (
		<Link className={styles.float} to={'/companies/' + id}>
			<Card className={styles.companyCard}>
				{loaded ? (
					<div
						style={{
							width: '250px',
							height: '250px',
							marginTop: '12.5px',
						}}
						className={styles.imgCover}
					>
						<img
							className={styles.companyImg}
							alt={name}
							src={img}
						/>
					</div>
				) : (
					<Skeleton
						variant="rectangular"
						width={'100%'}
						height={'250px'}
					/>
				)}
				<CardContent sx={{ padding: '2px' }}>
					<Typography variant="h5" component="div">
						<Highlight text={name} search={search} />
					</Typography>
					<div className={styles.infoBox}>
						<Typography
							variant="subtitle1"
							color="text.secondary"
							component="div"
						>
							Politician Investors:{' '}
							<Highlight
								text={politicians.length.toString()}
								search={search}
							/>
						</Typography>
					</div>
					<div className={styles.infoWrapper}>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Founded
							</Typography>
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Industry
							</Typography>
						</div>
						<div className={styles.infoBox}>
							<Highlight
								text={
									foundedYear === 0
										? 'N/A'
										: foundedYear.toString()
								}
								search={search}
							/>
						</div>
						<div className={styles.infoBox}>
							{category.industry}
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								HQ Location
							</Typography>
							<Highlight
								text={
									(geo.city ? geo.city + ', ' : '') +
									(geo.stateCode ?? geo.country)
								}
								search={search}
							/>
						</div>
						<div className={styles.infoBox}>
							<Typography
								variant="subtitle1"
								color="text.secondary"
								component="div"
							>
								Company Size
							</Typography>
							<Highlight
								text={metrics.employeesRange}
								search={search}
							/>
						</div>
					</div>
				</CardContent>
			</Card>
		</Link>
	);
}

export default CompanyCard;
