import React from 'react';
import { Card } from '@mui/material';
import styles from './Cards.module.css';

function SourceCodeCard({ img, link }) {
	return (
		<a className={styles.float} href={link}>
			<Card
				className={styles.sourceCard}
				style={{ width: '18rem', height: '18rem' }}
			>
				<div
					className={styles.imgCover}
					style={{ width: '215px', height: '215px' }}
				>
					<img
						className={styles.sourceImg}
						alt="Link to code source"
						src={img}
					/>
				</div>
			</Card>
		</a>
	);
}

export default SourceCodeCard;
