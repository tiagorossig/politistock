import { Card, CardContent, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import styles from './Cards.module.css';

function MoreCompany({ numResults, search }) {
	return (
		<Link
			className={styles.float}
			to={'/companies' + (search !== '' ? '?search=' + search : '')}
		>
			<Card className={styles.companyCard}>
				<CardContent
					sx={{
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						height: '100%',
					}}
				>
					<Typography variant="h6">
						See all {numResults} Companies in your search
					</Typography>
				</CardContent>
			</Card>
		</Link>
	);
}

export default MoreCompany;
