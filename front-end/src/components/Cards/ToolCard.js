import React from 'react';
import { Card, CardContent, Typography } from '@mui/material';
import styles from './Cards.module.css';

function ToolCard({ title, description, img, link }) {
	return (
		<a className={styles.float} href={link}>
			<Card className={styles.toolCard} style={{ width: '18rem' }}>
				<CardContent>
					<div
						style={{ width: '215px', height: '215px' }}
						className={styles.imgCover}
					>
						<img className={styles.toolImg} src={img} alt={title} />
					</div>
					<Typography variant="h6" component="div">
						{title}
					</Typography>
					<Typography
						variant="subtitle1"
						color="text.secondary"
						component="div"
					>
						{description}
					</Typography>
				</CardContent>
			</Card>
		</a>
	);
}

export default ToolCard;
