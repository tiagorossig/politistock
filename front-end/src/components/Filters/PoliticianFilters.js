import * as React from 'react';
import {
	InputLabel,
	MenuItem,
	FormControl,
	Select,
	Button,
	TextField,
} from '@mui/material';
import {
	sortMap,
	partyMap,
	tradesMap,
	tradeVolMap,
	chamberMap,
	stateMap,
} from './FilterValues/PoliticianFilterValues';
import styles from './PoliticianFilters.module.css';
export default function PoliticianFilters(props) {
	const setPage = props.setPage;
	const [sort, setSort] = props.sortHook;
	const [party, setParty] = props.partyHook;
	const [state, setState] = props.stateHook;
	const [trades, setTrades] = props.tradesHook;
	const [tradeVol, setTradeVol] = props.volHook;
	const [search, setSearch] = props.searchHook;
	const [address, setAddress] = props.addressHook;
	const [chamber, setChamber] = props.chamberHook;

	const [order, setOrder] = props.orderHook;

	const convertToSortLabel = (url) => {
		const keyToLabel = {
			party: 'party',
			num_trades: 'tradeNum',
			state: 'state',
			seniority: 'seniority',
			traded_volume: 'tradeVol',
		};

		switch (url) {
			case 'party':
				return keyToLabel.party;
			case 'state':
				return keyToLabel.state;
			case 'seniority':
				return keyToLabel.seniority + (order === 'desc' ? '_rev' : '');
			case 'num_trades':
				return keyToLabel.num_trades + (order === 'desc' ? '_rev' : '');
			case 'traded_volume':
				return (
					keyToLabel.traded_volume + (order === 'desc' ? '_rev' : '')
				);
			default:
				return '';
		}
	};

	const handleSort = (event) => {
		var sort_dict = {
			party: 'party',
			tradeNum: 'num_trades',
			tradeNum_rev: 'num_trades',
			seniority: 'seniority',
			seniority_rev: 'seniority',
			state: 'state',
			chamber: 'chamber',
			tradeVol: 'traded_volume',
			tradeVol_rev: 'traded_volume',
		};
		const rev_set = new Set([
			'tradeNum_rev',
			'tradeVol_rev',
			'seniority_rev',
		]);

		if (rev_set.has(event.target.value)) {
			setOrder('desc');
		} else {
			setOrder('');
		}
		setSort(sort_dict[event.target.value] ?? '');
		setPage(1);
	};

	const handleParty = (event) => {
		setParty(event.target.value);
		setPage(1);
	};

	const handleState = (event) => {
		setState(event.target.value);
		setPage(1);
	};

	const handleTrades = (event) => {
		setTrades(event.target.value);
		setPage(1);
	};

	const handleTradeVol = (event) => {
		setTradeVol(event.target.value);
		setPage(1);
	};

	const handleChamber = (event) => {
		setChamber(event.target.value);
		setPage(1);
	};

	const handleAddress = (value) => {
		setAddress(value);
		setPage(1);
	};

	const handleReset = (event) => {
		setSearch('');
		setSort('');
		setParty('');
		setState('');
		setTrades('');
		setTradeVol('');
		setChamber('');
		setAddress('');
		setPage(1);
	};
	return (
		<div>
			<div className={styles.cardContainer}>
				<FormControl sx={{ m: 1, minWidth: 50 }}>
					<Button onClick={handleReset}>Reset</Button>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<TextField
						defaultValue={search}
						label="Search"
						variant="outlined"
						onKeyUp={(e) => {
							if (e.key === 'Enter') {
								setSearch(e.target.value);
								setPage(1);
							}
						}}
					/>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<InputLabel id="demo-simple-select-helper-label">
						Sort By:
					</InputLabel>
					<Select
						labelId="demo-simple-select-helper-label"
						id="demo-simple-select-helper"
						value={convertToSortLabel(sort)}
						label="Sort By: "
						onChange={handleSort}
					>
						<MenuItem value="">
							<em>None</em>
						</MenuItem>
						{Object.entries(sortMap).map(([name, title]) => (
							<MenuItem key={name} value={name}>
								{title}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<InputLabel id="demo-simple-select-helper-label">
						Political Party:
					</InputLabel>
					<Select
						labelId="demo-simple-select-helper-label"
						id="demo-simple-select-helper"
						value={party}
						label="Political Party: "
						onChange={handleParty}
					>
						<MenuItem value="">
							<em>All</em>
						</MenuItem>
						{Object.entries(partyMap).map(([name, title]) => (
							<MenuItem key={name} value={name}>
								{title}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<InputLabel id="demo-simple-select-helper-label">
						State:
					</InputLabel>
					<Select
						labelId="demo-simple-select-helper-label"
						id="demo-simple-select-helper"
						value={state}
						label="State: "
						onChange={handleState}
					>
						<MenuItem value="">
							<em>All</em>
						</MenuItem>
						{Object.entries(stateMap).map(([name, title]) => (
							<MenuItem key={name} value={name}>
								{title}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<InputLabel id="demo-simple-select-helper-label">
						# of Trades:
					</InputLabel>
					<Select
						labelId="demo-simple-select-helper-label"
						id="demo-simple-select-helper"
						value={trades}
						label="Number of Trades: "
						onChange={handleTrades}
					>
						<MenuItem value="">
							<em>All</em>
						</MenuItem>
						{Object.entries(tradesMap).map(([name, title]) => (
							<MenuItem key={name} value={name}>
								{title}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<InputLabel id="demo-simple-select-helper-label">
						Trade Volume:
					</InputLabel>
					<Select
						labelId="demo-simple-select-helper-label"
						id="demo-simple-select-helper"
						value={tradeVol}
						label="Maximum Trades: "
						onChange={handleTradeVol}
					>
						<MenuItem value="">
							<em>All</em>
						</MenuItem>
						{Object.entries(tradeVolMap).map(([name, title]) => (
							<MenuItem key={name} value={name}>
								{title}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 200 }}>
					<InputLabel id="demo-simple-select-helper-label">
						Chamber:
					</InputLabel>
					<Select
						labelId="demo-simple-select-helper-label"
						id="demo-simple-select-helper"
						value={chamber}
						label="Chamber: "
						onChange={handleChamber}
					>
						<MenuItem value="">
							<em>All</em>
						</MenuItem>
						{Object.entries(chamberMap).map(([name, title]) => (
							<MenuItem key={name} value={name}>
								{title}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl sx={{ m: 1, minWidth: 400 }}>
					<TextField
						defaultValue={address}
						variant="outlined"
						label="Enter your address to find who represents you:"
						onKeyPress={(ev) => {
							if (ev.key === 'Enter') {
								handleAddress(ev.target.value);
							}
						}}
					/>
				</FormControl>
			</div>
		</div>
	);
}
