import * as React from 'react';
import {
	InputLabel,
	MenuItem,
	FormControl,
	Select,
	Button,
	TextField,
} from '@mui/material';
import {
	sortMap,
	fiftyTwoWeekHighMap,
	fiftyTwoWeekLowMap,
	marketCapMap,
	exchangeMap,
} from './FilterValues/StockFilterValues';
import styles from './StockFilters.module.css';

function StockFilters(props) {
	const setPage = props.setPage;
	const [sort, setSort] = props.sortHook;
	const [marketCap, setMarketCap] = props.capHook;
	const [fiftyTwoWeekLow, setFiftyTwoWeekLow] = props.fiftyTwoWeekLowHook;
	const [fiftyTwoWeekHigh, setFiftyTwoWeekHigh] = props.fiftyTwoWeekHighHook;
	const [exchange, setExchange] = props.excHook;
	const [search, setSearch] = props.searchHook;

	const [order, setOrder] = props.orderHook;

	const convertToSortLabel = (url) => {
		const keyToLabel = {
			symbol: 'ticker',
			marketCap: 'marketCap',
			exchange: 'exchange',
			shortName: 'name',
		};

		switch (url) {
			case 'symbol':
				return keyToLabel.symbol + (order === 'desc' ? '_rev' : '');
			case 'marketCap':
				return keyToLabel.marketCap + (order === 'desc' ? '_rev' : '');
			case 'exchange':
				return keyToLabel.exchange;
			case 'shortName':
				return keyToLabel.shortName + (order === 'desc' ? '_rev' : '');
			default:
				return '';
		}
	};

	const handleSort = (event) => {
		var sort_dict = {
			ticker: 'symbol',
			ticker_rev: 'symbol',
			marketCap: 'marketCap',
			marketCap_rev: 'marketCap',
			exchange: 'exchange',
			name: 'shortName',
			name_rev: 'shortName',
		};

		var rev_sorts = ['ticker_rev', 'marketCap_rev', 'name_rev'];
		var rev_set = new Set(rev_sorts);
		if (rev_set.has(event.target.value)) {
			setOrder('desc');
		} else {
			setOrder('');
		}
		setSort(sort_dict[event.target.value] ?? '');
		setPage(1);
	};

	const handleFiftyTwoWeekLow = (event) => {
		setFiftyTwoWeekLow(event.target.value);
		setPage(1);
	};

	const handleMarketCap = (event) => {
		setMarketCap(event.target.value);
		setPage(1);
	};

	const handleFiftyTwoWeekHigh = (event) => {
		setFiftyTwoWeekHigh(event.target.value);
		setPage(1);
	};

	const handleExchange = (event) => {
		setExchange(event.target.value);
		setPage(1);
	};

	const handleReset = (event) => {
		setSearch('');
		setSort('');
		setFiftyTwoWeekLow('');
		setMarketCap('');
		setFiftyTwoWeekHigh('');
		setExchange('');
		setPage(1);
	};

	return (
		<div>
			<div className={styles.table}>
				<div className={styles.cardContainer}>
					<FormControl sx={{ m: 1, minWidth: 50 }}>
						<Button onClick={handleReset}>Reset</Button>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<TextField
							defaultValue={search}
							label="Search"
							variant="outlined"
							onKeyUp={(e) => {
								if (e.key === 'Enter') {
									setSearch(e.target.value);
									setPage(1);
								}
							}}
						/>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Sort By:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={convertToSortLabel(sort)}
							label="Sort By: "
							onChange={handleSort}
						>
							<MenuItem value="">
								<em>None</em>
							</MenuItem>
							{Object.entries(sortMap).map(([name, title]) => (
								<MenuItem key={name} value={name}>
									{title}
								</MenuItem>
							))}
						</Select>
					</FormControl>

					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Market Cap:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={marketCap}
							label="Market Cap: "
							onChange={handleMarketCap}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(marketCapMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>

					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							52 Week High :
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={fiftyTwoWeekHigh}
							label="52 Week High: "
							onChange={handleFiftyTwoWeekHigh}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(fiftyTwoWeekHighMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>

					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							52 Week Low:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={fiftyTwoWeekLow}
							label="52 Week High: "
							onChange={handleFiftyTwoWeekLow}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(fiftyTwoWeekLowMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>

					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Exchange:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={exchange}
							label="Exchange: "
							onChange={handleExchange}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(exchangeMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>
				</div>
			</div>
		</div>
	);
}
export default StockFilters;
