import * as React from 'react';
import {
	InputLabel,
	MenuItem,
	FormControl,
	Select,
	Button,
	TextField,
} from '@mui/material';
import {
	sortMap,
	industryMap,
	countryMap,
	employeeMap,
	foundingMap,
} from './FilterValues/CompanyFilterValues';
import styles from './StockFilters.module.css';

export default function CompanyFilters(props) {
	const setPage = props.setPage;
	const [sort, setSort] = props.sortHook;
	const [industry, setIndustry] = props.industryHook;
	const [country, setCountry] = props.countryHook;
	const [employees, setEmployees] = props.employeeHook;
	const [founding, setFounding] = props.foundingHook;
	const [search, setSearch] = props.searchHook;

	const [order, setOrder] = props.orderHook;

	const convertToSortLabel = (url) => {
		const keyToLabel = {
			name: 'name',
			country: 'country',
			industry: 'industry',
			employees: 'employees',
			ticker: 'shortName',
		};

		switch (url) {
			case 'name':
				return keyToLabel.name + (order === 'desc' ? '_rev' : '');
			case 'country':
				return keyToLabel.country + (order === 'desc' ? '_rev' : '');
			case 'industry':
				return keyToLabel.industry;
			case 'employees':
				return keyToLabel.employees;
			case 'ticker':
				return keyToLabel.ticker;
			default:
				return '';
		}
	};

	const handleSort = (event) => {
		var sort_dict = {
			name: 'name',
			name_rev: 'name',
			country: 'country',
			country_rev: 'country',
			industry: 'industry',
			employees: 'employees',
			ticker: 'shortName',
		};
		var rev_sorts = ['country_rev', 'name_rev'];
		var rev_set = new Set(rev_sorts);
		if (rev_set.has(event.target.value)) {
			setOrder('desc');
		} else {
			setOrder('');
		}
		setSort(sort_dict[event.target.value] ?? '');
		setPage(1);
	};

	const handleIndustry = (event) => {
		setIndustry(event.target.value);
		setPage(1);
	};

	const handleCountry = (event) => {
		setCountry(event.target.value.replace('_', ' '));
		setPage(1);
	};

	const handleEmployees = (event) => {
		setEmployees(event.target.value);
		setPage(1);
	};

	const handleFounding = (event) => {
		setFounding(event.target.value);
		setPage(1);
	};

	const handleReset = (event) => {
		setSearch('');
		setSort('');
		setIndustry('');
		setCountry('');
		setEmployees('');
		setFounding('');
		setPage(1);
	};

	return (
		<div>
			<div className={styles.table}>
				<div className={styles.cardContainer}>
					<FormControl sx={{ m: 1, minWidth: 50 }}>
						<Button onClick={handleReset}>Reset</Button>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<TextField
							defaultValue={search}
							label="Search"
							variant="outlined"
							onKeyUp={(e) => {
								if (e.key === 'Enter') {
									setSearch(e.target.value);
									setPage(1);
								}
							}}
						/>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Sort By:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={convertToSortLabel(sort)}
							label="Sort By: "
							onChange={handleSort}
						>
							<MenuItem value="">
								<em>None</em>
							</MenuItem>
							{Object.entries(sortMap).map(([name, title]) => (
								<MenuItem key={name} value={name}>
									{title}
								</MenuItem>
							))}
						</Select>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Industry:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={industry}
							label="Industry: "
							onChange={handleIndustry}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(industryMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Country:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={country.replace(' ', '_')}
							label="Country: "
							onChange={handleCountry}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(countryMap).map(([name, title]) => (
								<MenuItem key={name} value={name}>
									{title}
								</MenuItem>
							))}
						</Select>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Number of Employees:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={employees}
							label="Num Employees: "
							onChange={handleEmployees}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(employeeMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>
					<FormControl sx={{ m: 1, minWidth: 200 }}>
						<InputLabel id="demo-simple-select-helper-label">
							Founding Year:
						</InputLabel>
						<Select
							labelId="demo-simple-select-helper-label"
							id="demo-simple-select-helper"
							value={founding}
							label="Founding Year: "
							onChange={handleFounding}
						>
							<MenuItem value="">
								<em>All</em>
							</MenuItem>
							{Object.entries(foundingMap).map(
								([name, title]) => (
									<MenuItem key={name} value={name}>
										{title}
									</MenuItem>
								)
							)}
						</Select>
					</FormControl>
				</div>
			</div>
		</div>
	);
}
