/*const sortMap = {
    name: "Name: A-Z",
    name_rev: "Name: Z-A",
    holdings: "Holdings: High-Low",
    holdings_rev: "Holdings: Low-High",
    state: "State: A-Z",
    state_rev: "State: Z-A",
    tradevol: "Trade Volume: High-Low",
    tradevol_rev: "Trade Volume: Low-High"

}*/

const sortMap = {
	name: 'Name: A-Z',
	name_rev: 'Name: Z-A',
	country: 'Country: A-Z',
	country_rev: 'Country: Z-A',
	industry: 'Industry: A-Z',
	employees: 'Number of Employees: High-Low',
	ticker: 'Ticker: A-Z',
};

const industryMap = {
	Aerospace__Defense: 'Aerospace & Defense',
	Air_Freight__Logistics: 'Air Freight & Logistics',
	Airlines: 'Airlines',
	Automotive: 'Automotive',
	Banks: 'Banks',
	Beverages: 'Beverages',
	Biotechnology: 'Biotechnology',
	Building_Materials: 'Building Materials',
	Capital_Markets: 'Capital Markets',
	Chemicals: 'Chemicals',
	Communications_Equipment: 'Communications Equipment',
	Construction__Engineering: 'Construction & Engineering',
	Consumer_Discretionary: 'Consumer Discretionary',
	Consumer_Goods: 'Consumer Goods',
	Consumer_Staples: 'Consumer Staples',
	Containers__Packaging: 'Containers & Packaging',
	Distributors: 'Distributors',
	Diversified_Consumer_Services: 'Diversified Consumer Services',
	Diversified_Financial_Services: 'Diversified Financial Services',
	Diversified_Telecommunication_Services:
		'Diversified Telecommunication Services',
	Electric_Utilities: 'Electric Utilities',
	Electrical_Equipment: 'Electrical Equipment',
	Food_Products: 'Food Products',
	Gas_Utilities: 'Gas Utilities',
	Health_Care_Providers__Services: 'Health Care Providers & Services',
	Hotels_Restaurants__Leisure: 'Hotels, Restaurants & Leisure',
	Household_Durables: 'Household Durables',
	IT_Services: 'IT Services',
	Industrial_Conglomerates: 'Industrial Conglomerates',
	Insurance: 'Insurance',
	Internet_Software__Services: 'Internet Software & Services',
	Machinery: 'Machinery',
	Marine: 'Marine',
	Media: 'Media',
	Metals__Mining: 'Metals & Mining',
	Paper__Forest_Products: 'Paper & Forest Products',
	Personal_Products: 'Personal Products',
	Pharmaceuticals: 'Pharmaceuticals',
	Professional_Services: 'Professional Services',
	Real_Estate: 'Real Estate',
	Renewable_Electricity: 'Renewable Electricity',
	Retailing: 'Retailing',
	Technology_Hardware_Storage__Peripherals:
		'Technology Hardware, Storage & Peripherals',
	Textiles_Apparel__Luxury_Goods: 'Textiles, Apparel & Luxury Goods',
	Transportation: 'Transportation',
	Utilities: 'Utilities',
	None: 'None',
};

const countryMap = {
	Argentina: 'Argentina',
	Australia: 'Australia',
	Belgium: 'Belgium',
	Bermuda: 'Bermuda',
	Brazil: 'Brazil',
	Canada: 'Canada',
	China: 'China',
	Denmark: 'Denmark',
	Finland: 'Finland',
	France: 'France',
	Germany: 'Germany',
	India: 'India',
	Ireland: 'Ireland',
	Israel: 'Israel',
	Japan: 'Japan',
	Netherlands: 'Netherlands',
	Norway: 'Norway',
	Singapore: 'Singapore',
	Switzerland: 'Switzerland',
	Taiwan: 'Taiwan',
	Turkey: 'Turkey',
	United_Kingdom: 'United Kingdom',
	United_States: 'United States',
};

const employeeMap = {
	250: '< 250',
	1000: '< 1K',
	5000: '< 5K',
	10000: '< 10K',
	50000: '< 50K',
	100000: '< 100K',
	1000000: '100K+',
};

const foundingMap = {
	1940: 'before 1940',
	1970: 'before 1970',
	1990: 'before 1990',
	2000: 'before 2000',
	2010: 'before 2010',
	2023: 'after 2010',
};

export { sortMap, industryMap, countryMap, employeeMap, foundingMap };
