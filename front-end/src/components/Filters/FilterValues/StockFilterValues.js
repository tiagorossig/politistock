const sortMap = {
	ticker: 'Ticker: A-Z',
	ticker_rev: 'Ticker: Z-A',
	marketCap: 'Market Cap: Low-High',
	marketCap_rev: 'Market Cap: High-Low',
	exchange: 'Stock Exchange: A-Z',
	name: 'Company Name: A-Z',
	name_rev: 'Company Name: Z-A',
};

const fiftyTwoWeekHighMap = {
	5: '< $5',
	10: '< $10',
	20: '< $20',
	50: '< $50',
	100: '< $100',
	250: '< $250',
	500: '< $500',
};

const fiftyTwoWeekLowMap = {
	5: '$5+',
	10: '$10+',
	20: '$20+',
	50: '$50+',
	100: '$100+',
	250: '$250+',
	500: '$500+',
};

const marketCapMap = {
	100000000: '< $100M',
	500000000: '< $500M',
	1000000000: '< $1B',
	10000000000: '< $10B',
	50000000000: '< $50B',
	100000000000: '< $100B',
	1000000000000: '< $1T',
	100000000000000: '$1T+',
};

const exchangeMap = {
	ASE: 'NYSE American',
	BTS: 'BATS',
	NAS: 'Nasdaq',
	NCM: 'NasdaqCM',
	NGM: 'NasdaqGM',
	NMS: 'NasdaqGS',
	NYQ: 'NYSE',
	PCX: 'NYSEArca',
	PNK: 'Other OTC',
	YHD: 'YHD',
	NA: 'N/A',
};

export {
	sortMap,
	fiftyTwoWeekHighMap,
	fiftyTwoWeekLowMap,
	marketCapMap,
	exchangeMap,
};
