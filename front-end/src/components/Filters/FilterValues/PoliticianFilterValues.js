const sortMap = {
	party: 'Party: A-Z',
	tradeNum_rev: 'Trades: High-Low',
	tradeNum: 'Trades: Low-High',
	seniority: 'Seniority: Low-High',
	seniority_rev: 'Seniority: High-Low',
	state: 'State: A-Z',
	chamber: 'Chamber: A-Z',
	tradeVol_rev: 'Trade Volume: High-Low',
	tradeVol: 'Trade Volume: Low-High',
};

const stateMap = {
	alabama: 'Alabama',
	alaska: 'Alaska',
	arizona: 'Arizona',
	arkansas: 'Arkansas',
	california: 'California',
	colorado: 'Colorado',
	connecticut: 'Connecticut',
	delaware: 'Delaware',
	florida: 'Florida',
	georgia: 'Georgia',
	hawaii: 'Hawaii',
	idaho: 'Idaho',
	illinois: 'Illinois',
	indiana: 'Indiana',
	iowa: 'Iowa',
	kansas: 'Kansas',
	kentucky: 'Kentucky',
	louisiana: 'Louisiana',
	maine: 'Maine',
	maryland: 'Maryland',
	massachusetts: 'Massachusetts',
	michigan: 'Michigan',
	minnesota: 'Minnesota',
	mississippi: 'Mississippi',
	missouri: 'Missouri',
	montana: 'Montana',
	nebraska: 'Nebraska',
	nevada: 'Nevada',
	new_hampshire: 'New Hampshire',
	new_jersey: 'New Jersey',
	new_mexico: 'New Mexico',
	new_york: 'New York',
	north_carolina: 'North Carolina',
	north_dakota: 'North Dakota',
	ohio: 'Ohio',
	oklahoma: 'Oklahoma',
	oregon: 'Oregon',
	pennsylvania: 'Pennsylvania',
	rhode_island: 'Rhode Island',
	south_carolina: 'South Carolina',
	south_dakota: 'South Dakota',
	tennessee: 'Tennessee',
	texas: 'Texas',
	utah: 'Utah',
	vermont: 'Vermont',
	virginia: 'Virginia',
	washington: 'Washington',
	washington_DC: 'Washington, D.C.',
	west_virginia: 'West Virginia',
	wisconsin: 'Wisconsin',
	wyoming: 'Wyoming',
};

const partyMap = {
	democrat: 'Democrat',
	republican: 'Republican',
	other: 'Other',
};

const tradesMap = {
	50: '< 50',
	100: '< 100',
	250: '< 250',
	500: '< 500',
	1000: '< 1000',
	1000000: '1000+',
};

const tradeVolMap = {
	1000000: '< $1M',
	10000000: '< $10M',
	50000000: '< $50M',
	1000000000: '$50M +',
};

const chamberMap = {
	senate: 'Senate',
	house: 'House',
};

export { sortMap, stateMap, partyMap, tradesMap, tradeVolMap, chamberMap };
