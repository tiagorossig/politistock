import React from 'react';
import Highlighter from 'react-highlight-words';

function Highlight({ search, text }) {
	return (
		<Highlighter
			highlightStyle={{ background: '#f7f761', padding: '0' }}
			searchWords={search.split(' ')}
			textToHighlight={text}
		/>
	);
}

export default Highlight;
