import React, { useEffect, useState } from 'react';
import PersonCard from '../../components/Cards/PersonCard';
import ToolCard from '../../components/Cards/ToolCard';
import GitlabCard from '../../components/Cards/GitlabCard';
import { teamInfo, toolsUsed, apiInfo, sourceInfo } from './AboutInfo';
import CircularProgress from '@mui/material/CircularProgress';
import styles from './About.module.css';
import SourceCodeCard from '../../components/Cards/SourceCodeCard';

const getGitlabInfo = async () => {
	let totalCommitCount = 0,
		totalIssueCount = 0,
		totalTestCount = 0;

	teamInfo.forEach((member) => {
		totalTestCount += member.tests;
		member.issues = 0;
		member.commits = 0;
	});

	let commitList = await fetch(
		'https://gitlab.com/api/v4/projects/33895918/repository/contributors'
	);

	commitList = await commitList.json();
	commitList.forEach((element) => {
		const { name, email, commits } = element;
		teamInfo.forEach((member) => {
			if (
				member.name === name ||
				member.username === name ||
				member.emails.includes(email)
			) {
				member.commits += commits;
			}
		});
		totalCommitCount += commits;
	});

	// More than 100 issues, need to do pagination to get all
	const issuePaginationLength = 100;
	let page = 1;
	let issuePage = [];
	let issueList = [];
	do {
		issuePage = await fetch(
			`https://gitlab.com/api/v4/projects/33895918/issues?per_page=${issuePaginationLength}&page=${page++}`
		);
		issuePage = await issuePage.json();
		issueList = [...issueList, ...issuePage];
	} while (issuePage.length === 100);

	issueList.forEach((element) => {
		const { assignees } = element;
		assignees.forEach((a) => {
			const { name, username } = a;
			teamInfo.forEach((member) => {
				if (
					member.name === name ||
					member.name === username ||
					member.username === username
				) {
					member.issues += 1;
				}
			});
		});
		totalIssueCount += 1;
	});

	let branchList = await fetch(
		'https://gitlab.com/api/v4/projects/33895918/repository/branches'
	);
	branchList = await branchList.json();

	return {
		totalCommits: totalCommitCount,
		totalIssues: totalIssueCount,
		totalTests: totalTestCount,
		totalBranches: branchList.length,
		teamInfo: teamInfo,
	};
};

function About() {
	const [teamList, setTeamList] = useState([]);
	const [totalCommits, setTotalCommits] = useState(0);
	const [totalIssues, setTotalIssues] = useState(0);
	const [totalBranches, setTotalBranches] = useState(0);
	const [totalTests, setTotalTests] = useState(0);
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			if (teamList === undefined || teamList.length === 0) {
				const gitlabInfo = await getGitlabInfo();
				setTotalCommits(gitlabInfo.totalCommits);
				setTotalIssues(gitlabInfo.totalIssues);
				setTotalBranches(gitlabInfo.totalBranches);
				setTotalTests(gitlabInfo.totalTests);
				setTeamList(gitlabInfo.teamInfo);
				setLoaded(true);
			}
		};
		fetchData();
	}, [teamList]);

	var team = [];

	team.push();

	return (
		<div className={styles.wrapper}>
			<h1 className={styles.title}>About Us</h1>
			<p style={{ fontSize: 20 }}>
				Politistock was created to empower people’s investment decisions
				and to hold our representatives accountable for potential
				insider trading. Due to recent controversy, insider trading
				among politicians has been an important issue and we want to
				level the playing field for everyday people by giving them
				access to the investment decisions being made by the people who
				can directly affect the market.
			</p>

			<div className={styles.gridContainer}>
				{loaded ? (
					[
						teamList.map((m, i) => {
							return <PersonCard key={i} {...m} />;
						}),
						<GitlabCard
							key={-1}
							commits={totalCommits}
							issues={totalIssues}
							branches={totalBranches}
							tests={totalTests}
						/>,
					]
				) : (
					<CircularProgress />
				)}
			</div>
			<h1 className={styles.title}>Tools Used</h1>
			<div className={styles.gridContainer}>
				{toolsUsed.map((m, i) => {
					return <ToolCard key={i} {...m} />;
				})}
			</div>
			<h1 className={styles.title}>APIs Used</h1>
			<div className={styles.gridContainer}>
				{apiInfo.map((m, i) => {
					return <ToolCard key={i} {...m} />;
				})}
			</div>
			<h1 className={styles.title}>Gitlab Repository</h1>
			<div className={styles.gridContainer}>
				{sourceInfo.map((m, i) => {
					return <SourceCodeCard key={i} {...m} />;
				})}
			</div>
		</div>
	);
}

export default About;
