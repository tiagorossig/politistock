import KyleImg from '../../images/team/kyle.jpg';
import PriyaImg from '../../images/team/priya.png';
import TiagoImg from '../../images/team/tiago.jpg';
import RamonImg from '../../images/team/ramon.png';
import JonathanImg from '../../images/team/jonathan.jpg';
import AWS from '../../images/tools/aws.png';
import Gitlab from '../../images/tools/gitlab.png';
import Namecheap from '../../images/tools/namecheap.png';
import React from '../../images/tools/react.png';
import Postman from '../../images/tools/postman.png';
import Discord from '../../images/tools/discord.png';
import Puppeteer from '../../images/tools/puppeteer.png';
import Soup from '../../images/tools/soup.png';
import RBootstrap from '../../images/tools/bootstrap.png';
import Yahoo from '../../images/tools/yahoo.png';
import CT from '../../images/tools/ctrades.png';
import Clearbit from '../../images/tools/clearbit.png';
import Twitter from '../../images/tools/twitter.jpeg';
import TradingView from '../../images/tools/tradingview.png';
import SQL from '../../images/tools/sql.png';
import MUI from '../../images/tools/mui.png';
import Jest from '../../images/tools/jest.png';
import Flask from '../../images/tools/flask.jpg';
import Enzyme from '../../images/tools/enzyme.png';
import EB from '../../images/tools/eb.png';
import Docker from '../../images/tools/docker.png';
import Chart from '../../images/tools/chart.svg';
import Black from '../../images/tools/black.png';
import Serp from '../../images/tools/serp.png';
import Civic from '../../images/tools/civic.png';
import ProPub from '../../images/tools/pro.png';
import Pretty from '../../images/tools/pretty.png';
import D3 from '../../images/tools/d3.png';
import Recharts from '../../images/tools/recharts.png';

const teamInfo = [
	{
		name: 'Kyle Kamka',
		username: 'kkamka',
		emails: ['kyle.kamka01@gmail.com', 'kylekmk@users.noreply.github.com'],
		role: 'Front-end',
		phaseLeader: 'IV',
		bio: "I'm a third year CS major at UT Austin and I'm from the Austin area. I expect to graduate Spring 2023. I spend free time hiking, cooking, and playing video games.",
		img: KyleImg,
		commits: 0,
		issues: 0,
		tests: 0,
		link: 'https://www.linkedin.com/in/kyle-kamka/',
	},
	{
		name: 'Priyanka Barve',
		username: 'pbarve1',
		emails: ['pbarve@utexas.edu'],
		role: 'Full Stack',
		phaseLeader: 'II',
		bio: "I'm also a third year CS major at UT Austin, and I'm from Dallas. I'll be graduating in Spring 2023, and enjoy cooking, running, snowboarding, and drawing.",
		img: PriyaImg,
		commits: 0,
		issues: 0,
		tests: 0,
		link: 'https://www.linkedin.com/in/priyanka-barve-80151817a',
	},
	{
		name: 'Tiago Rossi',
		username: 'tiagorossig',
		emails: ['tiagogrimaldirossi@gmail.com'],
		role: 'Back-end',
		phaseLeader: 'I',
		bio: "I'm a Senior at UT Austin studying computer science. I will be graduating in Spring 2022 and will start at a full time Software Engineering position in July 2022.",
		img: TiagoImg,
		commits: 0,
		issues: 0,
		tests: 0,
		link: 'https://www.linkedin.com/in/tiagogrossi',
	},
	{
		name: 'Ramon Marquez',
		username: 'ramon_marquez',
		emails: ['ramon.marquez3@yahoo.com'],
		role: 'Front-end',
		phaseLeader: 'III',
		bio: "I'm a Junior at UT Austin studying computer science. I have a SWE internship coming up this summer and am taking this course in order to better prepare for it.",
		img: RamonImg,
		commits: 0,
		issues: 0,
		tests: 20,
		link: 'https://github.com/mariscos1',
	},
	{
		name: 'Jonathan Li',
		username: 'jonathanbli',
		emails: ['jonathan.li@utexas.edu'],
		role: 'Back-end',
		bio: "I'm a sophomore CS and math major from the Dallas area. I expect to graduate in Spring 2024. In my free time, I like to play the piano, chess, and various card games.",
		img: JonathanImg,
		commits: 0,
		issues: 0,
		tests: 12,
		link: 'https://www.linkedin.com/in/jonathan-b-li/',
	},
];

const toolsUsed = [
	{
		title: 'React',
		description: 'Javascript framework for front-end web development',
		img: React,
		link: 'https://reactjs.org/',
	},
	{
		title: 'AWS',
		description: 'AWS Ampliyfy was used to host our website',
		img: AWS,
		link: 'https://aws.amazon.com/amplify/',
	},
	{
		title: 'GitLab',
		description: 'Used to host our git repository',
		img: Gitlab,
		link: 'https://about.gitlab.com/',
	},
	{
		title: 'Namecheap',
		description: 'Provided domain name',
		img: Namecheap,
		link: 'https://www.namecheap.com/',
	},
	{
		title: 'Discord',
		description: 'Platform for team communication',
		img: Discord,
		link: 'https://discord.com/',
	},
	{
		title: 'Puppeteer',
		description: 'Used to scrape data',
		img: Puppeteer,
		link: 'https://developers.google.com/web/tools/puppeteer/get-started',
	},
	{
		title: 'Beautiful Soup',
		description: 'Used to scrape data',
		img: Soup,
		link: 'https://www.crummy.com/software/BeautifulSoup/bs4/doc/',
	},
	{
		title: 'React Bootstrap',
		description: 'Used for a small portion of React components',
		img: RBootstrap,
		link: 'https://react-bootstrap.github.io/',
	},
	{
		title: 'Material UI',
		description: 'Used for the majority of React components',
		img: MUI,
		link: 'https://mui.com/',
	},
	{
		title: 'Chart.js',
		description:
			'Used to show the relative differences in politician investments',
		img: Chart,
		link: 'https://www.chartjs.org/',
	},
	{
		title: 'Docker',
		description:
			'Used to containerize runtime environments and to deploy GitLab tests',
		img: Docker,
		link: 'https://www.docker.com/',
	},
	{
		title: 'Elastic Beanstalk',
		description: 'Used to deploy the Flask backend application',
		img: EB,
		link: 'https://aws.amazon.com/elasticbeanstalk/',
	},
	{
		title: 'Enzyme',
		description:
			"JS Testing utility for React that makes it easier to test your React Components' output",
		img: Enzyme,
		link: 'https://enzymejs.github.io/enzyme/',
	},
	{
		title: 'Jest',
		description:
			'JS testing framework designed to ensure correctness of any JavaScript codebase',
		img: Jest,
		link: 'https://jestjs.io/',
	},
	{
		title: 'Flask',
		description: 'Framework used for backend API development',
		img: Flask,
		link: 'https://flask.palletsprojects.com/en/2.1.x/',
	},
	{
		title: 'MySQL',
		description: 'Open-source relational database management system',
		img: SQL,
		link: 'https://www.mysql.com/',
	},
	{
		title: 'Black',
		description: 'Python code formatter for our backend',
		img: Black,
		link: 'https://black.readthedocs.io/en/stable/',
	},
	{
		title: 'Prettier',
		description:
			'Used to format all frontend code to keep it uniform across files',
		img: Pretty,
		link: 'https://prettier.io/',
	},
	{
		title: 'D3',
		description: 'Charting library used to make custom visualizations',
		img: D3,
		link: 'https://d3js.org/',
	},
	{
		title: 'Recharts',
		description: 'Charting library used for visualizations',
		img: Recharts,
		link: 'https://recharts.org/en-US/',
	},
];

const apiInfo = [
	{
		title: 'Yahoo Finance API',
		description: 'Supplied stock and company data',
		img: Yahoo,
		link: 'https://www.yahoofinanceapi.com/',
	},
	{
		title: 'Capitol Trades',
		description: 'Supplied scrapable data on politicians recent trades',
		img: CT,
		link: 'https://www.capitoltrades.com/',
	},
	{
		title: 'Clearbit API',
		description: 'Supplied company data',
		img: Clearbit,
		link: 'https://dashboard.clearbit.com/docs',
	},
	{
		title: 'Twitter API',
		description: 'Supplied Twitter feed for politicians and companies',
		img: Twitter,
		link: 'https://developer.twitter.com/en/docs/twitter-api',
	},
	{
		title: 'Trading View API',
		description:
			'Used for real-time stock charts for each stock instance. ',
		img: TradingView,
		link: 'https://www.tradingview.com/',
	},
	{
		title: 'SerpAPI',
		description: 'Used to scrape Google for high quality images',
		img: Serp,
		link: 'https://serpapi.com/',
	},
	{
		title: 'Google Civic Information API',
		description: 'Used to search for representatives based on address',
		img: Civic,
		link: 'https://developers.google.com/civic-information',
	},
	{
		title: 'ProPublica Congress API',
		description:
			'Used to get a list of members of congress based on their respective chamber',
		img: ProPub,
		link: 'https://projects.propublica.org/api-docs/congress-api/members/',
	},
];

const sourceInfo = [
	{
		img: Gitlab,
		link: 'https://gitlab.com/tiagorossig/politistock',
	},
	{
		img: Postman,
		link: 'https://documenter.getpostman.com/view/19780329/UVksMEHb',
	},
];

export { teamInfo, toolsUsed, apiInfo, sourceInfo };
