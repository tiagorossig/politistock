import {
	Container,
	Grid,
	FormControl,
	TextField,
	Typography,
} from '@mui/material';
import MiniCompanyTable from './MiniModels/Companies/MiniCompanyTable';
import MiniPoliticiansTable from './MiniModels/Politicians/MiniPoliticiansTable';
import MiniStocksTable from './MiniModels/Stocks/MiniStocksTable';
import { useSearchParams, useNavigate } from 'react-router-dom';

function Search() {
	const navigate = useNavigate();
	const [searchParams] = useSearchParams();
	const search = searchParams.get('search') ?? '';

	return (
		<Container maxWidth="xl">
			<Grid item xs={12}>
				<Typography variant="h2">Search</Typography>
			</Grid>
			<Grid item xs={12}>
				<FormControl sx={{ width: '100%', margin: '35px 0 15px 0' }}>
					<TextField
						defaultValue={search}
						label="Enter a sitewide search here"
						variant="outlined"
						sx={{ width: '100%' }}
						onKeyUp={(e) => {
							if (e.key === 'Enter') {
								navigate(
									'/search' +
										(e.target.value !== ''
											? '?search=' + e.target.value
											: '')
								);
							}
						}}
					/>
				</FormControl>
			</Grid>
			<Grid item xs={12}>
				<MiniPoliticiansTable search={search} />
			</Grid>
			<Grid item xs={12}>
				<MiniCompanyTable search={search} />
			</Grid>
			<Grid item xs={12}>
				<MiniStocksTable search={search} />
			</Grid>
		</Container>
	);
}

export default Search;
