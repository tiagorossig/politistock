import React, { useState, useEffect } from 'react';
import styles from './MiniPoliticiansTable.module.css';
import PoliticianCard from '../../../../components/Cards/PoliticianCard';
import { CircularProgress, Typography, Grid, Container } from '@mui/material';
import MorePolitician from '../../../../components/Cards/MorePoliticianCard';

function MiniPoliticiansTable({ search }) {
	const page = 1;
	const postsPerPage = 11;

	const [posts, setPosts] = useState([]);
	const [loaded, setLoaded] = useState(false);
	const [numResults, setNumResults] = useState(0);

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};

			fetch(
				'https://api.politistock.me/politicians/' +
					postsPerPage +
					':' +
					(page - 1) * postsPerPage +
					'?search=' +
					search,
				requestOptions
			)
				.then((response) => response.json())
				.then((result) => {
					var postArr = [];
					for (const [key, value] of Object.entries(result)) {
						if (key !== 'num_entries') {
							postArr.push(value);
						} else {
							setNumResults(value);
						}
					}
					setPosts(postArr);
				})
				.catch((error) => console.log('error', error));
			setLoaded(true);
		};

		fetchPosts();
	}, [page, postsPerPage, search]);

	return loaded ? (
		<Container maxWidth="xl">
			<Grid item xs={12} sx={{ padding: '15px 0' }}>
				<Typography variant="h4" className={styles.title}>
					Politicians
				</Typography>
			</Grid>
			{posts.length > 0 ? (
				<Grid container spacing={2}>
					{posts.map((m, i) => {
						return (
							<Grid item xs={12} md={6} lg={4} xl={3} key={i}>
								<PoliticianCard search={search} {...m} />
							</Grid>
						);
					})}
					<Grid item xs={12} md={6} lg={4} xl={3}>
						<MorePolitician
							search={search}
							numResults={numResults}
						/>
					</Grid>
				</Grid>
			) : (
				<Grid
					item
					xs={12}
					sx={{
						padding: '15px 0',
						height: '300px',
						background: '#fff',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Typography variant="h6">
						No results found for search
					</Typography>
				</Grid>
			)}
		</Container>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default MiniPoliticiansTable;
