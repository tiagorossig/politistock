import React, { useState, useEffect } from 'react';
import styles from './MiniStocksTable.module.css';
import {
	CircularProgress,
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	Paper,
	Typography,
	Container,
	Grid,
} from '@mui/material';
import StockRow from '../../../Stocks/Table/StockRow';
import MoreStockRow from './MoreStockRow';

function MiniStocksTable({ search }) {
	const page = 1;
	const postsPerPage = 11;

	const [posts, setPosts] = useState([]);
	const [loaded, setLoaded] = useState(false);
	const [numResults, setNumResults] = useState(0);

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};

			fetch(
				'https://api.politistock.me/stocks/' +
					postsPerPage +
					':' +
					(page - 1) * postsPerPage +
					'?search=' +
					search,
				requestOptions
			)
				.then((response) => response.json())
				.then((result) => {
					var postArr = [];
					for (const [key, value] of Object.entries(result)) {
						if (key !== 'num_entries') {
							postArr.push(value);
						} else {
							setNumResults(value);
						}
					}
					setPosts(postArr);
				})
				.catch((error) => console.log('error', error));
			setLoaded(true);
		};

		fetchPosts();
	}, [page, postsPerPage, search]);

	return loaded ? (
		<Container maxWidth="xl">
			<Grid item xs={12} sx={{ padding: '15px 0' }}>
				<Typography variant="h4" className={styles.title}>
					Traded Stocks
				</Typography>
			</Grid>
			<Grid>
				{posts.length > 0 ? (
					<>
						<TableContainer component={Paper}>
							<Table
								sx={{ minWidth: 650 }}
								aria-label="simple table"
							>
								<TableHead>
									<TableRow>
										<TableCell>Ticker</TableCell>
										<TableCell align="left">
											Company Name
										</TableCell>
										<TableCell align="right">
											Exchange
										</TableCell>
										<TableCell align="right">
											Market Cap
										</TableCell>
										<TableCell align="right">
											52 Week High
										</TableCell>
										<TableCell align="right">
											52 Week Low
										</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{posts.map((post, i) => {
										return (
											<StockRow
												key={post.id}
												search={search}
												{...post}
											/>
										);
									})}
								</TableBody>
							</Table>
						</TableContainer>
						<MoreStockRow search={search} numResults={numResults} />
					</>
				) : (
					<Grid
						item
						xs={12}
						sx={{
							padding: '15px 0',
							height: '300px',
							background: '#fff',
							display: 'flex',
							justifyContent: 'center',
							alignItems: 'center',
						}}
					>
						<Typography variant="h6">
							No results found for search
						</Typography>
					</Grid>
				)}
			</Grid>
		</Container>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default MiniStocksTable;
