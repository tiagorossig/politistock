import React from 'react';
import { Grid, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

function MoreStockRow({ numResults, search }) {
	return (
		<Link to={'/stocks' + (search !== '' ? '?search=' + search : '')}>
			<Grid
				item
				xs={12}
				sx={{
					padding: '15px 0',
					height: '50px',
					background: '#fff',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					cursor: 'pointer',
					border: '1px solid #DCDCDC',
				}}
			>
				<Typography variant="subtitle1">
					See all {numResults} Companies in your search
				</Typography>
			</Grid>
		</Link>
	);
}

export default MoreStockRow;
