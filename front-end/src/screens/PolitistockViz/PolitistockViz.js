import React, { useState } from 'react';
import { Container, Grid, Typography } from '@mui/material';
import MoneyMap from '../../components/Visualizations/MoneyMap';
import StockCount from '../../components/Visualizations/StockCount';
import SectorCount from '../../components/Visualizations/SectorCount';
function PolitistockViz() {
	const maxWidth = 1250;
	const [width, setWidth] = useState(
		window.innerWidth < maxWidth ? window.innerWidth : maxWidth
	);
	window.onresize = () => {
		setWidth(window.innerWidth < maxWidth ? window.innerWidth : maxWidth);
	};

	return (
		<Container maxWidth="xl">
			<Grid item xs={12}>
				<Typography variant="h2" sx={{ margin: '50px 0' }}>
					Politistock Visualizations
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Trade Volume By State</Typography>
			</Grid>
			<Grid item xs={12}>
				<MoneyMap width={width} />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">
					50 Most Popular Stock Holdings
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<StockCount />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Total Market Cap by Sector</Typography>
			</Grid>
			<Grid item xs={12}>
				<SectorCount />
			</Grid>
		</Container>
	);
}

export default PolitistockViz;
