import React from 'react';
import { Container, Grid, Typography } from '@mui/material';
import MoneyMap from '../../components/Visualizations/MoneyMap';
import StockCount from '../../components/Visualizations/StockCount';
import SectorCount from '../../components/Visualizations/SectorCount';
import DosageCount from '../../components/Visualizations/DosageCount';
import ManufacturerDrugs from '../../components/Visualizations/ManufacturerDrugs';

function Viz() {
	return (
		<Container maxWidth="lg">
			<Grid item xs={12}>
				<Typography variant="h2" sx={{ marginBottom: '50px' }}>
					Visualizations
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h3" sx={{ marginBottom: '50px' }}>
					Politistock Visualizations
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Trade Volume By State</Typography>
			</Grid>
			<Grid item xs={12}>
				<MoneyMap />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">
					50 Most Popular Stock Holdings
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<StockCount />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Total Market Cap by Sector</Typography>
			</Grid>
			<Grid item xs={12}>
				<SectorCount />
			</Grid>
			<Grid item xs={12}>
				<Typography
					variant="h3"
					sx={{ marginBottom: '50px', marginTop: '50px' }}
				>
					Provider Visualizations
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Most Popular Dosage Forms</Typography>
			</Grid>
			<Grid item xs={12}>
				<DosageCount />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">
					Best Providing Manufacturers
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<ManufacturerDrugs />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Provider Viz 3</Typography>
			</Grid>
			<Grid item xs={12}>
				<>VIZ 3 HERE</>
			</Grid>
		</Container>
	);
}

export default Viz;
