import React from 'react';

function Error() {
	return (
		<div style={{ textAlign: 'center', marginTop: '10%' }}>
			<h1>404 Error: Page Not Found</h1>
			<h2>We'll keep looking for the page...</h2>
			<h5>We didn't find it...</h5>
			<h6>Sorry...</h6>
		</div>
	);
}

export default Error;
