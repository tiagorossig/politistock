import React, { useState } from 'react';
import { Container, Grid, Typography } from '@mui/material';
import DosageCount from '../../components/Visualizations/DosageCount';
import ManufacturerDrugs from '../../components/Visualizations/ManufacturerDrugs';
import ElementScatterplot from '../../components/Visualizations/ElementScatterplot';

function ProviderViz() {
	const maxWidth = 1250;
	const [width, setWidth] = useState(
		window.innerWidth < maxWidth ? window.innerWidth : maxWidth
	);
	window.onresize = () => {
		setWidth(window.innerWidth < maxWidth ? window.innerWidth : maxWidth);
	};

	return (
		<Container maxWidth="xl">
			<Grid item xs={12}>
				<Typography variant="h2" sx={{ margin: '50px 0' }}>
					Provider Visualizations
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">Most Popular Dosage Forms</Typography>
			</Grid>
			<Grid item xs={12}>
				<DosageCount />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">
					Best Providing Manufacturers
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<ManufacturerDrugs width={width} />
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h4">
					Ionization Energy vs. Atomic Radius
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<ElementScatterplot size={width < 800 ? width : 800} />
			</Grid>
		</Container>
	);
}

export default ProviderViz;
