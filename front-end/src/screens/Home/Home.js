import styles from './Home.module.css';
import ModelCard from '../../components/Cards/ModelCard';
import { modelInfo } from './HomeInfo';
import {
	Typography,
	Container,
	Grid,
	FormControl,
	TextField,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

function Home() {
	const navigate = useNavigate();

	return (
		<Container maxWidth="lg">
			<Grid item xs={12}>
				<Typography variant="h1" className={styles.title}>
					Politistock
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<p className={styles.summary}>
					See who your Federal Representatives are investing in with
					their insider knowledge. Find information on the companies
					politicians are investing in as well as how the
					corresponding stocks are performing.
				</p>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h3" className={styles.title}>
					Model Pages
				</Typography>
			</Grid>

			<Grid item xs={12}>
				<FormControl sx={{ width: '100%', margin: '35px 0 15px 0' }}>
					<TextField
						label="Enter a sitewide search here"
						variant="outlined"
						sx={{ width: '100%' }}
						onKeyUp={(e) => {
							if (e.key === 'Enter') {
								navigate('/search?search=' + e.target.value);
							}
						}}
					/>
				</FormControl>
			</Grid>

			<Grid item xs={12}>
				<div className={styles.cardContainer}>
					{modelInfo.map((m, i) => {
						return <ModelCard key={i} {...m} />;
					})}
				</div>
			</Grid>
		</Container>
	);
}

export default Home;
