import congress from '../../images/home/congress.jpeg';
import stocks from '../../images/home/stocks.jpeg';
import companies from '../../images/home/companies.jpeg';

const modelInfo = [
	{
		img: congress,
		title: 'Politicians',
		text: "Want to learn more about a particular legislator? What about stocks they've recently bought or sold and companies those stocks are related to?",
		buttonText: 'See All Politicians',
		href: 'politicians',
	},
	{
		img: stocks,
		title: 'Stocks',
		text: "Want to see how a particular stock has historically performed or what company they're associated with? What about which politicians are investing in them?",
		buttonText: 'See All Stocks',
		href: 'stocks',
	},
	{
		img: companies,
		title: 'Companies',
		text: "Want to learn more about a particular company? What about the stock they're associated with or which companies politicians are investing in?",
		buttonText: 'See All Companies',
		href: 'companies',
	},
];

export { modelInfo };
