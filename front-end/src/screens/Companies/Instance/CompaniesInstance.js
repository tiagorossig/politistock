import React, { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import styles from './CompaniesInstance.module.css';
import noImage from '../../../images/noimage.png';
import { ArrowBackIosNew, LanguageSharp as Website } from '@mui/icons-material';
import { GraphUpArrow } from 'react-bootstrap-icons';
import {
	Skeleton,
	Grid,
	CircularProgress,
	Container,
	Typography,
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	IconButton,
} from '@mui/material';
import PartiesViz from '../../../components/Charts/PartiesViz';
import { TwitterTimelineEmbed } from 'react-twitter-embed';

const camelCase = (string) => {
	return string
		.match(/(\w)+/g)
		.map(
			(word) =>
				word.charAt(0).toUpperCase() +
				word.substring(1, word.length) +
				' '
		);
};

const getHandle = (string) => {
	if (string) {
		const handle = string.split(' ')[1];
		return handle.substring(1, handle.length - 2);
	} else {
		return null;
	}
};

function CompaniesInstance() {
	const { id } = useParams();
	const [img, setImg] = useState();
	const [loaded, setLoaded] = useState(false);
	const [company, setCompany] = useState({});
	const [politicians, setPoliticians] = useState([]);
	const [parties, setParties] = useState({});
	const [handle, setHandle] = useState(null);
	const navigate = useNavigate();

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};
			fetch('https://api.politistock.me/company/' + id, requestOptions)
				.then((response) => response.json())
				.then((result) => {
					setCompany(result[id]);

					const politicianNames = [];
					const counts = {};

					fetch(
						'https://api.politistock.me/politicians',
						requestOptions
					)
						.then((response) => response.json())
						.then((politicianFetch) => {
							for (
								var i = 0;
								i < result[id].politicians.length;
								i++
							) {
								const pol = result[id].politicians[i];
								const party = camelCase(
									politicianFetch[pol].party
								);
								counts[party] =
									counts[party] === undefined
										? 1
										: counts[party] + 1;
								politicianNames.push(
									politicianFetch[pol].preferred_name
								);
							}
							const handle = getHandle(company.twitter);
							if (handle) {
								setHandle(handle);
							}
							setParties(counts);
							setPoliticians(politicianNames);
						})
						.catch((error) => {
							console.log('error', error);
						});

					if (result[id].logo !== 'None') {
						fetch(result[id].logo)
							.then((response) => response.blob())
							.then((imageBlob) => {
								const imageObjectURL =
									URL.createObjectURL(imageBlob);
								setImg(imageObjectURL);
								setLoaded(true);
							})
							.catch((error) => {
								console.log('error', error);
								setImg(noImage);
								setLoaded(true);
							});
					} else {
						setImg(noImage);
						setLoaded(true);
					}
				})
				.catch((error) => {
					console.log('error', error);
					navigate('/error');
				});
		};

		fetchPosts();
	}, [id, company.logo, company.twitter, navigate]);

	return loaded && politicians.length > 0 ? (
		<div className={styles.wrapper}>
			<div className={styles.name}>{company.name}</div>
			<Container maxWidth="md">
				<Grid container spacing={{ xs: 2, md: 2 }}>
					<Grid
						item
						container
						spacing={{ xs: 2, md: 2 }}
						columns={{ xs: 4, sm: 8, md: 12 }}
						alignItems="center"
					>
						<Grid item xs={4} key={3}>
							<Link to={'/companies/'}>
								<IconButton size="large">
									<ArrowBackIosNew fontSize="inherit" />
								</IconButton>
							</Link>
						</Grid>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<Typography variant="h6">
								Business Summary:
							</Typography>
							<Typography variant="body1">
								{company.description}
							</Typography>
						</div>
					</Grid>
					<Grid item xs={6}>
						<div className={styles.info_block}>
							{loaded ? (
								<div
									style={{
										display: 'flex',
										justifyContent: 'center',
										alignItems: 'center',
										position: 'relative',
										overflow: 'hidden',
									}}
								>
									<img
										className={styles.companyImg}
										src={img}
										alt={id + ' logo'}
									></img>
								</div>
							) : (
								<Skeleton
									variant="rectangular"
									width={'100%'}
									height={'250px'}
								/>
							)}
							<div className={styles.info_text}>
								<b>Name: </b>
								{company.name}
							</div>
							<div className={styles.info_text}>
								<b>Stock Ticker: </b>
								{company.ticker}
							</div>
							<div className={styles.info_text}>
								<b>Twitter Handle: </b>
								<a href={`https://twitter.com/@${handle}`}>
									<u>{'@' + handle}</u>
								</a>
							</div>
							<div className={styles.info_text}>
								<b>Founding Year: </b>
								{company.foundedYear}
							</div>
							<div className={styles.info_text}>
								<b>industry: </b>
								{company.category.industry}
							</div>
							<div className={styles.info_text}>
								<b>Address: </b>
								{company.location}
							</div>
							<a
								href={`https://${company.domain}`}
								className={styles.iconLink}
							>
								<IconButton size="large">
									<Website fontSize="inherit" />
								</IconButton>
								<Typography
									variant="caption"
									sx={{ display: 'block' }}
								>
									Company Site
								</Typography>
							</a>
							<a
								href={'/stocks/' + company.ticker}
								className={styles.iconLink}
							>
								<IconButton size="large">
									<GraphUpArrow fontSize="inherit" />
								</IconButton>
								<Typography
									variant="caption"
									sx={{ display: 'block' }}
								>
									See Company
								</Typography>
							</a>
						</div>
					</Grid>
					<Grid item xs={6}>
						<div className={styles.info_block}>
							<TableContainer style={{ maxHeight: '400px' }}>
								<Table stickyHeader>
									<TableHead>
										<TableRow style={{ height: '35px' }}>
											<TableCell
												sx={{ textAlign: 'center' }}
											>
												<Typography variant="h5">
													Invested Politicians
												</Typography>
											</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{politicians.map((pol, i) => {
											return (
												<TableRow
													key={pol}
													hover={true}
													onClick={() => {
														navigate(
															'/politicians/' +
																company
																	.politicians[
																	i
																]
														);
													}}
												>
													<TableCell
														sx={{
															textAlign: 'center',
															cursor: 'pointer',
														}}
													>
														<Typography variant="h6">
															{pol}
														</Typography>
													</TableCell>
												</TableRow>
											);
										})}
									</TableBody>
								</Table>
							</TableContainer>
						</div>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<div className={styles.chart}>
								<PartiesViz parties={parties} />
							</div>
						</div>
					</Grid>
					{handle === null ? (
						<></>
					) : (
						<Grid item xs={12}>
							<div className={styles.company_info}>
								<TwitterTimelineEmbed
									sourceType="profile"
									screenName={handle}
									options={{ height: '900px' }}
								/>
							</div>
						</Grid>
					)}
				</Grid>
			</Container>
		</div>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default CompaniesInstance;
