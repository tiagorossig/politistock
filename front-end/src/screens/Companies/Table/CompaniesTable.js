import React, { useState, useEffect, useRef } from 'react';
import Pagination from '../../../components/Pagination/Pagination';
import styles from './CompaniesTable.module.css';
import CompanyCard from '../../../components/Cards/CompanyCard';
import { CircularProgress, Typography, Container, Grid } from '@mui/material';
import CompanyFilters from '../../../components/Filters/CompanyFilters';
import { useSearchParams, useNavigate } from 'react-router-dom';
import QuickPagination from '../../../components/Pagination/QuickPagination';

function CompaniesTable() {
	const [searchParams] = useSearchParams();
	const navigate = useNavigate();
	const [page, setPage] = useState(parseInt(searchParams.get('page') ?? '1'));
	const [postsPerPage, setPostsPerPage] = useState(
		parseInt(searchParams.get('perPage') ?? '12')
	);

	const [posts, setPosts] = useState([]);
	const [numPosts, setNumPosts] = useState(0);
	const [loaded, setLoaded] = useState(false);
	const pagination = useRef();

	const [sort, setSort] = useState(searchParams.get('sort') ?? '');
	const [industry, setIndustry] = useState(
		searchParams.get('industry') ?? ''
	);
	const [country, setCountry] = useState(searchParams.get('country') ?? '');
	const [employees, setEmployees] = useState(
		searchParams.get('employee_high') ?? ''
	);
	const [founding, setFounding] = useState(
		searchParams.get('founding_high') ?? ''
	);
	const [search, setSearch] = useState(searchParams.get('search') ?? '');

	const [filterQuery, setFilterQuery] = useState(null);
	const [useFilter, setUseFilter] = useState('');
	const [order, setOrder] = useState(searchParams.get('order') ?? '');

	window.onpopstate = (e) => {
		setPostsPerPage(parseInt(searchParams.get('perPage') ?? '12'));
		setSort(searchParams.get('sort') ?? '');
		setIndustry(searchParams.get('industry') ?? '');
		setCountry(searchParams.get('country') ?? '');
		setEmployees(searchParams.get('employee_high') ?? '');
		setFounding(searchParams.get('founding_high') ?? '');
		setSearch(searchParams.get('search') ?? '');
		setOrder(searchParams.get('order') ?? '');
	};

	useEffect(() => {
		var query_string = '';
		var multiple = false;
		var add_q = false;
		var filter_dict = {
			industry: industry,
			country: country,
			founding_high: founding,
			employee_high: employees,
			sort: sort,
			order: order,
			search: search,
		};
		for (let filter in filter_dict) {
			if (filter_dict[filter] !== '') {
				add_q = true;
				if (multiple) {
					query_string += '&';
				}
				var dict_val = filter_dict[filter];
				if (
					filter === 'employee_high' &&
					filter_dict[filter] === '1000000'
				) {
					filter = 'employee_low';
				}
				if (
					filter === 'founding_high' &&
					filter_dict[filter] === '2023'
				) {
					filter = 'founding_low';
					dict_val = 2010;
				}
				query_string += filter + '=' + dict_val;
				multiple = true;
			}
		}
		if (add_q) {
			query_string = '?' + query_string;
			setUseFilter(true);
		}
		setFilterQuery(query_string);
	}, [industry, country, employees, founding, sort, order, search]);

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};
			if (filterQuery && filterQuery !== '') {
				navigate(
					(filterQuery ? filterQuery + '&' : '?') +
						`page=${page}&perPage=${postsPerPage}`
				);
			}

			fetch(
				'https://api.politistock.me/companies/' +
					postsPerPage +
					':' +
					(page - 1) * postsPerPage +
					filterQuery,
				requestOptions
			)
				.then((response) => response.json())
				.then((result) => {
					console.log(
						'fetched: ' +
							'https://api.politistock.me/companies/' +
							postsPerPage +
							':' +
							(page - 1) * postsPerPage +
							filterQuery
					);
					var postArr = [];
					for (const [key, value] of Object.entries(result)) {
						if (key !== 'num_entries') {
							postArr.push(value);
						} else {
							setNumPosts(value);
						}
					}
					setPosts(postArr);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		if (filterQuery !== null) {
			fetchPosts();
		}
	}, [page, postsPerPage, filterQuery, useFilter, navigate]);

	return loaded ? (
		<Container maxWidth="xl">
			{' '}
			<Grid item xs={12}>
				<Typography variant="h2" className={styles.title}>
					Companies
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<CompanyFilters
					setPage={setPage}
					sortHook={[sort, setSort]}
					industryHook={[industry, setIndustry]}
					countryHook={[country, setCountry]}
					employeeHook={[employees, setEmployees]}
					foundingHook={[founding, setFounding]}
					queryHook={[filterQuery, setFilterQuery]}
					filterHook={[useFilter, setUseFilter]}
					orderHook={[order, setOrder]}
					searchHook={[search, setSearch]}
				/>
			</Grid>
			<Grid item xs={12}>
				<QuickPagination
					pagination={pagination}
					posts={postsPerPage}
					setPosts={setPostsPerPage}
				/>
			</Grid>
			{posts.length > 0 ? (
				<Grid item xs={12}>
					<Container maxWidth="xl">
						<div className={styles.cardContainer}>
							{posts.map((m, i) => {
								return (
									<Grid
										item
										xs={12}
										md={6}
										lg={4}
										xl={12}
										key={i}
									>
										<CompanyCard
											key={i}
											search={search}
											{...m}
										/>
									</Grid>
								);
							})}
						</div>
					</Container>
				</Grid>
			) : (
				<Grid
					item
					xs={12}
					sx={{
						padding: '15px 0',
						height: '300px',
						background: '#fff',
						marginTop: '25px',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Typography variant="h6">No results found</Typography>
				</Grid>
			)}
			<Grid item xs={12}>
				<Pagination
					page={page}
					postsPerPage={postsPerPage}
					totalPosts={numPosts}
					reference={pagination}
					setPage={setPage}
				/>
			</Grid>
		</Container>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default CompaniesTable;
