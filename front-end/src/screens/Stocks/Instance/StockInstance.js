import React, { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import styles from './StockInstance.module.css';
import { ArrowBackIosNew, LanguageSharp as Website } from '@mui/icons-material';
import { Building } from 'react-bootstrap-icons';
import noImage from '../../../images/noimage.png';
import {
	Skeleton,
	Grid,
	CircularProgress,
	Container,
	Typography,
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	IconButton,
} from '@mui/material';
import MiniChart from './MiniChart.tsx';

function StockInstance() {
	const { id } = useParams();
	const [loaded, setLoaded] = useState(false);
	const [stock, setStock] = useState([]);
	const [politicians, setPoliticians] = useState([]);
	const [img, setLogo] = useState();
	var sym = id;

	const navigate = useNavigate();

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};
			fetch('https://api.politistock.me/stock/' + id, requestOptions)
				.then((response) => response.json())
				.then((result) => {
					const stockFetch = result[id];
					const politicianNames = [];

					fetch(
						'https://api.politistock.me/politicians',
						requestOptions
					)
						.then((response) => response.json())
						.then((politicianFetch) => {
							for (
								var i = 0;
								i < stockFetch.politicians.length;
								i++
							) {
								const pol = stockFetch.politicians[i];
								politicianNames.push(
									politicianFetch[pol].preferred_name
								);
							}
							setPoliticians(politicianNames);
						})
						.catch((error) => {
							console.log('error', error);
						});

					if (result[id].companyId === 'None') {
						setLogo(noImage);
					} else {
						fetch(
							'https://api.politistock.me/company/' +
								stockFetch.companyId,
							requestOptions
						)
							.then((response) => response.json())
							.then((result) => {
								const companyLogo =
									result[stockFetch.companyId].logo;
								fetch(companyLogo)
									.then((response) => response.blob())
									.then((imageBlob) => {
										const imageObjectURL =
											URL.createObjectURL(imageBlob);
										setLogo(imageObjectURL);
										setLoaded(true);
									})
									.catch((error) => {
										console.log('error', error);
										setLoaded(true);
									});
							})
							.catch((error) => console.log('error', error));
					}
					setStock(result[id]);
					setLoaded(true);
				})
				.catch((error) => {
					console.log('error', error);
					navigate('/error');
				});
		};

		fetchPosts();
	}, [navigate, id]);

	const hasParentCompany = stock.companyId !== 'N/A';
	const name =
		stock.displayName !== 'N/A'
			? stock.displayName
			: stock.longName !== 'N/A'
			? stock.longName
			: stock.companyId !== 'N/A'
			? stock.companyId
			: 'N/A';
	return loaded && politicians.length > 0 ? (
		<div className={styles.wrapper}>
			<div className={styles.name}>${id}</div>

			<Container maxWidth="md">
				<Grid item container spacing={{ xs: 2, md: 2 }}>
					<Grid
						container
						spacing={{ xs: 2, md: 2 }}
						columns={{ xs: 4, sm: 8, md: 12 }}
						alignItems="center"
					>
						<Grid item xs={hasParentCompany ? 4 : 6} key={3}>
							<Link to={'/stocks/'}>
								<IconButton size="large">
									<ArrowBackIosNew fontSize="inherit" />
								</IconButton>
							</Link>
						</Grid>
					</Grid>
					<Grid item xs={6}>
						<div className={styles.info_block}>
							{loaded ? (
								<div>
									<img
										className={styles.stockImg}
										src={img}
										alt={id + ' logo'}
									></img>
								</div>
							) : (
								<Skeleton
									variant="rectangular"
									width={'100%'}
									height={'250px'}
								/>
							)}
							<div className={styles.info_text}>
								<b>Company Name: </b>
								{name}
							</div>
							<div className={styles.info_text}>
								<b>Analyst Action: </b>
								{stock.analystAction}
							</div>
							<div className={styles.info_text}>
								<b>Exchange: </b>
								{stock.fullExchangeName}
							</div>
							<div className={styles.info_text}>
								<b>Country: </b>
								{stock.country}
							</div>
							<div className={styles.info_text}>
								<b>Sector: </b>
								{stock.sector}
							</div>
							<div className={styles.info_text}>
								<b>Currency: </b>
								{stock.currency}
							</div>
							<a href={stock.website} className={styles.iconLink}>
								<IconButton size="large">
									<Website fontSize="inherit" />
								</IconButton>
								<Typography
									variant="caption"
									sx={{ display: 'block' }}
								>
									Company Site
								</Typography>
							</a>
							{hasParentCompany ? (
								<Link
									to={'/companies/' + stock.companyId}
									className={styles.iconLink}
								>
									<IconButton size="large">
										<Building fontSize="inherit" />
									</IconButton>
									<Typography
										variant="caption"
										sx={{ display: 'block' }}
									>
										See Company
									</Typography>
								</Link>
							) : (
								<></>
							)}
						</div>
					</Grid>

					<Grid item xs={6}>
						<div className={styles.info_block}>
							<TableContainer style={{ maxHeight: '400px' }}>
								<Table stickyHeader>
									<TableHead>
										<TableRow style={{ height: '35px' }}>
											<TableCell
												sx={{ textAlign: 'center' }}
											>
												<Typography variant="h5">
													Invested Politicians
												</Typography>
											</TableCell>
										</TableRow>
									</TableHead>
									<TableBody sx={{ cursor: 'pointer' }}>
										{politicians.map((pol, i) => {
											return (
												<TableRow
													key={pol}
													hover={true}
													onClick={() => {
														navigate(
															'/politicians/' +
																stock
																	.politicians[
																	i
																]
														);
													}}
												>
													<TableCell
														sx={{
															textAlign: 'center',
														}}
													>
														<Typography variant="h6">
															{pol}
														</Typography>
													</TableCell>
												</TableRow>
											);
										})}
									</TableBody>
								</Table>
							</TableContainer>
						</div>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<MiniChart symbol={sym} />
							<div className={styles.disclaimer}>
								Please note: For charts that don't load, the
								given ticker is not public and/or TradingView
								does not have data on it.
							</div>
						</div>
					</Grid>
				</Grid>
			</Container>
			<Container></Container>
		</div>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default StockInstance;
