import React from 'react';

function MiniChart({ symbol }) {
	const ref: { current: HTMLDivElement | null } = React.createRef();

	React.useEffect(() => {
		let refValue: any;

		if (ref.current) {
			const script = document.createElement('script');
			script.src =
				'https://s3.tradingview.com/external-embedding/' +
				'embed-widget-mini-symbol-overview.js';

			script.async = true;
			script.type = 'text/javascript';
			script.innerHTML = JSON.stringify({
				symbol: symbol,
				locale: 'en',
				dateRange: '12M',
				colorTheme: 'dark',
				trendLineColor: '#37a6ef',
				underLineColor: 'rgba(55, 166, 239, 0.15)',
				isTransparent: false,
				autosize: false,
				largeChartUrl: '',
			});

			ref.current.appendChild(script);
			refValue = ref.current;
		}

		return () => {
			if (refValue) {
				while (refValue.firstChild) {
					refValue.removeChild(refValue.firstChild);
				}
			}
		};
	}, [ref, symbol]);

	return <div ref={ref} />;
}

export default MiniChart;
