import React from 'react';
import { TableRow, TableCell } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Highlight from '../../../components/Search/Highlight';

function StockRow({
	id,
	search,
	companyId,
	marketCap,
	displayName,
	longName,
	fullExchangeName,
	fiftyTwoWeekHigh,
	fiftyTwoWeekLow,
}) {
	var formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
	});

	const name =
		displayName !== 'N/A'
			? displayName
			: longName !== 'N/A'
			? longName
			: 'N/A';
	const navigate = useNavigate();

	return (
		<TableRow
			key={id}
			sx={{
				'&:last-child td, &:last-child th': { border: 0 },
				cursor: 'pointer',
			}}
			hover={true}
			onClick={() => {
				navigate('/stocks/' + id);
			}}
		>
			<TableCell component="th" scope="row">
				<Highlight text={id} search={search} />
			</TableCell>
			<TableCell align="left">
				<Highlight text={name} search={search} />
			</TableCell>
			<TableCell align="right">
				<Highlight text={fullExchangeName} search={search} />
			</TableCell>
			<TableCell align="right">
				<Highlight
					text={formatter.format(marketCap).toString()}
					search={search}
				/>
			</TableCell>
			<TableCell align="right">
				<Highlight
					text={formatter.format(fiftyTwoWeekHigh).toString()}
					search={search}
				/>
			</TableCell>
			<TableCell align="right">
				<Highlight
					text={formatter.format(fiftyTwoWeekLow).toString()}
					search={search}
				/>
			</TableCell>
		</TableRow>
	);
}

export default StockRow;
