import React, { useState, useEffect, useRef } from 'react';
import Pagination from '../../../components/Pagination/Pagination';
import styles from './StocksTable.module.css';
import { useSearchParams } from 'react-router-dom';
import {
	CircularProgress,
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	Paper,
	Container,
	Grid,
	Typography,
} from '@mui/material';
import StockRow from './StockRow';
import StockFilters from '../../../components/Filters/StockFilters';
import QuickPagination from '../../../components/Pagination/QuickPagination';
import { useNavigate } from 'react-router-dom';

function StocksTable() {
	const [searchParams] = useSearchParams();
	const navigate = useNavigate();
	const [page, setPage] = useState(parseInt(searchParams.get('page') ?? '1'));
	const [postsPerPage, setPostsPerPage] = useState(
		parseInt(searchParams.get('perPage') ?? '12')
	);

	const [posts, setPosts] = useState([]);
	const [numPosts, setNumPosts] = useState(0);
	const [loaded, setLoaded] = useState(false);
	const pagination = useRef();

	const [sort, setSort] = useState(searchParams.get('sort') ?? '');
	const [marketCap, setMarketCap] = useState(
		searchParams.get('marketcap_high') ?? ''
	);
	const [fiftyTwoWeekLow, setFiftyTwoWeekLow] = useState(
		searchParams.get('52weekLow') ?? ''
	);
	const [fiftyTwoWeekHigh, setFiftyTwoWeekHigh] = useState(
		searchParams.get('52weekHigh') ?? ''
	);
	const [exchange, setExchange] = useState(
		searchParams.get('exchange') ?? ''
	);
	const [search, setSearch] = useState(searchParams.get('search') ?? '');

	const [filterQuery, setFilterQuery] = useState(null);
	const [useFilter, setUseFilter] = useState(false);
	const [order, setOrder] = useState(searchParams.get('order') ?? '');

	window.onpopstate = (e) => {
		setPostsPerPage(parseInt(searchParams.get('perPage') ?? '12'));
		setSort(searchParams.get('sort') ?? '');
		setMarketCap(searchParams.get('marketcap_high') ?? '');
		setFiftyTwoWeekLow(searchParams.get('52weekLow') ?? '');
		setFiftyTwoWeekHigh(searchParams.get('52weekHigh') ?? '');
		setExchange(searchParams.get('exchange') ?? '');
		setSearch(searchParams.get('search') ?? '');
		setOrder(searchParams.get('order') ?? '');
	};

	useEffect(() => {
		var query_string = '';
		var multiple = false;
		var add_q = false;
		var filter_dict = {
			'52weekHigh': fiftyTwoWeekHigh,
			marketcap_high: marketCap,
			'52weekLow': fiftyTwoWeekLow,
			exchange: exchange,
			sort: sort,
			order: order,
			search: search,
		};
		for (let filter in filter_dict) {
			if (filter_dict[filter] !== '') {
				add_q = true;
				if (multiple) {
					query_string += '&';
				}
				var dict_val = filter_dict[filter];
				if (
					filter === 'marketcap_high' &&
					filter_dict[filter] === '100000000000000'
				) {
					filter = 'marketcap_low';
					dict_val = 1000000000000;
				}
				query_string += filter + '=' + dict_val;
				multiple = true;
			}
		}
		if (add_q) {
			query_string = '?' + query_string;
			setUseFilter(true);
		}
		setFilterQuery(query_string);
	}, [
		fiftyTwoWeekHigh,
		marketCap,
		fiftyTwoWeekLow,
		exchange,
		sort,
		order,
		search,
	]);

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};
			if (filterQuery && filterQuery !== '') {
				navigate(
					(filterQuery ? filterQuery + '&' : '?') +
						`page=${page}&perPage=${postsPerPage}`
				);
			}
			fetch(
				'https://api.politistock.me/stocks/' +
					postsPerPage +
					':' +
					(page - 1) * postsPerPage +
					filterQuery,
				requestOptions
			)
				.then((response) => response.json())
				.then((result) => {
					var postArr = [];
					for (const [key, value] of Object.entries(result)) {
						if (key !== 'num_entries') {
							postArr.push(value);
						} else {
							setNumPosts(value);
						}
					}
					setLoaded(true);
					setPosts(postArr);
				})
				.catch((error) => console.log('error', error));
		};
		if (filterQuery !== null) {
			fetchPosts();
		}
	}, [page, postsPerPage, filterQuery, useFilter, navigate]);

	return loaded ? (
		<Container maxWidth="xl">
			<Grid item xs={12}>
				<Typography variant="h2" className={styles.title}>
					Traded Stocks
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<StockFilters
					setPage={setPage}
					sortHook={[sort, setSort]}
					capHook={[marketCap, setMarketCap]}
					fiftyTwoWeekLowHook={[fiftyTwoWeekLow, setFiftyTwoWeekLow]}
					fiftyTwoWeekHighHook={[
						fiftyTwoWeekHigh,
						setFiftyTwoWeekHigh,
					]}
					excHook={[exchange, setExchange]}
					queryHook={[filterQuery, setFilterQuery]}
					filterHook={[useFilter, setUseFilter]}
					orderHook={[order, setOrder]}
					searchHook={[search, setSearch]}
				/>
			</Grid>
			<Grid item xs={12}>
				<QuickPagination
					pagination={pagination}
					posts={postsPerPage}
					setPosts={setPostsPerPage}
				/>
			</Grid>
			{posts.length > 0 ? (
				<Grid item xs={12}>
					<TableContainer
						component={Paper}
						sx={{ marginTop: '20px' }}
					>
						<Table sx={{ minWidth: 650 }} aria-label="simple table">
							<TableHead>
								<TableRow>
									<TableCell>Ticker</TableCell>
									<TableCell align="left">
										Company Name
									</TableCell>
									<TableCell align="right">
										Exchange
									</TableCell>
									<TableCell align="right">
										Market Cap
									</TableCell>
									<TableCell align="right">
										52 Week High
									</TableCell>
									<TableCell align="right">
										52 Week Low
									</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{posts.map((post, i) => {
									return (
										<StockRow
											key={post.id}
											search={search}
											{...post}
										/>
									);
								})}
							</TableBody>
						</Table>
					</TableContainer>
				</Grid>
			) : (
				<Grid
					item
					xs={12}
					sx={{
						padding: '15px 0',
						height: '300px',
						background: '#fff',
						marginTop: '25px',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Typography variant="h6">No results found</Typography>
				</Grid>
			)}
			<Grid item xs={12}>
				<Pagination
					page={page}
					postsPerPage={postsPerPage}
					totalPosts={numPosts}
					reference={pagination}
					setPage={setPage}
				/>
			</Grid>
		</Container>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default StocksTable;
