import React, { useState, useEffect, useRef } from 'react';
import Pagination from '../../../components/Pagination/Pagination';
import styles from './PoliticiansTable.module.css';
import PoliticianCard from '../../../components/Cards/PoliticianCard';
import { CircularProgress, Typography, Container, Grid } from '@mui/material';
import PoliticianFilters from '../../../components/Filters/PoliticianFilters';
import { useSearchParams, useNavigate } from 'react-router-dom';
import QuickPagination from '../../../components/Pagination/QuickPagination';

function PoliticiansTable() {
	const [searchParams] = useSearchParams();
	const navigate = useNavigate();
	const [page, setPage] = useState(parseInt(searchParams.get('page') ?? '1'));
	const [postsPerPage, setPostsPerPage] = useState(
		parseInt(searchParams.get('perPage') ?? '12')
	);

	const [posts, setPosts] = useState([]);
	const [numPosts, setNumPosts] = useState(0);
	const [loaded, setLoaded] = useState(false);
	const pagination = useRef();

	const [sort, setSort] = useState(searchParams.get('sort') ?? '');
	const [party, setParty] = useState(searchParams.get('party') ?? '');
	const [state, setState] = useState(searchParams.get('state') ?? '');
	const [trades, setTrades] = useState(searchParams.get('trade_high') ?? '');
	const [tradeVol, setTradeVol] = useState(
		searchParams.get('tradevol_high') ?? ''
	);
	const [chamber, setChamber] = useState(searchParams.get('chamber') ?? '');
	const [search, setSearch] = useState(searchParams.get('search') ?? '');
	const [address, setAddress] = useState(searchParams.get('address') ?? '');

	const [filterQuery, setFilterQuery] = useState(null);
	const [useFilter, setUseFilter] = useState(false);
	const [order, setOrder] = useState(searchParams.get('order') ?? '');

	window.onpopstate = (e) => {
		setPostsPerPage(parseInt(searchParams.get('perPage') ?? '12'));
		setSort(searchParams.get('sort') ?? '');
		setParty(searchParams.get('party') ?? '');
		setState(searchParams.get('state') ?? '');
		setTrades(searchParams.get('trade_high') ?? '');
		setTradeVol(searchParams.get('tradevol_high') ?? '');
		setChamber(searchParams.get('chamber') ?? '');
		setSearch(searchParams.get('search') ?? '');
		setAddress(searchParams.get('address') ?? '');
		setOrder(searchParams.get('order') ?? '');
	};

	useEffect(() => {
		var query_string = '';
		var multiple = false;
		var add_q = false;
		var filter_dict = {
			party: party,
			state: state,
			trade_high: trades,
			tradevol_high: tradeVol,
			address: address,
			sort: sort,
			order: order,
			search: search,
			chamber: chamber,
		};
		for (let filter in filter_dict) {
			if (filter_dict[filter] !== '') {
				add_q = true;
				if (multiple) {
					query_string += '&';
				}
				var dict_val = filter_dict[filter];
				if (
					filter === 'trade_high' &&
					filter_dict[filter] === '1000000'
				) {
					filter = 'trade_low';
					dict_val = 1000;
				}
				if (
					filter === 'tradevol_high' &&
					filter_dict[filter] === '1000000000'
				) {
					filter = 'tradevol_low';
					dict_val = 50000000;
				}
				query_string += filter + '=' + dict_val;
				multiple = true;
			}
		}
		if (add_q) {
			query_string = '?' + query_string;
			setUseFilter(true);
		}
		setFilterQuery(query_string);
	}, [sort, order, party, state, trades, tradeVol, address, search, chamber]);

	useEffect(() => {
		setLoaded(false);
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};
			if (filterQuery !== '') {
				navigate(
					(filterQuery ? filterQuery + '&' : '?') +
						`page=${page}&perPage=${postsPerPage}`
				);
			}
			fetch(
				'https://api.politistock.me/politicians/' +
					postsPerPage +
					':' +
					(page - 1) * postsPerPage +
					filterQuery,
				requestOptions
			)
				.then((response) => response.json())
				.then((result) => {
					var postArr = [];
					for (const [key, value] of Object.entries(result)) {
						if (key !== 'num_entries') {
							postArr.push(value);
						} else {
							setNumPosts(value);
						}
					}
					setPosts(postArr);
					setLoaded(true);
				})
				.catch((error) => console.log('error', error));
		};
		if (filterQuery !== null) {
			fetchPosts();
		}
	}, [page, postsPerPage, filterQuery, useFilter, navigate]);

	return loaded ? (
		<Container maxWidth="xl">
			<Grid item xs={12}>
				<Typography variant="h2" className={styles.title}>
					Politicians
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<PoliticianFilters
					setPage={setPage}
					sortHook={[sort, setSort]}
					partyHook={[party, setParty]}
					stateHook={[state, setState]}
					tradesHook={[trades, setTrades]}
					volHook={[tradeVol, setTradeVol]}
					chamberHook={[chamber, setChamber]}
					addressHook={[address, setAddress]}
					queryHook={[filterQuery, setFilterQuery]}
					filterHook={[useFilter, setUseFilter]}
					orderHook={[order, setOrder]}
					searchHook={[search, setSearch]}
				/>
			</Grid>
			<Grid item xs={12}>
				<QuickPagination
					pagination={pagination}
					posts={postsPerPage}
					setPosts={setPostsPerPage}
				/>
			</Grid>
			{posts.length > 0 ? (
				<Grid item xs={12}>
					<Container maxWidth="xl">
						<div className={styles.cardContainer}>
							{posts.map((m, i) => {
								return (
									<Grid
										item
										xs={12}
										md={6}
										lg={4}
										xl={3}
										key={i}
									>
										<PoliticianCard
											key={i}
											search={search}
											{...m}
										/>
									</Grid>
								);
							})}
						</div>
					</Container>
				</Grid>
			) : (
				<Grid
					item
					xs={12}
					sx={{
						padding: '15px 0',
						height: '300px',
						background: '#fff',
						marginTop: '25px',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Typography variant="h6">No results found</Typography>
				</Grid>
			)}
			<Grid item xs={12}>
				<Pagination
					page={page}
					postsPerPage={postsPerPage}
					totalPosts={numPosts}
					reference={pagination}
					setPage={setPage}
				/>
			</Grid>
		</Container>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default PoliticiansTable;
