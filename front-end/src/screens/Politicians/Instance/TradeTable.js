import styles from './TradeTable.module.css';

import {
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	Paper,
} from '@mui/material';
import TradeRow from './TradeRow';

function TradeTable({ trades }) {
	return (
		<div className={styles.tableContainer}>
			<TableContainer component={Paper}>
				<Table sx={{ minWidth: 650 }} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell>Ticker</TableCell>
							<TableCell align="left">Issuer</TableCell>
							<TableCell align="right">Type</TableCell>
							<TableCell align="right">Value</TableCell>
							<TableCell align="right">Publish Date</TableCell>
							<TableCell align="right">Trade Date</TableCell>
							<TableCell align="right">Delay</TableCell>
						</TableRow>
					</TableHead>
					<TableBody sx={{ cursor: 'pointer' }}>
						{trades.map((trade, i) => {
							return <TradeRow key={i} {...trade} />;
						})}
					</TableBody>
				</Table>
			</TableContainer>
		</div>
	);
}

export default TradeTable;
