import React from 'react';
import { TableRow, TableCell } from '@mui/material';
import { useNavigate } from 'react-router-dom';

function TradeRow({
	id,
	delay,
	issuer,
	pub_date,
	ticker,
	traded_date,
	type,
	value,
}) {
	const navigate = useNavigate();

	return (
		<TableRow
			key={id}
			sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
			hover={true}
			onClick={() => {
				if (ticker !== 'N/A') {
					navigate('/stocks/' + ticker.match(/([A-Z])+/g)[0]);
				}
			}}
		>
			<TableCell component="th" scope="row">
				{ticker}
			</TableCell>
			<TableCell align="left">{issuer}</TableCell>
			<TableCell align="right">{type}</TableCell>
			<TableCell align="right">${value}</TableCell>
			<TableCell align="right">{pub_date}</TableCell>
			<TableCell align="right">{traded_date}</TableCell>
			<TableCell align="right">{delay}</TableCell>
		</TableRow>
	);
}

export default TradeRow;
