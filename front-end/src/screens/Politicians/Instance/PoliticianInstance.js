import React, { useState, useEffect } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { ArrowBackIosNew } from '@mui/icons-material';
import { Twitter } from 'react-bootstrap-icons';
import styles from './PoliticiansInstance.module.css';
import {
	Skeleton,
	Grid,
	CircularProgress,
	Container,
	Typography,
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	IconButton,
} from '@mui/material';
import noImage from '../../../images/noimage.png';
import TradeTable from './TradeTable';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import SectorViz from '../../../components/Charts/SectorViz';
import CompanyViz from '../../../components/Charts/CompanyViz';
import LocationsViz from '../../../components/Charts/LocationsViz';
import YearInvestmentViz from '../../../components/Charts/YearInvestmentViz';

// Parse Min Investment and Max Investment Range
const parseScore = (value) => {
	const match = value.match(/(\d)+[KM]/g);
	const min = match[0];
	const max = match[1];

	const minFactor = min.charAt(min.length - 1);
	const minNumber = parseInt(min.substring(0, min.length - 1));

	const maxFactor = max.charAt(max.length - 1);
	const maxNumber = parseInt(max.substring(0, max.length - 1));

	const range = [
		minNumber * (minFactor === 'K' ? 1000 : 1000000),
		maxNumber * (maxFactor === 'K' ? 1000 : 1000000),
	];

	return range;
};

const getMaxScores = (value) => {
	const match = value.match(/(\d)+[KM]/g);
	const max = match[1];
	if (max) {
		const maxFactor = max.charAt(max.length - 1);
		const maxNumber = parseInt(max.substring(0, max.length - 1));

		const range = maxNumber * (maxFactor === 'K' ? 1000 : 1000000);

		return range;
	} else {
		return 0;
	}
};

const camelCase = (string) => {
	return string
		.match(/(\w)+/g)
		.map(
			(word) =>
				word.charAt(0).toUpperCase() +
				word.substring(1, word.length) +
				' '
		);
};

const formatRange = (string) => {
	const vals = string.split(' - ');
	return (
		<div className={styles.info_smaller_range}>
			{formatter.format(vals[0]) + ' - ' + formatter.format(vals[1])}
		</div>
	);
};

const getLocation = (string) => {
	const vals = string.split(' ');
	if (vals.length > 4) {
		const state = vals[vals.length - 3];
		if (state.length > 2) return 'International';
		return state;
	}
	return;
};

var formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
});

function PoliticianInstance() {
	const { id } = useParams();
	const [loaded, setLoaded] = useState(false);
	const [sectorsLoaded, setSectorsLoaded] = useState(false);
	const [companyInvestment, setCompanyInvestment] = useState({});
	const [politician, setPolitician] = useState({});
	const [companies, setCompanies] = useState([]);
	const [locations, setLocationCount] = useState({});
	const [years, setYears] = useState({});
	const [handle, setHandle] = useState('');
	const [sectorScores, setSectorScores] = useState(undefined);
	const navigate = useNavigate();

	useEffect(() => {
		const fetchPosts = async () => {
			setLoaded(false);
			var requestOptions = {
				method: 'GET',
				redirect: 'follow',
			};

			fetch('https://api.politistock.me/politician/' + id, requestOptions)
				.then((response) => response.json())
				.then((result) => {
					var twitter_handle = result[id].twitter;
					var idx = 0;
					for (var i = 0; i < twitter_handle.length; i++) {
						if (twitter_handle.charAt(i) === '@') {
							idx = i;
						}
					}
					twitter_handle = twitter_handle.substring(idx + 1);
					setHandle(twitter_handle);
					setPolitician(result[id]);
					setLoaded(true);

					const companyNames = [];
					const counts = {};

					fetch(
						'https://api.politistock.me/companies',
						requestOptions
					)
						.then((response) => response.json())
						.then((companiesFetch) => {
							for (
								var i = 0;
								i < result[id].companies.length;
								i++
							) {
								const company = result[id].companies[i];
								const place = getLocation(
									companiesFetch[company].location
								);
								counts[place] =
									counts[place] === undefined
										? 1
										: counts[place] + 1;
								companyNames.push(companiesFetch[company].name);
							}
							setLocationCount(counts);
							setCompanies(companyNames);
						})
						.catch((error) => {
							console.log('error', error);
						});

					// fetch industry info
					fetch('https://api.politistock.me/stocks', requestOptions)
						.then((response) => response.json())
						.then((stocks) => {
							const sectorCount = {};
							const companyInvestment = {};
							const { trades } = result[id];
							const year_count = {};

							trades.forEach((trade) => {
								const { type, value, ticker, traded_date } =
									trade;

								const year = traded_date.split('-')[0];
								const max = getMaxScores(value);
								year_count[year] =
									year_count[year] === undefined
										? max
										: year_count[year] + max;

								if (type === 'buy' && ticker !== 'N/A') {
									const stockID =
										ticker.match(/([A-Z])+/g)[0];

									if (stocks[stockID] !== undefined) {
										const stock = stocks[stockID];
										const sector = stock.sector;
										const range = parseScore(value);
										if (sector !== 'N/A') {
											sectorCount[sector] =
												sectorCount[sector] ===
												undefined
													? range
													: [
															sectorCount[
																sector
															][0] + range[0],
															sectorCount[
																sector
															][1] + range[1],
													  ];
										}

										const name =
											stock.displayName !== 'N/A'
												? stock.displayName
												: stock.longName !== 'N/A'
												? stock.longName
												: stock.companyId !== 'N/A'
												? stock.companyId
												: 'N/A';
										if (name !== 'N/A') {
											companyInvestment[name] =
												companyInvestment[name] ===
												undefined
													? range
													: [
															companyInvestment[
																name
															][0] + range[0],
															companyInvestment[
																name
															][1] + range[1],
													  ];
										}
									}
								}
							});
							setYears(year_count);
							setCompanyInvestment(companyInvestment);
							setSectorScores(sectorCount);
							setSectorsLoaded(true);
						})
						.catch((error) => console.log('error', error));
					setPolitician(result[id]);
				})
				.catch((error) => {
					console.log('error', error);
					navigate('/error');
				});
		};

		fetchPosts();
	}, [id, navigate, politician.imgs]);

	const checkLocations = (map) => {
		return locations ? locations : {};
	};

	return loaded && sectorsLoaded ? (
		<div className={styles.wrapper}>
			<div className={styles.name}>
				{loaded ? politician.name : '404'}
			</div>
			<hr></hr>
			<Container maxWidth="md">
				<Grid container spacing={{ xs: 2, md: 2 }}>
					<Grid item container maxWidth="md" alignItems="center">
						<Link to={'/politicians'}>
							<IconButton size="large">
								<ArrowBackIosNew fontSize="inherit" />
							</IconButton>
						</Link>
					</Grid>
					<Grid item xs={6}>
						{loaded ? (
							<div
								style={{
									padding: '10px',
									width: '100%',
									height: '100%',
									display: 'flex',
									justifyContent: 'center',
									alignItems: 'center',
									position: 'relative',
									overflow: 'hidden',
								}}
							>
								<img
									className={styles.politicianImg}
									src={politician.imgs ?? noImage}
									alt={id + ' logo'}
								></img>
							</div>
						) : (
							<Skeleton
								variant="rectangular"
								width={'100%'}
								height={'250px'}
							/>
						)}
					</Grid>
					<Grid item xs={6}>
						<div className={styles.info_block}>
							<div className={styles.info_text}>
								<b>Age: </b>
								{politician.age}
							</div>
							<div className={styles.info_text}>
								<b>State: </b>
								{camelCase(politician.state)}
							</div>
							<div className={styles.info_text}>
								<b>Party: </b>
								{camelCase(politician.party)}
							</div>
							<div className={styles.info_text}>
								<b>Chamber: </b>
								{camelCase(politician.chamber)}
							</div>
							<div className={styles.info_text}>
								<b>Role: </b>
								{camelCase(politician.role)}
							</div>
							<div className={styles.info_text}>
								<b>Birthdate: </b>
								{politician.dob}
							</div>
							<div className={styles.info_text}>
								<b>Phone: </b>
								{politician.phone}
							</div>
							<div className={styles.info_text}>
								<b>Last Trade: </b>
								{politician.last_trade}
							</div>
							<div className={styles.info_text}>
								<b>Number of Trades: </b>
								{politician.num_trades}
							</div>
							<a href={politician.twitter}>
								<IconButton size="large">
									<Twitter fontSize="inherit" />
								</IconButton>
							</a>
						</div>
					</Grid>
					<Grid item xs={4} md={4}>
						<div className={styles.info_small_block}>
							<div className={styles.info_text}>
								<b>Traded Volume: </b>
								{formatter.format(politician.traded_volume)}
							</div>
						</div>
					</Grid>
					<Grid item xs={4} md={4}>
						<div className={styles.info_small_block}>
							<div className={styles.info_text}>
								<b>Traded Buy Calls: </b>
								{formatRange(politician.buy_volume)}
							</div>
						</div>
					</Grid>
					<Grid item xs={4} md={4}>
						<div className={styles.info_small_block}>
							<div className={styles.info_text}>
								<b>Traded Sell Calls: </b>
								{formatRange(politician.sell_volume)}
							</div>
						</div>
					</Grid>
					<Grid item xs={6}>
						<div className={styles.info_block}>
							<TableContainer style={{ maxHeight: '400px' }}>
								<Table stickyHeader>
									<TableHead>
										<TableRow style={{ height: '35px' }}>
											<TableCell
												sx={{ textAlign: 'left' }}
											>
												<Typography variant="h5">
													Invested Stocks
												</Typography>
											</TableCell>
										</TableRow>
									</TableHead>
									<TableBody sx={{ cursor: 'pointer' }}>
										{politician.tickers.map((tick, i) => {
											return tick !== '' ? (
												<TableRow
													key={tick}
													hover={true}
													onClick={() => {
														navigate(
															'/stocks/' + tick
														);
													}}
												>
													<TableCell
														sx={{
															textAlign: 'left',
														}}
													>
														<Typography variant="h6">
															${tick}
														</Typography>
													</TableCell>
												</TableRow>
											) : (
												<></>
											);
										})}
									</TableBody>
								</Table>
							</TableContainer>
						</div>
					</Grid>
					<Grid item xs={6}>
						<div className={styles.info_block}>
							<TableContainer style={{ maxHeight: '400px' }}>
								<Table stickyHeader>
									<TableHead>
										<TableRow style={{ height: '35px' }}>
											<TableCell
												sx={{ textAlign: 'left' }}
											>
												<Typography variant="h5">
													Invested Companies
												</Typography>
											</TableCell>
										</TableRow>
									</TableHead>
									<TableBody sx={{ cursor: 'pointer' }}>
										{companies.map((company, i) => {
											return (
												<TableRow
													key={company}
													hover={true}
													onClick={() => {
														navigate(
															'/companies/' +
																politician
																	.companies[
																	i
																]
														);
													}}
												>
													<TableCell
														sx={{
															textAlign: 'left',
														}}
													>
														<Typography variant="h6">
															{company}
														</Typography>
													</TableCell>
												</TableRow>
											);
										})}
									</TableBody>
								</Table>
							</TableContainer>
						</div>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<div className={styles.chart}>
								<SectorViz sectors={sectorScores} />
							</div>
						</div>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<div className={styles.chart}>
								<CompanyViz companies={companyInvestment} />
							</div>
						</div>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<div className={styles.chart}>
								<LocationsViz locations={checkLocations()} />
							</div>
						</div>
					</Grid>
					<Grid item xs={12}>
						<div className={styles.info_block}>
							<div className={styles.chart_bigger}>
								<YearInvestmentViz years={years} />
							</div>
						</div>
					</Grid>

					<Grid item xs={12}>
						<TradeTable trades={politician.trades} />
					</Grid>
					<Grid item xs={12}>
						<div className={styles.company_info}>
							<TwitterTimelineEmbed
								sourceType="profile"
								screenName={handle}
								options={{ height: '900px' }}
							/>
						</div>
					</Grid>
				</Grid>
			</Container>
		</div>
	) : (
		<div className={styles.loadWrapper}>
			<CircularProgress />
		</div>
	);
}

export default PoliticianInstance;
