import Home from './screens/Home/Home';
import About from './screens/About/About';
import PoliticiansTable from './screens/Politicians/Table/PoliticiansTable';
import CompaniesTable from './screens/Companies/Table/CompaniesTable';
import StocksTable from './screens/Stocks/Table/StocksTable';
import PoliticianInstance from './screens/Politicians/Instance/PoliticianInstance';
import CompaniesInstance from './screens/Companies/Instance/CompaniesInstance';
import StockInstance from './screens/Stocks/Instance/StockInstance';
import Error from './screens/Error/Error';
import Search from './screens/Search/Search';
import PolitistockViz from './screens/PolitistockViz/PolitistockViz';
import ProviderViz from './screens/ProviderViz/ProviderViz';

const Screens = [
	{
		path: '/',
		Component: Home,
		exact: true,
	},
	{
		path: '/about',
		Component: About,
		exact: true,
	},
	{
		path: '/search',
		Component: Search,
		exact: true,
	},
	{
		path: '/politistockViz',
		Component: PolitistockViz,
		exact: true,
	},
	{
		path: '/providerViz',
		Component: ProviderViz,
		exact: true,
	},
	{
		path: '/politicians',
		Component: PoliticiansTable,
		exact: true,
	},
	{
		path: '/stocks',
		Component: StocksTable,
		exact: true,
	},
	{
		path: '/companies',
		Component: CompaniesTable,
		exact: true,
	},
	{
		path: 'politicians/:id',
		Component: PoliticianInstance,
		exact: true,
	},
	{
		path: 'companies/:id',
		Component: CompaniesInstance,
		exact: true,
	},
	{
		path: 'stocks/:id',
		Component: StockInstance,
		exact: true,
	},
	{
		path: '*',
		Component: Error,
		exact: true,
	},
];

export { Screens };
