import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';
import { Screens } from './Screens';

function App() {
	return (
		<div className="App">
			<Router>
				<Navbar />
				<Routes>
					{Screens.map(({ path, Component, exact }) => {
						return (
							<Route
								key={path}
								path={path}
								element={<Component />}
							/>
						);
					})}
				</Routes>
			</Router>
		</div>
	);
}

export default App;
