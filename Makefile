
.DEFAULT_GOAL := all
SHELL         := bash

init:
	echo Initializing Environment
	cd front-end/ && npm install

start:
	echo "Opening Website"
	cd front-end/ && npm start

front-test:
	echo "running front-end tests"

back-test:
	echo "running back-end tests"

python-tests:
	echo "running python tests"
	python3 backend/docker-flask-mysql/app/unittests.py -v

backend-build:
	echo "running docker-compose down and docker-compose up in the backend"
	cd backend/docker-flask-mysql && docker-compose down && docker-compose build --no-cache && docker-compose up

backend-build-dev:
	echo "running docker-compose down and docker-compose up in the backend"
	cd backend/docker-flask-mysql && docker-compose -f docker-compose.dev.yml down && docker-compose -f docker-compose.dev.yml build --no-cache && docker-compose -f docker-compose.dev.yml up

deploy-api-prod:
	echo "deploying api prod"
	cd backend/docker-flask-mysql && eb deploy politistock-api-v3

clean:
	echo "running clean"

pretty:
	echo "Making pretty!"
	cd front-end && npx prettier --write src  
	