import csv

"""
First, create a dict of {stock : company}
Then create a dict of {stock : [politicians]} and {company: [politicians]} concurrently
 --> also create a list of companies for each politician during this
Then add [politicians] to each stock row
Then add [politicians] to each company row
"""

# declare original CSV file names: for reading only
stock_raw_path = "../data-mod/modify-by-model/stocks-new.csv"
pol_raw_path = "../data-mod/modify-by-model/politicians-new.csv"
com_raw_path = "../data-mod/modify-by-model/companies-new.csv"

# declare new CSV file names: for writing
stock_mod_path = "../data-mod/link-models/stocks-linked.csv"
pol_mod_path = "../data-mod/link-models/politicians-linked.csv"
com_mod_path = "../data-mod/link-models/companies-linked.csv"

# linking dicts
stock_company_map = {}
stock_pol_map = {}
company_pol_map = {}
company_stock_map = {}
pol_stock_map = {}
pol_company_map = {}


def generate_stock_com_map():
    row_count = 0
    final_count = 0
    with open(stock_raw_path, "r", encoding="utf8") as stocks:
        stock_reader = csv.reader(stocks)
        for row in stock_reader:
            row_count += 1
            ticker = row[0]
            companyID = row[1]
            if companyID != "None":
                stock_company_map[ticker] = companyID
                final_count += 1
    print(
        f"Original stock instance count: {row_count}, Final stock instance count: {final_count}"
    )


def generate_company_stock_map():
    row_count = 0
    final_count = 0
    with open(com_raw_path, "r", encoding="utf8") as companies:
        company_reader = csv.reader(companies)
        for row in company_reader:
            row_count += 1
            ticker = row[15]
            company_id = row[0]
            if ticker != "None" and ticker != "":
                company_stock_map[company_id] = ticker
                final_count += 1
    print(
        f"Original company instance count: {row_count}, Final company instance count: {final_count}"
    )


def pol_mapper():
    line_count = 0  # number of politicians
    with open(pol_raw_path, "r", encoding="utf8") as politicians:
        pol_reader = csv.reader(politicians)
        for row in pol_reader:
            if line_count > 0:
                polID = row[0]
                tickers = row[14]
                stock_set = get_stock_set(tickers)
                if len(stock_set) > 0:  # politician has stock links
                    # link to {politician: [stocks]}
                    pol_stock_map[polID] = list(stock_set)
                companies = []
                for ticker in stock_set:  # extract company ID's
                    if ticker in stock_company_map:
                        companyID = stock_company_map.get(ticker)
                        companies.append(companyID)

                        # link to {company: [politicians]}
                        default_pol_list = []
                        company_pol_list = company_pol_map.get(
                            companyID, default_pol_list
                        )
                        company_pol_list.append(polID)
                        company_pol_map[companyID] = company_pol_list

                    # link to {stock: [politicians]}
                    default_pol_list = []
                    stock_pol_list = stock_pol_map.get(ticker, default_pol_list)
                    stock_pol_list.append(polID)
                    stock_pol_map[ticker] = stock_pol_list

                # finished looping through all tickers
                if (
                    len(companies) > 0 or len(stock_set) > 0
                ):  # don't add isolated politician rows
                    # link to {politician: [companies]}
                    pol_company_map[polID] = companies
            line_count += 1
    print(
        f"Number of linked companies: {len(company_pol_map)}, Number of linked stocks: {len(stock_pol_map)}"
    )


def get_stock_set(ticker_string):
    stock_set = set()
    ticker_string = ticker_string.replace("[", "")
    ticker_string = ticker_string.replace("]", "")
    ticker_string = ticker_string.replace("'", "")
    ticker_string = ticker_string.replace(" ", "")
    stock_list = ticker_string.split(",")
    stock_set.update(stock_list)
    return stock_set


def update_stocks():
    stock_write_count = 0
    with open(stock_raw_path, "r", encoding="utf8") as stocks:
        stock_reader = csv.reader(stocks)
        with open(stock_mod_path, "w", encoding="utf8", newline="") as stock_linked:
            stock_writer = csv.writer(stock_linked, lineterminator="\n")
            line_count = 0
            # copy it over, add a field at the end
            for row in stock_reader:
                if line_count == 0:
                    row.append("politicians")
                    stock_writer.writerow(row)
                else:
                    ticker = row[0]
                    default_list = []
                    pol_list = stock_pol_map.get(ticker, default_list)
                    # don't add stock with no links
                    if (
                        (ticker in stock_pol_map) and (ticker in stock_company_map)
                    ) and ticker != "N/A":
                        row.append(pol_list)
                        stock_writer.writerow(row)
                        stock_write_count += 1
                line_count += 1
    print(f"New number of stock rows: {stock_write_count}, Old Number: {line_count}")


def update_companies():
    company_write_count = 0
    with open(com_raw_path, "r", encoding="utf8") as companies:
        company_reader = csv.reader(companies)
        with open(com_mod_path, "w", encoding="utf8", newline="") as com_linked:
            company_writer = csv.writer(com_linked, lineterminator="\n")
            line_count = 0
            # copy it over, add a field at the end
            for row in company_reader:
                if line_count == 0:
                    row.append("politicians")
                    company_writer.writerow(row)
                else:
                    companyID = row[0]
                    if companyID == "n/a1":
                        continue
                    default_list = []
                    pol_list = company_pol_map.get(companyID, default_list)
                    # don't add company with no links
                    if (
                        companyID in company_pol_map and companyID in company_stock_map
                    ) and companyID != "n/a":
                        row.append(pol_list)
                        company_writer.writerow(row)
                        company_write_count += 1

                line_count += 1
    print(
        f"New number of company rows: {company_write_count}, Old Number: {line_count}"
    )


def update_politicians():
    politician_write_count = 0
    with open(pol_raw_path, "r", encoding="utf8") as politicians:
        pol_reader = csv.reader(politicians)
        with open(pol_mod_path, "w", encoding="utf8", newline="") as pol_linked:
            pol_writer = csv.writer(pol_linked, lineterminator="\n")
            line_count = 0
            # copy it over, add a field at the end
            for row in pol_reader:
                if line_count == 0:
                    row.append("companies")
                    pol_writer.writerow(row)
                else:
                    polID = row[0]
                    default_row = []
                    stock_list = pol_stock_map.get(polID, default_row)
                    default_row = []
                    company_list = pol_company_map.get(polID, default_row)
                    # don't add politician with no links
                    if polID in pol_stock_map and polID in pol_company_map:
                        row.append(company_list)
                        row[14] = stock_list
                        pol_writer.writerow(row)
                        politician_write_count += 1
                line_count += 1
    print(
        f"New number of politician rows: {politician_write_count}, Old Number: {line_count}"
    )


if __name__ == "__main__":
    print("Initiating csv change...")
    generate_stock_com_map()
    generate_company_stock_map()
    pol_mapper()
    update_stocks()
    update_companies()
    update_politicians()
