import csv
from hashlib import new
import json

"""
old columns: 
name,preferred_name,num_trades,traded_volume,last_trade,
party,state,role,chamber,dob,
trades

new columns: 
id,name,preferred_name,num_trades,traded_volume,
last_trade,party,state,role,chamber,
age,dob,trades,tickers

"""

# declare original CSV file names: for reading only
pol_raw_path = "../data/politicians.csv"

# declare new CSV file names: for writing
pol_mod_path = "../data-mod/modify-by-model/politicians-new.csv"

# helper dicts
politicians = {}

# columns to delete from the raw file
delete_cols = [

    ]

# save original header to get index values while parsing
original_header = []


# driver to create politician IDs for each data point.
# Eventually, this driver should also create a list of all stocks
# associated with a given politician
def politican_driver():
    line_count = 0

    # open original and writing files
    with open(pol_raw_path, "r") as pol_raw_csv:
        pol_reader = csv.reader(pol_raw_csv, delimiter=",")
        with open(pol_mod_path, "w", newline="", encoding="utf-8") as pol_mod_csv:
            pol_writer = csv.writer(pol_mod_csv)
            # read original row by row
            for row in pol_reader:
                parse_row(row, line_count, pol_writer)
                line_count += 1

# main row parsing: remove unwanted fields and improper formatting
def parse_row(row, line_count, writer):
    new_row = []
    new_row = clean_row(row, new_row)
    if line_count == 0:
        for r in row: # save header row
            original_header.append(r)
        print(f"Headers: {new_row}")
        writer.writerow(new_row)
    else:
        # add new columns: list of tickers, buy volume, sell volume, state id
        trades = row[original_header.index("trades")]
        ticker_list = parse_trades(trades)
        new_row[new_row.index("tickers")] = ticker_list
        buy_sell = init_buy_sell(trades)
        new_row[new_row.index("buy_volume")] = buy_sell["buy_trades"]
        new_row[new_row.index("sell_volume")] = buy_sell["sell_trades"]
        new_row[new_row.index("state_id")] = generate_state_id(row[original_header.index("state")])
        
        # generate ID for each rowID field with generated companyID
        name = row[original_header.row("name")]
        new_name = create_pol_ID(name)
        new_row[0] = new_name
        writer.writerow(new_row)


def clean_row(row, new_row):
    index = 0  # corresponds to the index in 'row' (not 'new_row')
    new_row.append("id")
    for s in row:
        new_string = reformat(s, index)
        if index == original_header.index("dob"):
            strings = new_string.split(" ")
            age = "age"
            if len(strings) != 2:  # print error, append blank age
                print(f"error parsing DOB, parsed: {strings} \t original: {s}")
            else:
                new_string = strings[0]
                age = strings[1]
            new_row.append(age)
        new_row.append(new_string)
        index += 1
    new_row.append("tickers")
    new_row.append("buy_volume")
    new_row.append("sell_volume")
    new_row.append("state_id")
    return new_row


# reformat data to fit SQL format
def reformat(string, index):
    string = string.replace("$", "")
    if index == original_header.index("num_trades") or index == original_header.index("traded_volume"):
        return reformat_num(string)
    if index == original_header.index("last_trade"):
        return reformat_last_trade("string")
    if index == original_header.index("role"):
        return reformat_role(string)
    if index == original_header.index("dob"):
        return reformat_dob(string)

    return string

# Get rid of extra spaces and commas
def reformat_num(num):
    num = num.replace(",", "")
    num = num.replace(" ", "")
    return num

# must be in YYYY-MM-DD format
def reformat_last_trade(last_trade):
    dates = last_trade.split("/")
    index = len(dates) - 1
    new_string = ""
    while index >= 0:
        value = dates[index]
        if len(value) < 2:  # add a padding 0
            value = "0" + value
        new_string += value
        if index > 0:
            new_string += "-"
        index -= 1
    if len(new_string) != 10:
        print(f"ERROR: date is wrong: {new_string}")
    return new_string

# remove the space in front
def reformat_role(role):
    role = role.replace(" ", "") 
    return role


def reformat_dob(dob):
    # return format: YYYY-MM-DD AGE
    # in clean_row, separate them
    dob = dob.replace("(", "")
    dob = dob.replace(")", "")
    return dob


def create_pol_ID(name):
    # lower case and split by words, join words together (remove spaces)
    lower_case = name.lower()
    names = lower_case.split()
    new_name = ""
    # start first two initials and last name
    index = 0
    while index < len(names):
        if index == len(names) - 1:
            new_name += names[index]
        else:
            temp_name = names[index]
            new_name += temp_name[0]  # first initial only
        index += 1
    count = check_pol_ID(new_name)

    if count == -1:
        return new_name.replace("'", "")

    # already taken, try first and last name
    new_name = ""
    index = 0
    while index < len(names):
        if index == len(names):
            new_name += names[index]
        elif index == 0:
            new_name += names[index]
        index += 1

    count = check_pol_ID(new_name)
    if count == -1:
        return new_name.replace("'", "")

    # add digits if nothing else works
    new_name += str(count + 1)
    print(new_name)
    return new_name.replace("'", "")


# check if politician ID is taken already
def check_pol_ID(name_id):
    count = politicians.get(name_id, -1)
    politicians[name_id] = count + 1
    return count


# will create a list of tickers that were traded
def parse_trades(trades):
    trades = trades.replace("'", '"')
    ticker_list = []
    done = False
    while not done:
        try:
            trades_list = json.loads(trades)
            for trade in trades_list:
                tickers = trade["ticker"].split(":")
                if len(tickers) == 2:
                    ticker_list.append(tickers[0])
            done = True
        except Exception as e:
            e = str(e)
            error_strings = e.split(" ")
            char_num = int(error_strings[6]) - 2
            new_trades = ""
            index = 0
            for c in trades:
                if index == char_num:
                    new_trades += "'"
                else:
                    new_trades += c
                index += 1
            trades = new_trades

    return ticker_list


# state id: all lowercase, no commas, _ instead of spaces
def generate_state_id(state):
    state = state.replace(" ", "_")
    state = state.lower()
    state = state.replace(",", "")
    return state


# will give value of {buy_trades : "range", sell_trades : "range"} to politicians

def init_buy_sell(trades):
    trades = trades.replace("'", '"')
    buy_sell = {}
    done = False

    sell_lower = 0
    sell_upper = 0
    buy_lower = 0
    buy_upper = 0

    while not done:
        try:
            trades_list = json.loads(trades)
            for trade in trades_list:
                # get all the info from json that we need

                type_trade = trade["type"]
                estimate = trade["value"].split(" - ")
                if len(estimate) == 2:
                    multiplyer_low = estimate[0][-1]
                    multiplyer_high = estimate[1][-1]
                    lower = int(estimate[0][0:-1])
                    high = int(estimate[1][0:-1])

                    # apply multipliers accordingly to the numbers
                    if multiplyer_low == "K":
                        lower *= 1000
                    elif multiplyer_low == "M":
                        lower *= 1000000
                    if multiplyer_high == "K":
                        high *= 1000
                    elif multiplyer_high == "M":
                        high *= 1000000

                    # assign the ranges to the specified category
                    if "buy" in type_trade:
                        buy_lower += lower
                        buy_upper += high
                    elif "sell" in type_trade:
                        sell_lower += lower
                        sell_upper += high

            done = True
        except Exception as e:
            e = str(e)
            error_strings = e.split(" ")
            print(error_strings)
            char_num = int(error_strings[6]) - 2
            new_trades = ""
            index = 0
            for c in trades:
                if index == char_num:
                    new_trades += "'"
                else:
                    new_trades += c
                index += 1
            trades = new_trades

    buy_sell["buy_trades"] = str(buy_lower) + " - " + str(buy_upper)
    buy_sell["sell_trades"] = str(sell_lower) + " - " + str(sell_upper)

    return buy_sell


if __name__ == "__main__":
    print("Initiating csv change...")
    politican_driver()


