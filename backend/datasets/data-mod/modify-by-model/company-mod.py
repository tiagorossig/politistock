from cmath import e
import csv
from hashlib import new
import json

"""
id,name,legalName,domain,domainAliases (4),
site (5),category,tags,description,foundedYear,
location,timeZone,utcOffset,geo,logo,
facebook(15),linkedin (16),twitter,crunchbase (18),emailProvider (19),
type,ticker,identifiers,phone,metrics,
indexedAt(25),tech(26),techCategories(27),parent,ultimateParent
"""
com_raw_path = "../data/companies.csv"

# declare new CSV file names: for writing
com_mod_path = "../data-mod/modify-by-model/companies-new.csv"

# helper dicts
companies = {}

# columns to delete from the raw file
delete_cols = [
    "domainAliases", 
    "site", 
    "facebook", 
    "linkedin",
    "crunchbase",
    "emailProvider",
    "indexedAt",
    "tech",
    "techCategories"
]

# save original header to get index values while parsing
original_header = []

# driver to create company ids for each row and parse the row
def company_driver():
    line_count = 0
    # open original and writing files (companies.csv)
    with open(com_raw_path, "r", newline="", encoding="utf-8") as com_raw_csv:
        com_reader = csv.reader(com_raw_csv, delimiter=",")
        with open(
            com_mod_path,
            "w" if line_count == 0 else "a",
            encoding="utf-8",
            newline="",
        ) as com_mod_csv:
            com_writer = csv.writer(com_mod_csv)
            # read original file row by row
            for row in com_reader:
                parse_row(row, line_count, com_writer)
                line_count += 1

# main row parsing function: remove unwanted fields and improper formatting
def parse_row(row, line_count, writer):
    new_row = []
    new_row = clean_row(row, new_row)
    if line_count == 0:
        for r in row: 
            original_header.append(r)
        print(f"New Headers: {new_row}")
        writer.writerow(new_row)
    else:
        # generate company ID for each row, replace ID field with generated companyID
        # get employee, country, and industry information for filtering
        company_name = new_row[1]
        metrics = row[original_header.index("metrics")]
        category = row[original_header.index("category")]
        geo = row[original_header.index("geo")]
        country = get_country(geo)
        new_row[new_row.index("country")] = country
        try:
            new_row[new_row.index("country_id")] = country
        except Exception as e:
            print(e)
            print(country)
        industry = get_industry(category)
        new_row[new_row.index("industry")] = industry
        try:
            industry_id = industry.replace(" ", "_")
            industry_id = industry_id.replace("&", "")
            new_row[new_row.index("industry_id")] = industry_id
        except Exception as e:
            print(e)
            print(industry)

        new_row[new_row.index("employees")] = get_employees(metrics)
        new_name = create_company_ID(company_name)
        new_row[0] = new_name
        writer.writerow(new_row)


# helper to remove unwanted things from the data


def clean_row(row, new_row):
    index = 0
    for s in row:
        # all elements: remove unnecessary "" and dollar signs
        new_string = s.replace("$", "")
        add_string = check_index(index)
        if index == original_header.index("phone"):
            new_string = reformat_phone(new_string)
        if index == original_header.index("twitter"):
            new_string = new_string.replace("\\n", " ")
        if add_string:
            new_row.append(new_string)
        index += 1
    # new columns (data points) to add to the end of each row
    new_row.append("country")
    new_row.append("country_id")
    new_row.append("industry")
    new_row.append("industry_id")
    new_row.append("employees")
    return new_row

# helper function to check the column index for raw columns that need to be deleted
def check_index(index):
    for col in delete_cols:
        if index == original_header.index(col):
            return False # false means delete the column
    return True # true means keep the column

# reformat phone number
def reformat_phone(string):
    new_string = string.replace("+", "")
    new_string = new_string.replace("-", "")
    return new_string.replace(" ", "")


# get the country from the geo data, for filtering
def get_country(geo):
    geo = geo.replace("'", '"')
    geo = geo.replace("None", '" "')
    try:
        geo_json = json.loads(geo)
        country = geo_json["country"]
        return country
    except Exception as e:
        e = str(e)
        print(e)
        print(geo[60:80])


# get the industry from the category data, for filtering
def get_industry(category):
    category = category.replace("'", '"')
    category = category.replace("None", '" "')
    try:
        category_json = json.loads(category)
        industry = category_json["industry"]
        industry_id = industry.replace(" ", "_")
        industry_id = industry_id.replace("&", "")
        return industry_id
    except Exception as e:
        e = str(e)
        print(e)
        print(category[40:60])


# get the number of employees from metrics data, for filtering
def get_employees(metrics):
    metrics = metrics.replace("'", '"')
    metrics = metrics.replace("None", '" "')
    try:
        metrics_json = json.loads(metrics)
        employees = metrics_json["employees"]
        return employees
    except Exception as e:
        e = str(e)
        print(e)
        print(metrics)


# create company ID
def create_company_ID(name):
    # lower case and split by words, join words together (remove spaces)
    lower_case = name.lower()
    names = lower_case.split()
    new_name = ""

    # add words one by one, stopping at first unique ID
    for i in range(len(names)):
        new_name += names[i]
        count = companies.get(new_name, -1)
        if count == -1:
            companies[new_name] = count + 1
            return new_name

    print(new_name)

    # if full company name already exists, add digit to the end
    count = companies.get(new_name, -1)
    companies[new_name] = count + 1
    if count == -1:
        return new_name

    new_name += str(count + 1)
    print(new_name)
    return new_name


if __name__ == "__main__":
    print("Initiating csv change...")
    company_driver()
