import csv
from hashlib import new
from datetime import datetime

"""
All columns in original set: 
language(0),region(1),quoteType,quoteSourceName,triggerable,
customPriceAlertConfidence,currency,fiftyTwoWeekHighChangePercent,fiftyTwoWeekLow,fiftyTwoWeekHigh,
dividendDate(10),earningsTimestamp(11),earningsTimestampStart(12),earningsTimestampEnd(13),trailingAnnualDividendRate,
trailingPE,trailingAnnualDividendYield,epsTrailingTwelveMonths,epsForward,epsCurrentYear,
priceEpsCurrentYear,sharesOutstanding,bookValue,fiftyDayAverage,fiftyDayAverageChange,
fiftyDayAverageChangePercent,twoHundredDayAverage,twoHundredDayAverageChange,twoHundredDayAverageChangePercent,marketCap,
forwardPE,priceToBook,sourceInterval,exchangeDataDelayedBy,pageViewGrowthWeekly,
averageAnalystRating(35),tradeable,marketState(37),exchange,shortName,
longName,messageBoardId(41),exchangeTimezoneName,exchangeTimezoneShortName,gmtOffSetMilliseconds(44),
market,esgPopulated,firstTradeDateMilliseconds(47),priceHint,regularMarketChange,
regularMarketChangePercent,regularMarketTime,regularMarketPrice,regularMarketDayHigh,regularMarketDayRange,
regularMarketDayLow,regularMarketVolume,regularMarketPreviousClose,bid,ask,
bidSize,askSize,fullExchangeName,financialCurrency,regularMarketOpen,
averageDailyVolume3Month,averageDailyVolume10Day,fiftyTwoWeekLowChange,fiftyTwoWeekLowChangePercent,fiftyTwoWeekRange,
fiftyTwoWeekHighChange,displayName,symbol(72),address1,city,
state,zip,country,phone(78),website,
industry,sector,longBusinessSummary(82),fullTimeEmployees,
companyOfficers(84)

Change 35 --> add new column (analyst_action)
Change 44 --> gmtoffsetmillisecons --> gmt offset
Change 47 --> firstTradeDateMilliseconds --> first trade date
Change 78 --> phone number

"""

# declare original CSV file names: for reading only
stock_raw_path = "../data/stocks.csv"
com_raw_path = "../data/companies.csv"

# declare new CSV file names: for writing
stock_mod_path = "../data-mod/modify-by-model/stocks-new.csv"
com_mod_path = "../data-mod/modify-by-model/companies-new.csv"

# columns to delete from the raw file
delete_cols = [
    "language", 
    'region', 
    "dividendDate", 
    "earningsTimestamp", 
    "earningsTimestampStart", 
    "earningsTimestampEnd",
    "marketState", 
    "messageBoardId", 
    "longBusinessSummary", 
    "companyOfficers"
    ]

# save original header to get index values while parsing
original_header = []

# driver function to create stockIDs and link to companies
def stock_driver():
    line_count = 0
    # open original and writable files for stocks
    with open(stock_raw_path, "r", encoding="utf8") as stock_raw_csv:
        stock_reader = csv.reader(stock_raw_csv)
        with open(stock_mod_path, "w", encoding="utf8", newline="") as stock_mod_csv:
            stock_writer = csv.writer(stock_mod_csv)
            # read row by row
            for row in stock_reader:
                parse_row(row, line_count, stock_writer)
                line_count += 1
                # print(f"line count: {line_count}")

# main row parsing function: remove unwanted fields and improper formatting
def parse_row(row, line_count, writer):
    new_row = []
    new_row = clean_row(row, new_row)
    if line_count == 0:
        for r in row: # save header row
            original_header.append(r)
        print(f"New Headers: {new_row}")
        writer.writerow(new_row)
    else:
        stock_ticker = original_header.index("symbol")
        new_row[0] = stock_ticker
        companyId = link_companies(stock_ticker)
        new_row[1] = companyId
        writer.writerow(new_row)


# helper to remove "" from data
def clean_row(row, new_row):
    index = 0
    new_row.append("id")
    new_row.append("companyId")
    for s in row:
        add_string = check_index(index)
        if add_string:
            new_string = reformat(s, index)
            # check index 35 here: add new column
            if index == original_header.index("averageAnalystRating"):
                strings = new_string.split(" ")
                analyst_action = "N/A"
                if len(strings) != 2:
                    pass
                else:
                    new_string = strings[0]  # analystrating
                    analyst_action = strings[1] #analyst action
                new_row.append(analyst_action)
            new_row.append(new_string)
        index += 1
    return new_row

# helper function to check the column index for raw columns that need to be deleted
def check_index(index):
    for col in delete_cols:
        if index == original_header.index(col):
            return False # false means delete the column
    return True # true means keep the column

# change the format of certain columns to fit SQL formatting
def reformat(string, index):
    string = string.replace("$", "")
    if index == original_header.index("averageAnalystRating"):  # special case
        return reformat_rating(string)
    if index == original_header.index("gmtOffSetMilliseconds"):
        if string == "gmtOffSetMilliseconds":
            return "gmtOffSet"
        # change milliseconds to hour offset
        return reformat_offset(string)
    if index == original_header.index("firstTradeDateMilliseconds"):
        if string == "firstTradeDateMilliseconds":
            return "firstTradeDate"
        # change milliseconds to date
        return reformat_date(string)
    if index == original_header.index("phone"):
        # reformat phone number
        return reformat_phone(string)
    return string

# helper to reformat buy/sell rating
def reformat_rating(string):
    string = string.replace(" ", "")
    string = string.replace("-", " ")
    return string

# helper to reformat GMT offset
def reformat_offset(string):
    try:
        new_string = ""
        # make a hard copy of the string
        if string[0] == "-" or string[0] == "+":
            index = 1
        else:
            index = 0
        while index < len(string):
            new_string += string[index]
            index += 1
        ms = int(new_string)
        dt = datetime.fromtimestamp(ms)
        ret = ""
        if string[0] == "-":
            ret += "-"
        ret += dt.strftime("%H:%M")
    except:
        pass
    return string

# reformat any date strings, from milliseconds to Y-M-D
def reformat_date(string):
    try:
        ms = int(string)
        # print(ms)
        dt = datetime.fromtimestamp(ms / 1000)
        # print(dt)
        string = dt.strftime("%Y-%m-%d")
    except:
        pass
    return string

# reformat phone number
def reformat_phone(string):
    # remove +, -, (), and ' '
    string = string.replace("+", "")
    string = string.replace("(", "")
    string = string.replace(")", "")
    string = string.replace("-", "")
    string = string.replace(" ", "")
    return string



# add company ID for each stock, for linking
def link_companies(stock_ticker):
    company_line_count = 0
    # open new company file
    with open(com_mod_path, "r", encoding="utf8") as companies:
        company_reader = csv.reader(companies)
        # loop through companies to find corresponding ID
        for company_row in company_reader:
            if company_line_count > 0:  # skip header
                if len(company_row) > company_ticker_index: # make sure not to run into index out of bounds
                    company_ticker = company_row[company_ticker_index]
                    # company ticker and stock ticker are the same, set company ID
                    if company_ticker.lower().replace(
                        '"', ""
                    ) == stock_ticker.lower().replace('"', ""):
                        return company_row[0]
                    # print(f"Stock ticker: {stock_ticker}, Company ID: {company_row[0]}, Company Ticker: {company_row[21]}")
            else:
                company_ticker_index = company_row.index("ticker")
            company_line_count += 1
            
    return "None"


if __name__ == "__main__":
    print("Initiating csv change...")
    stock_driver()
