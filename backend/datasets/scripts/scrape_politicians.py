import csv
import requests
from bs4 import BeautifulSoup
from sklearn.inspection import plot_partial_dependence

# handle csv
file = open("politicians.csv", "w", newline="")
writer = csv.writer(file)
row = [
    "name",
    "num_trades",
    "traded_volume",
    "last_trade",
    "party",
    "state",
    "role",
    "chamber",
    "dob",
    "trades",
]
writer.writerow(row)

# dict to hold all politician data
politician_dict = {}

# read raw polician html
with open("../data/politician_raw_html.txt") as file:
    content = file.read()

# get url suffixes
soup = BeautifulSoup(content, "html.parser")
url_suffixes = soup.find_all("a", class_="clickable-card__link link-element hoverable")

# generate urls for all politician pages
urls = []
for url in url_suffixes:
    urls.append(f'https://www.capitoltrades.com{url["href"]}')

count = 0  # used for logging
for url in urls:
    try:
        response = requests.get(url=url)
    except Exception as e:
        print(e)
        print("failed to get politician page")

    # PROFILE #
    # name
    soup = BeautifulSoup(response.content, "html.parser")
    name = soup.find(
        "div",
        class_="flex-row politician-profile__senator-name flex-row--h-align-center flex-row--v-align-center",
    )
    politician_dict["name"] = name.text

    # num_trades
    num_trades = soup.find(
        "div", class_="q-data-cell count-trades flavour--labelled-value"
    ).select_one(".q-value")
    politician_dict["num_trades"] = num_trades.text

    # num_issuers
    num_issuers = soup.find(
        "div", class_="q-data-cell count-issuers flavour--labelled-value"
    ).select_one(".q-value")
    politician_dict["num_trades"] = num_trades.text

    # traded_volume
    traded_volume = soup.find(
        "div", class_="q-data-cell volume flavour--labelled-value"
    ).select_one(".q-value")
    politician_dict["traded_volume"] = traded_volume.text

    # last_Traded
    last_trade = soup.find(
        "div", class_="q-data-cell last-traded flavour--labelled-value"
    ).select_one(".q-value")
    politician_dict["last_trade"] = last_trade.text

    # DETAILS #
    details = soup.select_one(".q-widget__body").find_all(
        "div",
        class_="flex-row flex-row--h-align-space-between flex-row--v-align-center",
    )

    # populate dict with scraped details
    for detail in details:
        split_detail = detail.text.split(":")
        politician_dict[split_detail[0].lower()] = split_detail[1].lower()

    # years active is often empty and not useful to us
    politician_dict.pop("years active", None)
    # rename date of birth key
    politician_dict["dob"] = politician_dict.pop("date of birth (age)")

    # TRADES #
    trades = []
    trades_html = soup.find_all("tr", class_="q-tr")
    # first q-tr is the row with each col name, remove it
    del trades_html[0]

    for trade in trades_html:
        cur_trade = {}

        # ticker
        company = trade.find("div", class_="q-data-cell hoverable traded-asset")
        ticker = company.find("span", class_="q-field company-ticker")
        cur_trade["ticker"] = ticker.text

        # issuer
        name = company.find("span", class_="q-field company-name")
        cur_trade["issuer"] = name.text

        # pub_date
        pub_date = trade.find("td", class_="q-td q-column--pubDate")
        cur_trade["pub_date"] = pub_date.text

        # traded_date
        traded_date = trade.find("td", class_="q-td q-column--txDate")
        cur_trade["traded_date"] = traded_date.text

        # delay
        delay = trade.find("td", class_="q-td q-column--reportingGap")
        cur_trade["delay"] = delay.text

        # type
        type = trade.find("td", class_="q-td q-column--txType")
        cur_trade["type"] = type.text

        # value
        value = trade.find("td", class_="q-td q-column--value")
        cur_trade["value"] = value.text

        # add cur_trade to trades list
        trades.append(cur_trade)

    # trades
    politician_dict["trades"] = trades

    # generate new dataset row and write it to csv
    new_row = []
    for attrib in row:
        new_row.append(politician_dict[attrib])

    writer.writerow(new_row)
    print(f"politician number #{count} written to csv")
    count += 1

file.close()
