// capitoltrades.com uses React to render the new politician pages client-side
// so we have to use a headless Chrome API like puppeteer to create a Chromium instance
// and launch the website instead of simply making http requests
// I attempted using the unofficial python port of this (pyppeteer) but it did not work well
const puppeteer = require('puppeteer');

(async function main() {
    const fsLibrary  = require('fs')
    try {
        // get our Chromium instance
        const browser = await puppeteer.launch();
        const [page] = await browser.pages();
        
        // loop through pages
        for (let i = 1; i < 16; i++) {
            // must use option networkidle0 to wait until there are no more network connections for at least 500ms
            // this ensures that all the react elements are rendered on the screen before we evaluate
            await page.goto(`https://www.capitoltrades.com/politicians?page=${i}`, { waitUntil: "networkidle0" });

            // get all HTML on this page and write it to a txt file
            const data = await page.evaluate(() => document.querySelector('*').outerHTML);
            console.log(`processing page ${i}`)
            fsLibrary.appendFile("backend/datasets/data/politician_raw_html.txt", data, (error) => {
                if (error) throw err;
            })
        }
        await browser.close();

    } catch (err) {
        console.error(err);
    }
})();
