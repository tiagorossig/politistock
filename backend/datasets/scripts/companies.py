import csv
import requests
import clearbit

# read stocks csv to get all company websites to use the Clearbit Company API
stocks = open("../data/stocks.csv", "r", encoding="utf8")
reader = csv.reader(stocks)
header = []
header = next(reader)
index = header.index("website")
company_names = set()

# handle new csv
file = open("../data/companies.csv", "w", newline="", encoding="utf-8")
writer = csv.writer(file)
row = [
    "id",
    "name",
    "legalName",
    "domain",
    "domainAliases",
    "site",
    "category",
    "tags",
    "description",
    "foundedYear",
    "location",
    "timeZone",
    "utcOffset",
    "geo",
    "logo",
    "facebook",
    "linkedin",
    "twitter",
    "crunchbase",
    "emailProvider",
    "type",
    "ticker",
    "identifiers",
    "phone",
    "metrics",
    "indexedAt",
    "tech",
    "techCategories",
    "parent",
    "ultimateParent",
]
writer.writerow(row)

count = 0  # used for logging
for csvRow in reader:
    website = csvRow[index]

    if website != "N/A":
        clearbit.key = "sk_33b82a16e6fe153b9fc0963595aff8ff"
        company = {}

        try:
            company = clearbit.Company.find(domain=website)
        except Exception as e:
            print(e)
            print("failed to find company")

        # generate new dataset row and write it to csv
        new_row = []
        if company is not None:
            for key, val in company.items():
                new_row.append(val)
        writer.writerow(new_row)

        count += 1
        print(f"company {website} written to csv. Total rows written: {count}")
