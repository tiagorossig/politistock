import shutil
import csv

# create companies-new
exec(open("../data-mod/modify-by-model/company-mod.py").read())


# create politicians-new
exec(open("../data-mod/modify-by-model/politician-mod.py").read())


# create stocks-new
exec(open("../data-mod/modify-by-model/stock-mod.py").read())


# create companies-linked, stocks-linked, politicians-linked
exec(open("../data-mod/link-models/link-models.py").read())


# append imgs, twitter, and seniority cols to politicians-linked
# read files into rows lists
politicians_db = open("../data-mod/link-models/politicians-linked.csv", "r")
reader = csv.reader(politicians_db)
politicians_linked_rows = []
for row in reader:
    politicians_linked_rows.append(row)
politicians_db.close()

politicians_twitters_db = open("../data/politicians-twitters.csv", "r")
reader = csv.reader(politicians_twitters_db)
politician_twitters_rows = []
for row in reader:
    politician_twitters_rows.append(row)
politicians_twitters_db.close()

politicians_images_db = open("../data/politicians-images.csv", "r")
reader = csv.reader(politicians_images_db)
politician_images_rows = []
for row in reader:
    politician_images_rows.append(row)
politicians_images_db.close()

politicians_seniority_db = open("../data/politicians-seniority.csv", "r") 
reader = csv.reader(politicians_seniority_db)
politician_seniority_rows = []
for row in reader:
    politician_seniority_rows.append(row)
politicians_seniority_db.close()

# append imgs, twitters, and seniorities
politicians_linked = open(
    "../data-mod/link-models/politicians-linked.csv", "w", newline="", encoding="utf-8"
)
writer = csv.writer(politicians_linked)

idx = 0
for row in politicians_linked_rows:
    if idx == 0:
        row.append("imgs")
        row.append("twitter")
        row.append("seniority")
    else:
        row.append(politician_images_rows[idx][0])
        row.append(politician_twitters_rows[idx][0])
        row.append(politician_seniority_rows[idx][0])
        
    idx += 1
    writer.writerow(row)

politicians_linked.close()


# move companies-linked, stocks-linked, politicians-linked into docker-flask-mysql
files = [
    "../data-mod/link-models/politicians-linked.csv",
    "../data-mod/link-models/stocks-linked.csv",
    "../data-mod/link-models/companies-linked.csv",
]
for file in files:
    shutil.copy(file, "../../docker-flask-mysql/db/init_data")
