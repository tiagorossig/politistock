import csv
import requests
import ast
from time import sleep

# handle csv
file = open("../data/stocks.csv", "w", newline="", encoding="utf-8")
writer = csv.writer(file)
row = [
    "language",
    "region",
    "quoteType",
    "quoteSourceName",
    "triggerable",
    "customPriceAlertConfidence",
    "currency",
    "fiftyTwoWeekHighChangePercent",
    "fiftyTwoWeekLow",
    "fiftyTwoWeekHigh",
    "dividendDate",
    "earningsTimestamp",
    "earningsTimestampStart",
    "earningsTimestampEnd",
    "trailingAnnualDividendRate",
    "trailingPE",
    "trailingAnnualDividendYield",
    "epsTrailingTwelveMonths",
    "epsForward",
    "epsCurrentYear",
    "priceEpsCurrentYear",
    "sharesOutstanding",
    "bookValue",
    "fiftyDayAverage",
    "fiftyDayAverageChange",
    "fiftyDayAverageChangePercent",
    "twoHundredDayAverage",
    "twoHundredDayAverageChange",
    "twoHundredDayAverageChangePercent",
    "marketCap",
    "forwardPE",
    "priceToBook",
    "sourceInterval",
    "exchangeDataDelayedBy",
    "pageViewGrowthWeekly",
    "averageAnalystRating",
    "tradeable",
    "marketState",
    "exchange",
    "shortName",
    "longName",
    "messageBoardId",
    "exchangeTimezoneName",
    "exchangeTimezoneShortName",
    "gmtOffSetMilliseconds",
    "market",
    "esgPopulated",
    "firstTradeDateMilliseconds",
    "priceHint",
    "regularMarketChange",
    "regularMarketChangePercent",
    "regularMarketTime",
    "regularMarketPrice",
    "regularMarketDayHigh",
    "regularMarketDayRange",
    "regularMarketDayLow",
    "regularMarketVolume",
    "regularMarketPreviousClose",
    "bid",
    "ask",
    "bidSize",
    "askSize",
    "fullExchangeName",
    "financialCurrency",
    "regularMarketOpen",
    "averageDailyVolume3Month",
    "averageDailyVolume10Day",
    "fiftyTwoWeekLowChange",
    "fiftyTwoWeekLowChangePercent",
    "fiftyTwoWeekRange",
    "fiftyTwoWeekHighChange",
    "displayName",
    "symbol",
    "address1",
    "city",
    "state",
    "zip",
    "country",
    "phone",
    "website",
    "industry",
    "sector",
    "longBusinessSummary",
    "fullTimeEmployees",
    "companyOfficers",
]
writer.writerow(row)

# read politicians csv to get all tickers traded
politicians = open("../data/politicians.csv", "r")
reader = csv.reader(politicians)
header = []
header = next(reader)
tickers = set()
for csvRow in reader:
    trades = ast.literal_eval(csvRow[-1])  # convert string to list
    for trade in trades:
        # split into ticker and country code
        split_ticker = trade["ticker"].split(":")
        # ignore non-US tickers, include OTC stocks (MLYBY) and crypto
        if split_ticker[0] == "N/A":
            pass
        elif split_ticker[0] == "MLYBY":
            tickers.add(split_ticker[0])
        elif split_ticker[0][0] == "$":
            tickers.add(split_ticker[0][1:])
        elif split_ticker[1] == "US":
            tickers.add(split_ticker[0])

count = 0  # used for logging
# make http requests with 10 symbols each
while len(tickers) > 0:
    ticker_str = tickers.pop()

    # generate url and make request to /quote
    url = "https://yfapi.net/v6/finance/quote"
    querystring = {"symbols": ticker_str, "region": "US", "lang": "en"}
    headers = {"x-api-key": "zpBPvt1miJ820OPjBrQ5A7AsI8cj9kLaadrKEgea"}

    try:
        response = requests.request("GET", url, headers=headers, params=querystring)
    except Exception as e:
        print(e)
        print("failed to get stock /quote")

    print("first response:", response.json())

    # index into json nested dicts and lists
    stock = response.json()["quoteResponse"]["result"]
    if len(stock) > 0:
        stock = stock[0]
    else:
        stock = {}

    # changed up the syntax for header and params to the output of https://curlconverter.com/
    # because I was unable to get the requests through otherwise
    headers = {
        "accept": "application/json",
        "X-API-KEY": "zpBPvt1miJ820OPjBrQ5A7AsI8cj9kLaadrKEgea",
    }
    params = (
        ("lang", "en"),
        ("region", "US"),
        ("modules", "defaultKeyStatistics,assetProfile"),
    )

    try:
        response = requests.get(
            f"https://yfapi.net/v11/finance/quoteSummary/{ticker_str}",
            headers=headers,
            params=params,
        )
    except Exception as e:
        print(e)
        print("failed to get stock /quoteSummary")

    print("second response:", response.json())

    # append list from new request to existing listc
    if "quoteSummary" in response.json():
        stock_summary = response.json()["quoteSummary"]["result"]
        # if result is None it means the API has no summary data found for this ticker
        if stock_summary is not None:
            stock_summary = stock_summary[0]["assetProfile"]
            # add stock_summary's items to stocks
            for key, val in stock_summary.items():
                stock[key] = val

    # generate new dataset row and write it to csv
    new_row = []
    for attrib in row:
        if attrib in stock:
            new_row.append(str(stock[attrib]))
        else:
            new_row.append("N/A")

    writer.writerow(new_row)

    count += 1
    print(f"ticker(s) {ticker_str} written to csv. Total rows written: {count}")

    # add delay in between requests due to APIs request rate limit
    sleep(1.0)

file.close()
