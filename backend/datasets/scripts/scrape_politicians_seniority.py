import csv
import requests

politicians_db = open(
    "../../docker-flask-mysql/db/init_data/politicians-linked.csv", "r"
)
reader = csv.reader(politicians_db)
rows_list = []
for row in reader:
    rows_list.append(row)

politicians_db.close()

headers = {"X-API-Key":"GtXcZCkme5OY18sqW7CsHOPmI4AlaFzWGnxI5vam"}

senate_117 = requests.get("https://api.propublica.org/congress/v1/117/senate/members.json", auth=None, headers=headers).json()
house_117 = requests.get("https://api.propublica.org/congress/v1/117/house/members.json", auth=None, headers=headers).json()

senate_116 = requests.get("https://api.propublica.org/congress/v1/116/senate/members.json", auth=None, headers=headers).json()
house_116 = requests.get("https://api.propublica.org/congress/v1/116/house/members.json", auth=None, headers=headers).json()

senate_115 = requests.get("https://api.propublica.org/congress/v1/115/senate/members.json", auth=None, headers=headers).json()
house_115 = requests.get("https://api.propublica.org/congress/v1/115/house/members.json", auth=None, headers=headers).json()

senate_114 = requests.get("https://api.propublica.org/congress/v1/114/senate/members.json", auth=None, headers=headers).json()
house_114 = requests.get("https://api.propublica.org/congress/v1/114/house/members.json", auth=None, headers=headers).json()

congress = [senate_117, house_117, senate_116, house_116, senate_115, house_115, senate_114, house_114]

file = open(
    "../data/politicians-seniority.csv",
    "w",
    newline="",
    encoding="utf-8",
)
writer = csv.writer(file)

idx = 0
for row in rows_list:
    new_row = []
    if idx == 0:
        new_row.append("seniority")
    else:
        twitter = row[20]
        before, sep, after = twitter.rpartition("@")
        politician = None
        for i in range(8):
            politician = next((pol for pol in congress[i]["results"][0]["members"] if pol["twitter_account"] == after), None)
            if (politician != None):
                new_row.append(politician["seniority"])
                break
        if (politician == None):
            new_row.append(0)

    print(f"writing row #{idx}")
    idx += 1
    writer.writerow(new_row)

file.close()
