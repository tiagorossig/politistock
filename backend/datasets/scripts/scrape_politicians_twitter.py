import csv
import requests
from bs4 import BeautifulSoup
from sklearn.inspection import plot_partial_dependence

# read file into rows_list
politicians_db = open(
    "../../docker-flask-mysql/db/init_data/politicians-linked.csv", "r"
)
reader = csv.reader(politicians_db)
rows_list = []
for row in reader:
    rows_list.append(row)

politicians_db.close()

# dict to hold all politician data
name_twitter = {}

# read raw polician html
with open("../data/politician_raw_html.txt") as file:
    content = file.read()

# get url suffixes
soup = BeautifulSoup(content, "html.parser")
url_suffixes = soup.find_all("a", class_="clickable-card__link link-element hoverable")

# generate urls for all politician pages
urls = []
for url in url_suffixes:
    urls.append(f'https://www.capitoltrades.com{url["href"]}')

count = 0  # used for logging
for url in urls:
    try:
        response = requests.get(url=url)
    except Exception as e:
        print(e)
        print("failed to get politician page")

    # soup
    soup = BeautifulSoup(response.content, "html.parser")

    # name
    name = soup.find("title")
    name = name.text.split(" Trades")[0]

    # twitter
    twitter = soup.find_all("a", class_="button hoverable button-icon button--size-m")
    twitter = twitter[len(twitter) - 1]["href"]

    # populate dict
    name_twitter[name] = twitter

    # logging
    print(f"populated dict #{count}")
    count += 1

# handle csv
file = open(
    "../data/politicians-twitters.csv",
    "w",
    newline="",
    encoding="utf-8",
)
writer = csv.writer(file)

idx = 0
for row in rows_list:
    new_row = []
    if idx == 0:
        new_row.append("twitter")
    else:
        pol_name = row[2]
        new_row.append(name_twitter[pol_name])

    print(f"writing row #{idx}")
    idx += 1
    writer.writerow(new_row)


file.close()
