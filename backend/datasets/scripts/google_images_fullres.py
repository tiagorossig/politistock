import json
import urllib.request
import csv
from serpapi import GoogleSearch


def get_google_images_fullres(name):
    params = {
        # 3 different accounts, rotate to get around hourly throughput limit
        "api_key": "4ff2db1888624ca5c9de98b6facaf86ab493bc0336a24218fef3980bceb260ee",
        #   "api_key": "60065e63fc6a6e9394f03937c1e3e9b6fd108cd3865c6654e24b4b26c8ec64fb",
        #   "api_key": "2bd0a051bcddbc8a9e18318c6c6c6ad0fee89300829f8464a04291d3ccf63d37",
        "engine": "google",
        "q": f"{name} US politician portrait",
        "tbm": "isch",
    }

    search = GoogleSearch(params)
    results = search.get_dict()

    return results["images_results"][0]["original"]


# read file into rows_list
politicians_db = open(
    "../../docker-flask-mysql/db/init_data/politicians-linked.csv", "r"
)
reader = csv.reader(politicians_db)
rows_list = []
for row in reader:
    rows_list.append(row)

politicians_db.close()

# write to file
politicians_db = open(
    "../data/politicians-images.csv", "a", newline="", encoding="utf-8"
)
writer = csv.writer(politicians_db)

idx = 0
for row in rows_list:
    new_row = []
    if idx == 0:
        new_row.append("imgs")
    else:
        pol_name = row[1]
        new_row.append(get_google_images_fullres(pol_name))

    idx += 1
    print(f"writing row #{idx}")
    writer.writerow(new_row)

politicians_db.close()
