from bs4 import BeautifulSoup
import requests
import csv


def get_google_img(name):  # get link to the first google image search result
    url = (
        "https://www.google.com/search?q="
        + name
        + " US politician portrait"
        + "&source=lnms&tbm=isch"
    )
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
    }

    html = requests.get(url, headers=headers).text

    soup = BeautifulSoup(html, "html.parser")
    image = soup.find("img", {"class": "yWs4tf"})

    if not image:
        return "N/A"
    return image["src"]


# read file into rows_list
politicians_db = open(
    "../../docker-flask-mysql/db/init_data/politicians-linked.csv", "r"
)
reader = csv.reader(politicians_db)
rows_list = []
for row in reader:
    rows_list.append(row)

politicians_db.close()

# write to file
politicians_db = open(
    "../../docker-flask-mysql/db/init_data/politicians-linked.csv",
    "w",
    newline="",
    encoding="utf-8",
)
writer = csv.writer(politicians_db)

idx = 0
for row in rows_list:
    if idx == 0:
        row.append("imgs")
    else:
        pol_name = row[1]
        row.append(get_google_img(pol_name))

    print(f"writing row #{idx}")
    idx += 1
    writer.writerow(row)

politicians_db.close()
