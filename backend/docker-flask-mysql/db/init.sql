CREATE DATABASE IF NOT EXISTS politistock;
CREATE TABLE IF NOT EXISTS politistock.politicians (
  id VARCHAR(50),
  `name` VARCHAR(50),
  `preferred_name` VARCHAR(50),
  num_trades SMALLINT(6) UNSIGNED,
  traded_volume INT(10) UNSIGNED,
  last_trade VARCHAR(50),
  party VARCHAR(50),
  `state` VARCHAR(50),
  `role` VARCHAR(50),
  chamber VARCHAR(50),
  age SMALLINT(2) UNSIGNED,
  dob VARCHAR(50),
  trades TEXT(65535),
  phone VARCHAR(50),
  tickers TEXT(65535),
  buy_volume TEXT(65535),
  sell_volume TEXT(65536),
  state_id VARCHAR(50),
  companies TEXT(65535),
  imgs TEXT(65535),
  twitter TEXT(65535),
  seniority SMALLINT(6) UNSIGNED
);

LOAD DATA LOCAL INFILE '/docker-entrypoint-initdb.d/init_data/politicians-linked.csv'
INTO TABLE politistock.politicians
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(id,name,preferred_name,num_trades,traded_volume,last_trade,party,state,role,chamber,age,dob,trades,phone,tickers,buy_volume,sell_volume,state_id,companies,imgs,twitter,seniority);

CREATE TABLE IF NOT EXISTS politistock.companies (
  id VARCHAR(50),
  `name` VARCHAR(50),
  legalName VARCHAR(60),
  domain VARCHAR(70),
  category TEXT(65535),
  tags VARCHAR(200),
  `description` TEXT(65535),
  foundedYear INT(4) UNSIGNED,
  `location` TEXT(65535),
  timezone VARCHAR(100),
  UTCoffset SMALLINT(3),
  geo TEXT(65535),
  logo VARCHAR(200),
  twitter TEXT(65535),
  `type` VARCHAR(50),
  ticker VARCHAR(50),
  identifiers VARCHAR(200),
  phone VARCHAR(50),
  metrics TEXT(65535),
  parent VARCHAR(200),
  ultimateParent VARCHAR(200), 
  country VARCHAR(100), 
  country_id VARCHAR(100),
  industry VARCHAR(200),
  industry_id VARCHAR(200), 
  employees INT(7) UNSIGNED,
  politicians TEXT(65535)
);
LOAD DATA LOCAL INFILE '/docker-entrypoint-initdb.d/init_data/companies-linked.csv' 
INTO TABLE politistock.companies 
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,name,legalName,domain,category,tags,description,foundedYear,location,timeZone,utcOffset,geo,logo,twitter,type,ticker,identifiers,phone,metrics,parent,ultimateParent,country,country_id,industry,industry_id,employees,politicians);


CREATE TABLE IF NOT EXISTS politistock.stocks (
  id VARCHAR(10),
  companyId VARCHAR(50),
  quoteType VARCHAR(50),
  quoteSourceName VARCHAR(50),
  triggerable VARCHAR(5),
  customPriceAlertConfidence VARCHAR(15),
  currency VARCHAR(10),
  fiftyTwoWeekHighChangePercent FLOAT(10, 8),
  fiftyTwoWeekLow FLOAT(7, 2),
  fiftyTwoWeekHigh FLOAT(7, 2),
  trailingAnnualDividendRate FLOAT(7, 3),
  trailingPE FLOAT(11, 6),
  trailingAnnualDividendYield FLOAT(14, 9),
  epsTrailingTwelveMonths FLOAT (8, 3),
  epsForward FLOAT(7, 2),
  epsCurrentYear FLOAT(7, 2),
  priceEpsCurrentYear FLOAT(9, 6),
  sharesOutstanding INT(12) UNSIGNED,
  bookValue FLOAT(7, 3),
  fiftyDayAverage FLOAT(9, 4),
  fiftyDayAverageChange FLOAT(12, 7),
  fiftyDayAverageChangePercent FLOAT(14, 9),
  twoHundredDayAverage FLOAT(9, 4),
  twoHundredDayAverageChange FLOAT(12, 7),
  twoHundredDayAverageChangePercent FLOAT (14, 9),
  marketCap BIGINT(20) UNSIGNED,
  forwardPE FLOAT(10, 5),
  priceToBook FLOAT(11, 6),
  sourceInterval INT(5) UNSIGNED,
  exchangeDataDelayedBy INT(4) UNSIGNED,
  pageViewGrowthWeekly FLOAT(13, 8),
  analystAction VARCHAR(6),
  averageAnalystRating FLOAT(5, 2),
  tradeable VARCHAR(5),
  exchange VARCHAR(8),
  shortName VARCHAR(50),
  longName VARCHAR(100),
  exchangeTimezoneName VARCHAR(50),
  exchangeTimezoneShortName VARCHAR(8),
  gmtOffSet INT(12),
  market VARCHAR(50),
  esgPopulated VARCHAR(5),
  firstTradeDate VARCHAR(10),
  priceHint SMALLINT(3),
  regularMarketChange FLOAT(5, 2),
  regularMarketChangePercent FLOAT (9, 4),
  regularMarketTime INT(12) UNSIGNED,
  regularMarketPrice FLOAT(7, 2),
  regularMarketDayHigh FLOAT(7, 2),
  regularMarketDayRange VARCHAR(30),
  regularMarketDayLow FLOAT(7, 2),
  regularMarketVolume INT(10) UNSIGNED,
  regularMarketPreviousClose FLOAT (7, 2),
  bid FLOAT (7, 2),
  ask FLOAT (7, 2),
  bidSize SMALLINT (5) UNSIGNED,
  askSize SMALLINT (5) UNSIGNED,
  fullExchangeName VARCHAR(50),
  financialCurrency VARCHAR(10),
  regularMarketOpen FLOAT (7, 2),
  averageDailyVolume3Month INT(10) UNSIGNED,
  averageDailyVolume10Day INT(10) UNSIGNED,
  fiftyTwoWeekLowChange FLOAT (10, 6),
  fiftyTwoWeekLowChangePercent FLOAT(12, 8),
  fiftyTwoWeekRange VARCHAR(30),
  fiftyTwoWeekHighChange FLOAT(12, 8),
  displayName VARCHAR(50),
  symbol VARCHAR(10),
  address1 VARCHAR (300),
  city VARCHAR(100),
  `state` VARCHAR(30),
  zip VARCHAR(5),
  country VARCHAR(50),
  phone VARCHAR(50),
  website TEXT(65535),
  industry VARCHAR (100),
  sector TEXT(65535),
  fullTimeEmployees INT(7) UNSIGNED, 
  politicians TEXT(65535)
);
LOAD DATA LOCAL INFILE '/docker-entrypoint-initdb.d/init_data/stocks-linked.csv' 
INTO TABLE politistock.stocks 
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id,companyId,quoteType,quoteSourceName,triggerable,customPriceAlertConfidence,currency,fiftyTwoWeekHighChangePercent,fiftyTwoWeekLow,fiftyTwoWeekHigh,trailingAnnualDividendRate,trailingPE,trailingAnnualDividendYield,epsTrailingTwelveMonths,epsForward,epsCurrentYear,priceEpsCurrentYear,sharesOutstanding,bookValue,fiftyDayAverage,fiftyDayAverageChange,fiftyDayAverageChangePercent,twoHundredDayAverage,twoHundredDayAverageChange,twoHundredDayAverageChangePercent,marketCap,forwardPE,priceToBook,sourceInterval,exchangeDataDelayedBy,pageViewGrowthWeekly,analystAction,averageAnalystRating,tradeable,exchange,shortName,longName,exchangeTimezoneName,exchangeTimezoneShortName,gmtOffSet,market,esgPopulated,firstTradeDate,priceHint,regularMarketChange,regularMarketChangePercent,regularMarketTime,regularMarketPrice,regularMarketDayHigh,regularMarketDayRange,regularMarketDayLow,regularMarketVolume,regularMarketPreviousClose,bid,ask,bidSize,askSize,fullExchangeName,financialCurrency,regularMarketOpen,averageDailyVolume3Month,averageDailyVolume10Day,fiftyTwoWeekLowChange,fiftyTwoWeekLowChangePercent,fiftyTwoWeekRange,fiftyTwoWeekHighChange,displayName,symbol,address1,city,state,zip,country,phone,website,industry,sector,fullTimeEmployees,politicians);
