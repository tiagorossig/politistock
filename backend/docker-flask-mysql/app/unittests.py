from unittest import main, TestCase
from app import *

class UnitTests(TestCase):
    def setUp(self):
        pass

    # politicians test
    def test_1(self):
        result = {"all": {"politicians": """((1, 2, {'A': 'B'}))"""}}
        python_style_to_json(result)
        expected = {"all": {"politicians": (1, 2, {"A": "B"})}}
        self.assertEqual(expected, result)

    # companies test
    def test_2(self):
        result = {"all": {"companies": """((1, 2, {'A': 'B'}))"""}}
        python_style_to_json(result)
        expected = {"all": {"companies": (1, 2, {"A": "B"})}}
        self.assertEqual(expected, result)

    # stocks test
    def test_3(self):
        result = {"all": {"stocks": """((1, 2, {'A': 'B'}))"""}}
        python_style_to_json(result)
        expected = {"all": {"stocks": "((1, 2, {'A': 'B'}))"}}
        self.assertEqual(expected, result)

    # politican by ID test
    def test_4(self):
        result = {"aaspanberger": {"politicians": """((1, 2, {'A': 'B'}))"""}}
        python_style_to_json(result)
        expected = {"aaspanberger": {"politicians": (1, 2, {"A": "B"})}}
        self.assertEqual(expected, result)

    # company by ID test
    def test_5(self):
        result = {"3d": {"politicians": """((1, 2, {'A': 'B'}))"""}}
        python_style_to_json(result)
        expected = {"3d": {"politicians": (1, 2, {"A": "B"})}}
        self.assertEqual(expected, result)

    # stock by ID test
    def test_6(self):
        result = {"A": {"politicians": """((1, 2, {'A': 'B'}))"""}}
        python_style_to_json(result)
        expected = {"A": {"politicians": (1, 2, {"A": "B"})}}
        self.assertEqual(expected, result)

if __name__ == "__main__":
    main()
