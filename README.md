Group Members (GitLab ID, EID):

- Tiago Grimaldi Rossi: @tiagorossig, tg24645
- Kyle Kamka: @kkamka, kmk3744
- Priyanka: @pbarve1, pab2874
- Ramon: @ramon_marquez, rm57622
- Jonathan: @jonathanbli, jbl2594

# Phase 1

Git SHA: 0c4de3893444b1db93492634838894549336ec99 \
Project Leader Phase 1: Tiago Rossi \
GitLab Pipelines: https://gitlab.com/tiagorossig/politistock/-/pipelines \
Website: https://politistock.me

Estimated completion time for each member:

- Tiago - 10 hours
- Kyle - 10 hours
- Priyanka - 8 hours
- Ramon - 6 hours
- Jonathan - 5 hours

Actual completion time for each member:

- Tiago - 12 hours
- Kyle - 14 hours
- Priyanka - 12 hours
- Ramon - 6 hours
- Jonathan - 7 hours

Comments

- Took inspiration from the texas-votes [gitlab pipline](https://gitlab.com/forbesye/fitsbits/-/blob/master/.gitlab-ci.yml) tests in order to be able to test our own build ourselves
- Referenced Texas Votes [`About.js` page](https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/About/About.js) for info on how to dynamically source GitLab statistics

# Phase 2

Git SHA: 250fe8a5c3feea06259c7e25f0bb57462d60d1ff \
Project Leader Phase 2: Priyanka Barve \
GitLab Pipelines: https://gitlab.com/tiagorossig/politistock/-/pipelines \
Website: https://politistock.me \
API: https://api.politistock.me

Estimated completion time for each member:

- Tiago - 25 hours
- Kyle - 15 hours
- Priyanka - 20 hours
- Ramon - 15 hours
- Jonathan - 15 hours

Actual completion time for each member:

- Tiago - 30 hours
- Kyle - 28 hours
- Priyanka - 30 hours
- Ramon - 21 hours
- Jonathan - 17 hours

Comments

- Docker Image used for Selenium was inspired by SafeTravels Image
- Our API is implemented in backend/docker-flask-mysql/app/app.py instead of backend/app.py. We favored the use of this particular folder structure because we're using Docker Compose.
- The mini chart typescript component for stock instances ('/front-end/src/screens/stocks/instance/MiniChart.tsx') was inspired by the npm package react-tradingview-embed.

# Phase 3

Git SHA: 1fdd5e4931c7c9085d68d09039131ce6754cc07e \
Project Leader Phase 3: Ramon Marquez \
GitLab Pipelines: https://gitlab.com/tiagorossig/politistock/-/pipelines \
Website: https://politistock.me \
API: https://api.politistock.me

Estimated completion time for each member:

- Tiago - 20 hours
- Kyle - 15 hours
- Priyanka - 15 hours
- Ramon - 12 hours
- Jonathan - 15 hours

Actual completion time for each member:

- Tiago - 23 hours
- Kyle - 25 hours
- Priyanka - 22 hours
- Ramon - 15 hours
- Jonathan - 15 hours

Comments

- Docker Image used for Selenium was inspired by SafeTravels Image
- Our API is implemented in backend/docker-flask-mysql/app/app.py instead of backend/app.py. We favored the use of this particular folder structure because we're using Docker Compose.
- The mini chart typescript component for stock instances ('/front-end/src/screens/stocks/instance/MiniChart.tsx') was inspired by the npm package react-tradingview-embed.

# Phase 4

Git SHA: 975ad12f3ad4f0536b824afda4ef5ec5e5d8af33 \
Project Leader Phase 4: Kyle Kamka \
GitLab Pipelines: https://gitlab.com/tiagorossig/politistock/-/pipelines \
Website: https://politistock.me \
API: https://api.politistock.me

Estimated completion time for each member:

- Tiago - 7 hours
- Kyle - 5 hours
- Priyanka - 5 hours
- Ramon - 6 hours
- Jonathan - 5 hours

Actual completion time for each member:

- Tiago - 7 hours
- Kyle - 7 hours
- Priyanka - 5 hours
- Ramon - 6 hours
- Jonathan - 7 hours

Comments

- `react-bubble-chart.js` is a slightly modified version of [WeKnow's implementation](https://github.com/weknowinc/react-bubble-chart-d3)
- `donut-chart.js` is a slightly modified version of [Vineeth.R's implementation](https://dev.to/vineethtrv/react-d3-donut-chart-49cm)
- `scatterplot.js` is a slightly modified version of [Yan Holtz's implementation](https://d3-graph-gallery.com/graph/scatter_basic.html)
