# PolitiStock

## Project for C S 373: Software Engineering - Spring 2022

### Canvas / Discord group number: 10 AM Group 3

### Members: Jonathan Li, Priyanka Barve, Kyle Kamka, Ramon Marquez, Tiago Grimaldi Rossi

### Name of the project: PolitiStock

### Gitlab Repo: https://gitlab.com/tiagorossig/politistock

# The proposed project

This site will provide transparency between the monetary movements of politicians within the United States and their constituents. We do this by displaying the recent holdings of politicians while also displaying information about them as well. We also provide a list of companies and stocks with their information such as which politicians have been involved in them.

### Data Sources

[Google’s Civic Information API](https://developers.google.com/civic-information) - Provide info on politician

[Census Data API](https://www.census.gov/data/developers/data-sets/decennial-census.html)

[Open Secrets API](https://www.opensecrets.org/open-data/api) - General Legislature Info

[Twitter API](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api) - Display Politicians Tweets

[ProPublica Congress API](https://projects.propublica.org/api-docs/congress-api/members/) - All info on politicians (Gets Twitter handle)

[YahooFinance](https://www.yahoofinanceapi.com/) - Chart Info, Stock info

[EDGAR](https://www.sec.gov/edgar/searchedgar/cik.htm) - CIK from Name

[WallStreet Bets API](https://dashboard.nbshare.io/apps/reddit/api/) - Sentiment analysis of Stocks discussed on r/wallstreetbets, a subreddit page

[Athlethia](https://aletheiaapi.com/docs/) - Connects Stock to Public Profile
ClearBit - Company Information

[Capitol Trades](https://www.capitoltrades.com/) - Politician trade data

[Google Maps API](https://developers.google.com/maps)

# Models

## Politicians

### Instances:

223 Estimated Instances

### Filterable/Sortable Atributes

1. Political Party
2. Number of Holdings
3. State
4. Trade Volume
5. State Rank

### Searchable Attributes

1. Name
2. Representative/Senator
3. Geographical Region (of the U.S.)
4. State Abbreviation
5. Address of user to match with their representative

### Model Connections

**Politician :arrow_right: Company**

- We see what companies each politician is invested in

**Politician :arrow_right: Stock**

- We see what stocks they have recently traded

### Rich Media

1. Images: There will be image(s) of each politician instance
2. Feed: The politician's Twitter feed will also be on each instance page

## Stocks

### Instances:

500+ Estimated Instances

### Filterable/Sortable Atributes

1. Country of Origin
2. Selling Price
3. Market Cap
4. Daily volume
5. Stock exchange where it is listed

### Searchable Attributes

1. Ticker
2. Company Association
3. Region/Country
4. Price
5. Where it is listed

### Model Connections

**Stock :arrow_right: Politician**

- Each stock instance is linked to the politicians that own that specific stock.

**Stock :arrow_right: Company**

- Each stock instance is linked to the company associated with that stock.

### Rich Media

1. News: There will be news articles that relate to the specific stock instance
2. Image/Chart: There will be a chart that shows the stock price over time
3. Reddit Feed: There will be sentiment analysis information on how that stock is being discussed on r/wallstreetbets, if it is being discussed.

## Companies

### Instances:

500+ Estimated Instances

### Filterable/Sortable Atributes

1. Name (alphabetical)
2. Country of Origin
3. Industry
4. Company Size (number of employees)
5. State/Province location

### Searchable Attributes

1. Company Name
2. CEO Name
3. Founding Date
4. Headquarters location
5. Industry

### Model Connections

**Company :arrow_right: Stock**

- Each company profile instance is linked to its respective stock profile

**Company :arrow_right: Politician**

- Each company instance is linked to the politician instances that are invested in them

### Rich Media

1. Google Map: There will be a map showing the Company's location
2. Image: There will be an image showing the Company's logo

## 3 Questions

1. **What stocks/companies are politicians investing in?**
2. **Which stocks are doing good? Which stocks should I buy?**
3. **What company is this stock associated with? What is some important info about this company?**
